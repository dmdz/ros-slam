# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/src/dji_osdk_ros/modules/osdkhal_linux.c" "/home/dmd/BitBucket/ros-slam/amr/build/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/CMakeFiles/dji_sdk_node.dir/__/dji_osdk_ros/modules/osdkhal_linux.c.o"
  "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/src/dji_osdk_ros/modules/osdkosal_linux.c" "/home/dmd/BitBucket/ros-slam/amr/build/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/CMakeFiles/dji_sdk_node.dir/__/dji_osdk_ros/modules/osdkosal_linux.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "ADVANCED_SENSING"
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"dji_osdk_ros\""
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/dmd/BitBucket/ros-slam/amr/devel/include"
  "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/include"
  "/usr/include/libusb-1.0"
  "/opt/ros/melodic/include"
  "/opt/ros/melodic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/../../include/dji_osdk_ros_obsoleted"
  "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/../../include"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/dji_linux_environment.cpp" "/home/dmd/BitBucket/ros-slam/amr/build/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/CMakeFiles/dji_sdk_node.dir/dji_linux_environment.cpp.o"
  "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/dji_linux_helpers.cpp" "/home/dmd/BitBucket/ros-slam/amr/build/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/CMakeFiles/dji_sdk_node.dir/dji_linux_helpers.cpp.o"
  "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/main.cpp" "/home/dmd/BitBucket/ros-slam/amr/build/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/CMakeFiles/dji_sdk_node.dir/main.cpp.o"
  "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/modules/dji_sdk_node.cpp" "/home/dmd/BitBucket/ros-slam/amr/build/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/CMakeFiles/dji_sdk_node.dir/modules/dji_sdk_node.cpp.o"
  "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/modules/dji_sdk_node_control.cpp" "/home/dmd/BitBucket/ros-slam/amr/build/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/CMakeFiles/dji_sdk_node.dir/modules/dji_sdk_node_control.cpp.o"
  "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/modules/dji_sdk_node_mission_services.cpp" "/home/dmd/BitBucket/ros-slam/amr/build/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/CMakeFiles/dji_sdk_node.dir/modules/dji_sdk_node_mission_services.cpp.o"
  "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/modules/dji_sdk_node_mobile_comm.cpp" "/home/dmd/BitBucket/ros-slam/amr/build/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/CMakeFiles/dji_sdk_node.dir/modules/dji_sdk_node_mobile_comm.cpp.o"
  "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/modules/dji_sdk_node_payload_comm.cpp" "/home/dmd/BitBucket/ros-slam/amr/build/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/CMakeFiles/dji_sdk_node.dir/modules/dji_sdk_node_payload_comm.cpp.o"
  "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/modules/dji_sdk_node_publisher.cpp" "/home/dmd/BitBucket/ros-slam/amr/build/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/CMakeFiles/dji_sdk_node.dir/modules/dji_sdk_node_publisher.cpp.o"
  "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/modules/dji_sdk_node_services.cpp" "/home/dmd/BitBucket/ros-slam/amr/build/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/CMakeFiles/dji_sdk_node.dir/modules/dji_sdk_node_services.cpp.o"
  "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/modules/dji_sdk_node_subscriber.cpp" "/home/dmd/BitBucket/ros-slam/amr/build/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/CMakeFiles/dji_sdk_node.dir/modules/dji_sdk_node_subscriber.cpp.o"
  "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/modules/dji_sdk_node_time_sync.cpp" "/home/dmd/BitBucket/ros-slam/amr/build/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/CMakeFiles/dji_sdk_node.dir/modules/dji_sdk_node_time_sync.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ADVANCED_SENSING"
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"dji_osdk_ros\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/dmd/BitBucket/ros-slam/amr/devel/include"
  "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/include"
  "/usr/include/libusb-1.0"
  "/opt/ros/melodic/include"
  "/opt/ros/melodic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/../../include/dji_osdk_ros_obsoleted"
  "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/src/dji_osdk_ros_obsoleted/../../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
