# CMake generated Testfile for 
# Source directory: /home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/src
# Build directory: /home/dmd/BitBucket/ros-slam/amr/build/Onboard-SDK-ROS/src
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("dji_osdk_ros")
subdirs("dji_osdk_ros_obsoleted")
