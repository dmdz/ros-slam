# CMake generated Testfile for 
# Source directory: /home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/src/dji_osdk_ros
# Build directory: /home/dmd/BitBucket/ros-slam/amr/build/Onboard-SDK-ROS/src/dji_osdk_ros
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("modules")
subdirs("samples")
