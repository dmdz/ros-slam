# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/src/dji_osdk_ros/samples/telemetry_node.cpp" "/home/dmd/BitBucket/ros-slam/amr/build/Onboard-SDK-ROS/src/dji_osdk_ros/samples/CMakeFiles/telemetry_node.dir/telemetry_node.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ADVANCED_SENSING"
  "OPEN_CV_INSTALLED"
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"dji_osdk_ros\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/dmd/BitBucket/ros-slam/amr/devel/include"
  "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/include"
  "/usr/include/libusb-1.0"
  "/opt/ros/melodic/include"
  "/opt/ros/melodic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/src/dji_osdk_ros/samples/stereo_utility"
  "/usr/local/include/opencv4"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/dmd/BitBucket/ros-slam/amr/build/Onboard-SDK-ROS/src/dji_osdk_ros/modules/CMakeFiles/dji_osdk_ros.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
