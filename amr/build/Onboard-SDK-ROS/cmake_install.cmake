# Install script for directory: /home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/dmd/BitBucket/ros-slam/amr/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/dji_osdk_ros/msg" TYPE FILE FILES
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/FCTimeInUTC.msg"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/GPSUTC.msg"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/Gimbal.msg"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/Waypoint.msg"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/WaypointList.msg"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/WaypointV2AircraftControlActuatorFlying.msg"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/WaypointV2AircraftControlActuatorRotateHeading.msg"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/WaypointV2AircraftControlActuator.msg"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/WaypointV2AssociateTrigger.msg"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/WaypointV2CameraActuatorFocalLengthParam.msg"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/WaypointV2CameraActuatorFocusParam.msg"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/WaypointV2CameraActuator.msg"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/WaypointV2Config.msg"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/WaypointV2GimbalActuatorRotationParam.msg"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/WaypointV2GimbalActuator.msg"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/WaypointV2InitSetting.msg"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/WaypointV2IntervalTrigger.msg"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/WaypointV2ReachpointTrigger.msg"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/WaypointV2SampleReachPointTrigger.msg"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/WaypointV2TrajectoryTrigger.msg"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/WaypointV2Action.msg"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/WaypointV2.msg"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/MobileData.msg"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/PayloadData.msg"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/MissionWaypointAction.msg"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/MissionWaypoint.msg"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/MissionWaypointTask.msg"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/MissionHotpointTask.msg"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/FlightAnomaly.msg"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/msg/VOPosition.msg"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/dji_osdk_ros/srv" TYPE FILE FILES
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/GetDroneType.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/GetM300StereoParams.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/FlightTaskControl.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/GimbalAction.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/CameraEV.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/CameraShutterSpeed.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/CameraAperture.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/CameraISO.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/CameraFocusPoint.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/CameraTapZoomPoint.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/CameraZoomCtrl.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/CameraStartShootSinglePhoto.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/CameraStartShootBurstPhoto.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/CameraStartShootAEBPhoto.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/CameraStartShootIntervalPhoto.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/CameraStopShootPhoto.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/CameraRecordVideoAction.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/MFIO.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/SetGoHomeAltitude.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/SetNewHomePoint.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/SetupCameraH264.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/AvoidEnable.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/InitWaypointV2Setting.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/UploadWaypointV2Mission.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/UploadWaypointV2Action.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/DownloadWaypointV2Mission.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/StartWaypointV2Mission.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/StopWaypointV2Mission.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/PauseWaypointV2Mission.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/ResumeWaypointV2Mission.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/GenerateWaypointV2Action.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/SetGlobalCruisespeed.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/GetGlobalCruisespeed.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/Activation.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/CameraAction.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/DroneTaskControl.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/SDKControlAuthority.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/SetLocalPosRef.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/MFIOConfig.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/MFIOSetValue.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/DroneArmControl.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/MissionStatus.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/MissionWpAction.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/MissionHpAction.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/MissionWpUpload.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/MissionWpSetSpeed.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/MissionWpGetSpeed.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/MissionWpGetInfo.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/MissionHpUpload.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/MissionHpGetInfo.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/MissionHpUpdateYawRate.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/MissionHpUpdateRadius.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/MissionHpResetYaw.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/SendMobileData.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/SendPayloadData.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/SetHardSync.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/QueryDroneVersion.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/Stereo240pSubscription.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/StereoVGASubscription.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/StereoDepthSubscription.srv"
    "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/srv/SetupCameraStream.srv"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/dji_osdk_ros/cmake" TYPE FILE FILES "/home/dmd/BitBucket/ros-slam/amr/build/Onboard-SDK-ROS/catkin_generated/installspace/dji_osdk_ros-msg-paths.cmake")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE DIRECTORY FILES "/home/dmd/BitBucket/ros-slam/amr/devel/include/dji_osdk_ros")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/roseus/ros" TYPE DIRECTORY FILES "/home/dmd/BitBucket/ros-slam/amr/devel/share/roseus/ros/dji_osdk_ros")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/common-lisp/ros" TYPE DIRECTORY FILES "/home/dmd/BitBucket/ros-slam/amr/devel/share/common-lisp/ros/dji_osdk_ros")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/gennodejs/ros" TYPE DIRECTORY FILES "/home/dmd/BitBucket/ros-slam/amr/devel/share/gennodejs/ros/dji_osdk_ros")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  execute_process(COMMAND "/usr/bin/python2" -m compileall "/home/dmd/BitBucket/ros-slam/amr/devel/lib/python2.7/dist-packages/dji_osdk_ros")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/python2.7/dist-packages" TYPE DIRECTORY FILES "/home/dmd/BitBucket/ros-slam/amr/devel/lib/python2.7/dist-packages/dji_osdk_ros")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/dmd/BitBucket/ros-slam/amr/build/Onboard-SDK-ROS/catkin_generated/installspace/dji_osdk_ros.pc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/dji_osdk_ros/cmake" TYPE FILE FILES "/home/dmd/BitBucket/ros-slam/amr/build/Onboard-SDK-ROS/catkin_generated/installspace/dji_osdk_ros-msg-extras.cmake")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/dji_osdk_ros/cmake" TYPE FILE FILES
    "/home/dmd/BitBucket/ros-slam/amr/build/Onboard-SDK-ROS/catkin_generated/installspace/dji_osdk_rosConfig.cmake"
    "/home/dmd/BitBucket/ros-slam/amr/build/Onboard-SDK-ROS/catkin_generated/installspace/dji_osdk_rosConfig-version.cmake"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/dji_osdk_ros" TYPE FILE FILES "/home/dmd/BitBucket/ros-slam/amr/src/Onboard-SDK-ROS/package.xml")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/dmd/BitBucket/ros-slam/amr/build/Onboard-SDK-ROS/src/cmake_install.cmake")

endif()

