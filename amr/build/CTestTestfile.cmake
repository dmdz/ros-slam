# CMake generated Testfile for 
# Source directory: /home/dmd/BitBucket/ros-slam/amr/src
# Build directory: /home/dmd/BitBucket/ros-slam/amr/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("mapper")
subdirs("perception")
subdirs("pfc")
subdirs("planner")
subdirs("simulator")
subdirs("slam")
subdirs("executive")
subdirs("gui")
subdirs("localization")
