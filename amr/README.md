To launch files:

source (amr-folder)/devel/setup.bash
roslaunch (amr-folder)/src/executive/launch/amr.launch

For simulation:
roslaunch (amr-folder)/src/executive/launch/amr_sim.launch