#include <iostream>
#include "simulator/simulator.h"
#include <math.h>
#include <stdlib.h>
#include <time.h> 
#include "perception/Landmarks.h"
#include "perception/Observations.h"
using namespace std;

geometry_msgs::Quaternion yaw_to_quaternion( const double& yaw ){
	geometry_msgs::Quaternion quaternion;
	quaternion.w = cos( yaw/2.0 );
	quaternion.x = 0.0;
	quaternion.y = 0.0;
	quaternion.z = sin( yaw/2.0 );
	return quaternion;
}
Simulator::Simulator() : _x( 0.0, 0.0, 0.0 ), _u( 0.0, 0.0, 0.0 ), _alpha1( 0.005 ), _alpha2( 0.05 ), _alpha3( 0.005 ), _alpha4( 0.05 ), _alpha5( 0.005 ), _alpha6( 0.05 ), _t( 0.0 ), _num_scan_angles( 128 )  {
}
Simulator::~Simulator() {
}
void Simulator::step( const double& dt ){
	Eigen::Vector3d _u_hat( _u );
	
	_u_hat(0) = _u(0) + ( ( _alpha1 * ( _u(0) * _u(0) ) ) + ( _alpha2 * ( _u(1) * _u(1) ) ) );
	_u_hat(1) = _u(1) + ( ( _alpha3 * ( _u(0) * _u(0) ) ) + ( _alpha4 * ( _u(1) * _u(1) ) ) );



	Eigen::Vector3d dx( 0.0 , 0.0 , 0.0 ); //x prime
	//fill this in
	if(_u_hat(1) != 0){
		dx(0) = ( ( ( -_u_hat(0) / _u_hat(1) ) * sin( _x(2) ) ) + ( ( _u_hat(0) / _u_hat(1) ) * sin( _x(2) + ( _u_hat(1) * dt ) ) ) );
		dx(1) = ( ( ( _u_hat(0) / _u_hat(1) ) * cos( _x(2) ) ) - ( ( _u_hat(0) / _u_hat(1) ) * cos( _x(2) + ( _u_hat(1) * dt ) ) ) );
		dx(2) = _u_hat(1) * dt;
	}
	else{
		dx(0) = ( _u_hat(0) * cos( _x(2) ) );
		dx(1) = ( _u_hat(0) * sin( _x(2) ) );
		dx(2) = 0;
	}

	_x += dx;
	_t += dt;

	std::cout << "_u[3]:{" << _u(0) << "," << _u(1) << "," << _u(2) << "}" << std::endl;
	std::cout << "_u_hat[3]:{" << _u_hat(0) << "," << _u_hat(1) << "," << _u_hat(2) << "}" << std::endl;
	std::cout << "_x[3]:{" << _x(0) << "," << _x(1) << "," << _x(2) << "}" << std::endl;
	std::cout << "_t:" << _t << std::endl << std::endl;
	return;
}
void Simulator::handle_command( const geometry_msgs::Twist::ConstPtr& msg ){
	_u( 0 ) = msg->linear.x;
	_u( 1 ) = msg->angular.z;
	return;
}

void Simulator::handle_obstacles( const geometry_msgs::Polygon::ConstPtr& msg ){
	_obstacles = *msg;
	return;
}

void Simulator::handle_landmarks( const perception::Landmarks::ConstPtr& msg ) {
	_landmarks = *msg;	
	return;
}

nav_msgs::Odometry Simulator::odometry_msg( void )const{
	nav_msgs::Odometry msg;
	msg.header.stamp = ros::Time::now();
	msg.pose.pose.position.x = _x( 0 );
	msg.pose.pose.position.y = _x( 1 );
	msg.pose.pose.position.z = 0.0;
	msg.pose.pose.orientation = yaw_to_quaternion( _x( 2 ) );
	msg.twist.twist.linear.x = _u( 0 );
	msg.twist.twist.angular.z = _u( 1 );
	return msg;
}

void Simulator::update_observations( void ) {

}

void Simulator::update_observed_landmarks( void ) {
	
}



double sample( const double& bsquared ){
	double tmp = 0.0;
	double b = 0.0;
	b = sqrt(bsquared);
	for(int i=0;i<12;i++){
		tmp += 0.5*(-b+2.0*b*(double)(rand()%1000)/(double)(1000));
	}
	return tmp;
}

sensor_msgs::LaserScan Simulator::scan_msg( void )const{
	sensor_msgs::LaserScan msg;
	msg.angle_min = -M_PI/4.0;
	msg.angle_max = M_PI/4.0;
	msg.angle_increment = ( msg.angle_max - msg.angle_min ) / ( double )(
		_num_scan_angles - 1 );
	msg.range_min = 0.1;
	msg.range_max = 5.0;;
	vector< double > scan_angles( _num_scan_angles );
	for( unsigned int i = 0; i < _num_scan_angles; i++ ){
		scan_angles[i] = msg.angle_min + ( double )( i ) * msg.angle_increment;
	}
	vector< double > obstacle_angles( _obstacles.points.size() );
	vector< double > obstacle_distances( _obstacles.points.size() );
	vector< double > obstacle_phimaxs( _obstacles.points.size() );
	for( unsigned int i = 0; i < _obstacles.points.size(); i++ ){
		obstacle_angles[ i ] = atan2( _obstacles.points[ i ].y - _x( 1 ),
			_obstacles.points[ i ].x - _x( 0 ) ) - _x( 2 );
		if( obstacle_angles[ i ] < -M_PI ){
			obstacle_angles[ i ] += 2.0 * M_PI;
		} else if ( obstacle_angles[ i ] > M_PI ){
			obstacle_angles[ i ] -= 2.0 * M_PI;
		}
		obstacle_distances[ i ] = sqrt( pow( _obstacles.points[ i ].x - _x( 0 ),
			2.0 ) + pow( _obstacles.points[ i ].y - _x( 1 ), 2.0 ) );
// inside of an obstacle
		if( obstacle_distances[ i ] < _obstacles.points[ i ].z ){
			return msg;
		}
		obstacle_phimaxs[ i ] = atan2( _obstacles.points[ i ].z, sqrt( pow(
			obstacle_distances[ i ], 2.0 ) - pow( _obstacles.points[ i ].z, 2.0 )
		) );
	}
	for( unsigned int i = 0; i < scan_angles.size(); i++ ){
		double min_range = msg.range_max;
		for( unsigned int j = 0; j < _obstacles.points.size(); j++ ){
// center of obstacle is within sensor range
			if( obstacle_distances[ j ] < ( msg.range_max + _obstacles.points[ j ].z) ){
// angle to see if in range range
				if( ( scan_angles[ i ] > ( obstacle_angles[ j ] - obstacle_phimaxs[ j] ) ) && ( scan_angles[ i ] < ( obstacle_angles[ j ] +	obstacle_phimaxs[ j ] ) ) ){
					double phi = scan_angles[ i ] - obstacle_angles[ j ];
					double a = 1.0 + pow( tan( phi ), 2.0 );
					double b = -2.0 * obstacle_distances[ j ];
					double c = pow( obstacle_distances[ j ], 2.0 ) - pow( _obstacles.points[ j ].z, 2.0 );
// candidate intersection points
					double x1 = ( -b + sqrt( pow( b, 2.0 ) - 4 * a * c ) ) / ( 2.0 * a );
					double y1 = tan( phi ) * x1;
					double d1squared = pow( x1, 2.0 ) + pow( y1, 2.0 );
					double x2 = ( -b - sqrt( pow( b, 2.0 ) - 4 * a * c ) ) / ( 2.0 * a );
					double y2 = tan( phi ) * x2;
					double d2squared = pow( x2, 2.0 ) + pow( y2, 2.0 );
					double range = 0.0;
					if( d1squared < d2squared ){
						range = sqrt( d1squared );
					} else {
						range = sqrt( d2squared );
					}
					if( range < min_range ){
						min_range = range;
					}
				}
			}
		}
		if( min_range > msg.range_min ){
			msg.ranges.push_back( std::min( min_range + sample( 0.001 ), ( double )(
				msg.range_max ) ) );
		} else {
			msg.ranges.push_back( 0.0 );
		}
	}
}
geometry_msgs::Polygon Simulator::simulated_obstacles_msg( void )const{
	return _obstacles;
}