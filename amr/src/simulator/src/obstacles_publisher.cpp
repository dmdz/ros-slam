#include <iostream>
#include "ros/ros.h"
#include "geometry_msgs/Polygon.h"
using namespace std;
int main( int argc, char* argv[] ){
	geometry_msgs::Polygon obstacles;

    //Obstacles to avoid when navigating waypoints
	// obstacles.points.push_back( geometry_msgs::Point32() );
 //    obstacles.points.back().x = 1.5;
 //    obstacles.points.back().y = 1.5;
 //    obstacles.points.back().z = 1.0;
 //    obstacles.points.push_back( geometry_msgs::Point32() );
 //    obstacles.points.back().x = -1.5;
 //    obstacles.points.back().y = 1.5;
 //    obstacles.points.back().z = 0.5;
 //    obstacles.points.push_back( geometry_msgs::Point32() );
 //    obstacles.points.back().x = -1.5;
 //    obstacles.points.back().y = -1.5;
 //    obstacles.points.back().z = 0.5;
 //    obstacles.points.push_back( geometry_msgs::Point32() );
 //    obstacles.points.back().x = 1.5;
 //    obstacles.points.back().y = -1.5;
 //    obstacles.points.back().z = 0.25;

	unsigned int num_obstacles = 0;
	double min_x = -1.0;
	double max_x = 1.25;
	double min_y = -1.0;
	double max_y = 1.25;
	double min_radius = 0.03;
	double max_radius = 0.07;

	for( unsigned int i = 0; i < num_obstacles; i++ ){
		obstacles.points.push_back( geometry_msgs::Point32() );
		obstacles.points.back().x = min_x + ( double )( rand() % 101 ) / ( 100.0 )
		* ( max_x - min_x );
		obstacles.points.back().y = min_y + ( double )( rand() % 101 ) / ( 100.0 )
		* ( max_y - min_y );
		obstacles.points.back().z = min_radius + ( double )( rand() % 101 ) / (
			100.0 ) * ( max_radius - min_radius );
	}

	ros::init( argc, argv, "obstacles_publisher_node" );
	ros::NodeHandle node_handle;
	ros::Publisher obstacles_publisher = node_handle.advertise< geometry_msgs::Polygon >( "obstacles", 1, true );
	//sleep( 1 );
	obstacles_publisher.publish( obstacles );
	sleep( 1 );
	
	return 0;
}
