#ifndef PLANNER_XY_H
#define PLANNER_XY_H

#include "ros/ros.h"
#include "geometry_msgs/Pose.h"
#include "nav_msgs/Odometry.h"
#include "nav_msgs/Path.h"
#include "mapper/mapper.h"
#include "map_msgs/OccupancyGridUpdate.h"

#include <iostream>
#include <vector>
#include <deque>


using namespace std;

class Node {
public:        
    Node( double _x = 0.0, double _y = 0.0,double _g = 0.0,double _h = 0.0, int _id = -1, int _backpointer = -1 ) : x(_x), y(_y), g(_g), h(_h), f(_g+_h), id(_id), backpointer(_backpointer) {};
    virtual ~Node(){};

    int backpointer;
    int id;
    double x;
    double y;
    double g;
    double h;
    double f;
    


};

std::ostream& operator<<( std::ostream& out, const Node& other ){
   out << other.x << "," << other.y << "," << other.g << "," << other.h << "," << other.f << "," << other.backpointer << "," << other.id;
   return out;
}

struct sorting_by_f //sort list
{
    inline bool operator() (const Node& struct1, const Node& struct2) {
        return (struct1.f > struct2.f);
    }
};


class PlannerXY{
public:

        //Check Map of Planner 
    bool check_map(const int& x, const int& y ); 
    double discretization;

        //Node Lists
    vector<Node> open_list;
    vector<Node> closed_list;

    Node first_node;
    Node goal_node;
    Node current_node;

    
    PlannerXY();
    
    virtual ~PlannerXY();

        // new subscriber callback functions
    void handle_odom( const nav_msgs::Odometry::ConstPtr& msg );
    void handle_goal( const geometry_msgs::Pose::ConstPtr& msg );
    void handle_map( const nav_msgs::OccupancyGrid::ConstPtr& msg );
    void expand( std::deque< Node* >& open, std::vector< Node* >& closed, Node* node, Node* goal );

        //search function
    bool search( const geometry_msgs::Pose& start, const geometry_msgs::Pose& goal, const double& w);

        // generate path function
    nav_msgs::Path generate_path( void );
    geometry_msgs::PoseStamped create_pose( const double& x, const double& y );

    bool is_in_list( Node& node ); 
    Node get_node(int& id);

        // publishing path, openlistsize, and closedlistsize and storing the nav_msgs::Odometry message
    ros::Publisher path_publisher;
    ros::Publisher openlistsize_publisher;
    ros::Publisher closedlistsize_publisher;
    nav_msgs::Odometry odometry;
    //double discretization;
    OCMap ocmap;
};
#endif /* PLANNER_XY_H */