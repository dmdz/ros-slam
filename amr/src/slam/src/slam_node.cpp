#include <iostream>
#include "ros/ros.h"
#include "nav_msgs/Odometry.h"
#include "slam/slam.h"
using namespace std;
int main( int argc, char *argv[] ){
	Eigen::VectorXd alpha = Eigen::VectorXd::Zero( 6 );
	alpha( 0 ) = 0.01;
	alpha( 1 ) = 0.01;
	alpha( 2 ) = 0.01;
	alpha( 3 ) = 0.01;
	alpha( 4 ) = 0.01;
	alpha( 5 ) = 0.01;
	
	Eigen::MatrixXd q = Eigen::MatrixXd::Zero( 3, 3 );
	q( 0, 0 ) = 0.01 * 0.01;
	q( 1, 1 ) = 0.01 * 0.01;
	q( 2, 2 ) = 0.01 * 0.01;

	SLAM slam( alpha, q );
	ros::init( argc, argv, "slam_node" );
	ros::NodeHandle node_handle;
    //  ros::Subscriber command_subscriber = node_handle.subscribe( "cmd_vel_mux/input/navi", 1, &EKF_Localization::handle_command, &ekf_localization );
	ros::Subscriber odometry_subscriber = node_handle.subscribe( "odom", 1, &SLAM::handle_odometry, &slam );
	ros::Subscriber landmarks_subscriber = node_handle.subscribe( "landmarks", 1, &SLAM::handle_landmarks, &slam );
	ros::Subscriber observations_subscriber = node_handle.subscribe( "observations", 1, &SLAM::handle_observations, &slam );
	
	ros::Publisher estimated_odometry_publisher = node_handle.advertise< nav_msgs::Odometry >( "estimated_odom", 1, true );
	sleep( 1 );
	
	double frequency = 50.0;
	ros::Rate timer( frequency );
	while( ros::ok() ){
		slam.step( 1.0/frequency );
		estimated_odometry_publisher.publish( slam.estimated_odometry());
		ros::spinOnce();
		timer.sleep();
	}
	return 0;
}
