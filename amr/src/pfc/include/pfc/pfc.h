#ifndef PFC
#define PFC
#include <iostream>
#include <deque>
#include "ros/ros.h"
#include "nav_msgs/Odometry.h"
#include "nav_msgs/Path.h"

class Pfc {
public:
	Pfc();
	virtual ~Pfc();
	void updatePathIndex( void );
	void updateCommand( void );
	void handleOdom( const nav_msgs::Odometry::ConstPtr& msg );
	void handlePath( const nav_msgs::Path::ConstPtr& msg );
	
	nav_msgs::Odometry odometry;    
	nav_msgs::Path path;
	geometry_msgs::Twist command;
	nav_msgs::Path projection;
	geometry_msgs::Point lookahead;

	double max_speed;
	int path_index;
	double distance_threshold;
	double angle_threshold;
};

std::ostream& operator<<( std::ostream& out, const Pfc& other );

#endif /* PFC */
