cmake_minimum_required(VERSION 2.8.3)
project(pfc)

find_package(catkin REQUIRED COMPONENTS
	geometry_msgs
	message_generation
	message_runtime
	nav_msgs
	roscpp
	rospy
	std_msgs
	std_msgs
)

catkin_package(
	INCLUDE_DIRS include
	LIBRARIES pfc
	std_msgs
)



include_directories(include)
include_directories(${catkin_INCLUDE_DIRS})


add_library(${PROJECT_NAME} src/${PROJECT_NAME}/pfc.cpp)

add_dependencies(${PROJECT_NAME} ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})


add_executable(pfc_node src/pfc_node.cpp)



add_dependencies(pfc_node ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})





target_link_libraries(pfc_node
${PROJECT_NAME}
${catkin_LIBRARIES}
)

