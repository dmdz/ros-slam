#ifndef GUI_H
#define GUI_H

#include <iostream>
#include "ros/ros.h"

#include <QtOpenGL/QGLWidget>
#include <QtGui/QKeyEvent>
#include <QtCore/QTimer>

#include "nav_msgs/Path.h"
#include "nav_msgs/Odometry.h"
#include "sensor_msgs/LaserScan.h"
#include "map_msgs/OccupancyGridUpdate.h"
#include "nav_msgs/OccupancyGrid.h"


#include "perception/Landmarks.h"
#include "perception/Observations.h"
#include "geometry_msgs/Polygon.h"

class GUI: public QGLWidget {
        Q_OBJECT
public:
        GUI( QWidget * parent = NULL );
        virtual ~GUI();
        void handleLaserScan( const sensor_msgs::LaserScan::ConstPtr& msg );//
        void handleOdom( const nav_msgs::Odometry::ConstPtr& msg );//
        void handleEstimatedOdom( const nav_msgs::Odometry::ConstPtr& msg );//
        void handleGoal( const geometry_msgs::Pose::ConstPtr& msg );//
        void handleLookahead( const geometry_msgs::Point::ConstPtr& msg );//
        void handlePath( const nav_msgs::Path::ConstPtr& msg );//
        void handleProjection( const nav_msgs::Path::ConstPtr& msg );
        void handleLandmarks( const perception::Landmarks::ConstPtr& msg );//
        void handleObservedLandmarks( const perception::Observations::ConstPtr& msg );
        void handleMap( const nav_msgs::OccupancyGrid::ConstPtr& msg );
        void handleSimulatedObstacles( const geometry_msgs::Polygon::ConstPtr& msg);
        void drawSquare( const double& center_x, const double& center_y );

        protected slots:
        void timer_callback( void );

protected:
        virtual void initializeGL();
        virtual void resizeGL( int width, int height );
        virtual void paintGL();
        void drawCoordinateSystem( void );
        void drawGrid();
        void drawPoint( const geometry_msgs::Point& point, const double& red =
                0.0, const double& green = 0.0, const double& blue = 0.0, const double & size = 1.0 );
        void drawLaserScan( const geometry_msgs::Pose& pose, const sensor_msgs::LaserScan& laserscan, const double& red, const double& green, const
                double& blue );
        void drawRobot( const geometry_msgs::Pose& pose, const double& red = 0.0,
                const double& green = 0.0, const double& blue = 0.0, const double&
                radius = 0.1225 );
        void drawRobotSensorHorizon( const geometry_msgs::Pose& pose,const double& radius,
                const double& angle );
        void drawPath( const nav_msgs::Path& path, const double& red = 0.0, const
                double& green = 0.0, const double& blue = 0.0, const double& width =1.0 );
        void drawLandmarks( const perception::Landmarks& _landmarks,  const double& red, const double& green, const double& blue, const double& size);
        void drawObservations( const perception::Observations& _observed_landmarks,  const double& red, const double& green, const double& blue, const double& size);
        void drawMap( const nav_msgs::OccupancyGrid& map, const double& r,
                const double& g, const double& b );
        virtual void keyPressEvent(QKeyEvent * event);
        virtual void drawObstacles( const geometry_msgs::Polygon& obstacles, const
                double& r, const double& g, const double& b );

        void drawPoint32( const geometry_msgs::Point32& point, const double& red, const double& green, const double& blue, const double& size );
        QTimer _timer;
        double _zoom;
        std::pair< double, double > _center;
        sensor_msgs::LaserScan _laserscan;
        nav_msgs::Odometry _odom;
        nav_msgs::Odometry _estimated_odom;
        nav_msgs::OccupancyGrid _map;
        geometry_msgs::Pose _goal;
        geometry_msgs::Point _lookahead;
        nav_msgs::Path _path;
        nav_msgs::Path _projection;
        perception::Landmarks _landmarks;
        perception::Observations _observed_landmarks;
        std::map< int, geometry_msgs::Point > _observed_landmarks_map;
        geometry_msgs::Polygon _simulated_obstacles;
};
#endif /* GUI_H */


