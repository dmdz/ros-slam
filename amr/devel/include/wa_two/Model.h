// Generated by gencpp from file wa_two/Model.msg
// DO NOT EDIT!


#ifndef WA_TWO_MESSAGE_MODEL_H
#define WA_TWO_MESSAGE_MODEL_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace wa_two
{
template <class ContainerAllocator>
struct Model_
{
  typedef Model_<ContainerAllocator> Type;

  Model_()
    : Xi0_0_0(0.0)
    , Xi0_1_0(0.0)
    , Ohm0_0_0(0.0)
    , Ohm0_0_1(0.0)
    , Ohm0_1_0(0.0)
    , Ohm0_1_1(0.0)
    , A_0_0(0.0)
    , A_0_1(0.0)
    , A_1_0(0.0)
    , A_1_1(0.0)
    , B_0_0(0.0)
    , B_0_1(0.0)
    , B_1_0(0.0)
    , B_1_1(0.0)
    , C_0_0(0.0)
    , C_0_1(0.0)
    , C_1_0(0.0)
    , C_1_1(0.0)
    , R_0_0(0.0)
    , R_0_1(0.0)
    , R_1_0(0.0)
    , R_1_1(0.0)
    , Q_0_0(0.0)
    , Q_0_1(0.0)
    , Q_1_0(0.0)
    , Q_1_1(0.0)  {
    }
  Model_(const ContainerAllocator& _alloc)
    : Xi0_0_0(0.0)
    , Xi0_1_0(0.0)
    , Ohm0_0_0(0.0)
    , Ohm0_0_1(0.0)
    , Ohm0_1_0(0.0)
    , Ohm0_1_1(0.0)
    , A_0_0(0.0)
    , A_0_1(0.0)
    , A_1_0(0.0)
    , A_1_1(0.0)
    , B_0_0(0.0)
    , B_0_1(0.0)
    , B_1_0(0.0)
    , B_1_1(0.0)
    , C_0_0(0.0)
    , C_0_1(0.0)
    , C_1_0(0.0)
    , C_1_1(0.0)
    , R_0_0(0.0)
    , R_0_1(0.0)
    , R_1_0(0.0)
    , R_1_1(0.0)
    , Q_0_0(0.0)
    , Q_0_1(0.0)
    , Q_1_0(0.0)
    , Q_1_1(0.0)  {
  (void)_alloc;
    }



   typedef float _Xi0_0_0_type;
  _Xi0_0_0_type Xi0_0_0;

   typedef float _Xi0_1_0_type;
  _Xi0_1_0_type Xi0_1_0;

   typedef float _Ohm0_0_0_type;
  _Ohm0_0_0_type Ohm0_0_0;

   typedef float _Ohm0_0_1_type;
  _Ohm0_0_1_type Ohm0_0_1;

   typedef float _Ohm0_1_0_type;
  _Ohm0_1_0_type Ohm0_1_0;

   typedef float _Ohm0_1_1_type;
  _Ohm0_1_1_type Ohm0_1_1;

   typedef float _A_0_0_type;
  _A_0_0_type A_0_0;

   typedef float _A_0_1_type;
  _A_0_1_type A_0_1;

   typedef float _A_1_0_type;
  _A_1_0_type A_1_0;

   typedef float _A_1_1_type;
  _A_1_1_type A_1_1;

   typedef float _B_0_0_type;
  _B_0_0_type B_0_0;

   typedef float _B_0_1_type;
  _B_0_1_type B_0_1;

   typedef float _B_1_0_type;
  _B_1_0_type B_1_0;

   typedef float _B_1_1_type;
  _B_1_1_type B_1_1;

   typedef float _C_0_0_type;
  _C_0_0_type C_0_0;

   typedef float _C_0_1_type;
  _C_0_1_type C_0_1;

   typedef float _C_1_0_type;
  _C_1_0_type C_1_0;

   typedef float _C_1_1_type;
  _C_1_1_type C_1_1;

   typedef float _R_0_0_type;
  _R_0_0_type R_0_0;

   typedef float _R_0_1_type;
  _R_0_1_type R_0_1;

   typedef float _R_1_0_type;
  _R_1_0_type R_1_0;

   typedef float _R_1_1_type;
  _R_1_1_type R_1_1;

   typedef float _Q_0_0_type;
  _Q_0_0_type Q_0_0;

   typedef float _Q_0_1_type;
  _Q_0_1_type Q_0_1;

   typedef float _Q_1_0_type;
  _Q_1_0_type Q_1_0;

   typedef float _Q_1_1_type;
  _Q_1_1_type Q_1_1;





  typedef boost::shared_ptr< ::wa_two::Model_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::wa_two::Model_<ContainerAllocator> const> ConstPtr;

}; // struct Model_

typedef ::wa_two::Model_<std::allocator<void> > Model;

typedef boost::shared_ptr< ::wa_two::Model > ModelPtr;
typedef boost::shared_ptr< ::wa_two::Model const> ModelConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::wa_two::Model_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::wa_two::Model_<ContainerAllocator> >::stream(s, "", v);
return s;
}


template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator==(const ::wa_two::Model_<ContainerAllocator1> & lhs, const ::wa_two::Model_<ContainerAllocator2> & rhs)
{
  return lhs.Xi0_0_0 == rhs.Xi0_0_0 &&
    lhs.Xi0_1_0 == rhs.Xi0_1_0 &&
    lhs.Ohm0_0_0 == rhs.Ohm0_0_0 &&
    lhs.Ohm0_0_1 == rhs.Ohm0_0_1 &&
    lhs.Ohm0_1_0 == rhs.Ohm0_1_0 &&
    lhs.Ohm0_1_1 == rhs.Ohm0_1_1 &&
    lhs.A_0_0 == rhs.A_0_0 &&
    lhs.A_0_1 == rhs.A_0_1 &&
    lhs.A_1_0 == rhs.A_1_0 &&
    lhs.A_1_1 == rhs.A_1_1 &&
    lhs.B_0_0 == rhs.B_0_0 &&
    lhs.B_0_1 == rhs.B_0_1 &&
    lhs.B_1_0 == rhs.B_1_0 &&
    lhs.B_1_1 == rhs.B_1_1 &&
    lhs.C_0_0 == rhs.C_0_0 &&
    lhs.C_0_1 == rhs.C_0_1 &&
    lhs.C_1_0 == rhs.C_1_0 &&
    lhs.C_1_1 == rhs.C_1_1 &&
    lhs.R_0_0 == rhs.R_0_0 &&
    lhs.R_0_1 == rhs.R_0_1 &&
    lhs.R_1_0 == rhs.R_1_0 &&
    lhs.R_1_1 == rhs.R_1_1 &&
    lhs.Q_0_0 == rhs.Q_0_0 &&
    lhs.Q_0_1 == rhs.Q_0_1 &&
    lhs.Q_1_0 == rhs.Q_1_0 &&
    lhs.Q_1_1 == rhs.Q_1_1;
}

template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator!=(const ::wa_two::Model_<ContainerAllocator1> & lhs, const ::wa_two::Model_<ContainerAllocator2> & rhs)
{
  return !(lhs == rhs);
}


} // namespace wa_two

namespace ros
{
namespace message_traits
{





template <class ContainerAllocator>
struct IsFixedSize< ::wa_two::Model_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::wa_two::Model_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::wa_two::Model_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::wa_two::Model_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::wa_two::Model_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::wa_two::Model_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::wa_two::Model_<ContainerAllocator> >
{
  static const char* value()
  {
    return "86a1455dd7ccb59c04c3e718684e2f4f";
  }

  static const char* value(const ::wa_two::Model_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x86a1455dd7ccb59cULL;
  static const uint64_t static_value2 = 0x04c3e718684e2f4fULL;
};

template<class ContainerAllocator>
struct DataType< ::wa_two::Model_<ContainerAllocator> >
{
  static const char* value()
  {
    return "wa_two/Model";
  }

  static const char* value(const ::wa_two::Model_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::wa_two::Model_<ContainerAllocator> >
{
  static const char* value()
  {
    return "float32 Xi0_0_0\n"
"float32 Xi0_1_0\n"
"\n"
"float32 Ohm0_0_0\n"
"float32 Ohm0_0_1\n"
"float32 Ohm0_1_0\n"
"float32 Ohm0_1_1\n"
"\n"
"float32 A_0_0\n"
"float32 A_0_1\n"
"float32 A_1_0\n"
"float32 A_1_1\n"
"\n"
"float32 B_0_0\n"
"float32 B_0_1\n"
"float32 B_1_0\n"
"float32 B_1_1\n"
"\n"
"float32 C_0_0\n"
"float32 C_0_1\n"
"float32 C_1_0\n"
"float32 C_1_1\n"
"\n"
"float32 R_0_0\n"
"float32 R_0_1\n"
"float32 R_1_0\n"
"float32 R_1_1\n"
"\n"
"float32 Q_0_0\n"
"float32 Q_0_1\n"
"float32 Q_1_0\n"
"float32 Q_1_1\n"
;
  }

  static const char* value(const ::wa_two::Model_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::wa_two::Model_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.Xi0_0_0);
      stream.next(m.Xi0_1_0);
      stream.next(m.Ohm0_0_0);
      stream.next(m.Ohm0_0_1);
      stream.next(m.Ohm0_1_0);
      stream.next(m.Ohm0_1_1);
      stream.next(m.A_0_0);
      stream.next(m.A_0_1);
      stream.next(m.A_1_0);
      stream.next(m.A_1_1);
      stream.next(m.B_0_0);
      stream.next(m.B_0_1);
      stream.next(m.B_1_0);
      stream.next(m.B_1_1);
      stream.next(m.C_0_0);
      stream.next(m.C_0_1);
      stream.next(m.C_1_0);
      stream.next(m.C_1_1);
      stream.next(m.R_0_0);
      stream.next(m.R_0_1);
      stream.next(m.R_1_0);
      stream.next(m.R_1_1);
      stream.next(m.Q_0_0);
      stream.next(m.Q_0_1);
      stream.next(m.Q_1_0);
      stream.next(m.Q_1_1);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct Model_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::wa_two::Model_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::wa_two::Model_<ContainerAllocator>& v)
  {
    s << indent << "Xi0_0_0: ";
    Printer<float>::stream(s, indent + "  ", v.Xi0_0_0);
    s << indent << "Xi0_1_0: ";
    Printer<float>::stream(s, indent + "  ", v.Xi0_1_0);
    s << indent << "Ohm0_0_0: ";
    Printer<float>::stream(s, indent + "  ", v.Ohm0_0_0);
    s << indent << "Ohm0_0_1: ";
    Printer<float>::stream(s, indent + "  ", v.Ohm0_0_1);
    s << indent << "Ohm0_1_0: ";
    Printer<float>::stream(s, indent + "  ", v.Ohm0_1_0);
    s << indent << "Ohm0_1_1: ";
    Printer<float>::stream(s, indent + "  ", v.Ohm0_1_1);
    s << indent << "A_0_0: ";
    Printer<float>::stream(s, indent + "  ", v.A_0_0);
    s << indent << "A_0_1: ";
    Printer<float>::stream(s, indent + "  ", v.A_0_1);
    s << indent << "A_1_0: ";
    Printer<float>::stream(s, indent + "  ", v.A_1_0);
    s << indent << "A_1_1: ";
    Printer<float>::stream(s, indent + "  ", v.A_1_1);
    s << indent << "B_0_0: ";
    Printer<float>::stream(s, indent + "  ", v.B_0_0);
    s << indent << "B_0_1: ";
    Printer<float>::stream(s, indent + "  ", v.B_0_1);
    s << indent << "B_1_0: ";
    Printer<float>::stream(s, indent + "  ", v.B_1_0);
    s << indent << "B_1_1: ";
    Printer<float>::stream(s, indent + "  ", v.B_1_1);
    s << indent << "C_0_0: ";
    Printer<float>::stream(s, indent + "  ", v.C_0_0);
    s << indent << "C_0_1: ";
    Printer<float>::stream(s, indent + "  ", v.C_0_1);
    s << indent << "C_1_0: ";
    Printer<float>::stream(s, indent + "  ", v.C_1_0);
    s << indent << "C_1_1: ";
    Printer<float>::stream(s, indent + "  ", v.C_1_1);
    s << indent << "R_0_0: ";
    Printer<float>::stream(s, indent + "  ", v.R_0_0);
    s << indent << "R_0_1: ";
    Printer<float>::stream(s, indent + "  ", v.R_0_1);
    s << indent << "R_1_0: ";
    Printer<float>::stream(s, indent + "  ", v.R_1_0);
    s << indent << "R_1_1: ";
    Printer<float>::stream(s, indent + "  ", v.R_1_1);
    s << indent << "Q_0_0: ";
    Printer<float>::stream(s, indent + "  ", v.Q_0_0);
    s << indent << "Q_0_1: ";
    Printer<float>::stream(s, indent + "  ", v.Q_0_1);
    s << indent << "Q_1_0: ";
    Printer<float>::stream(s, indent + "  ", v.Q_1_0);
    s << indent << "Q_1_1: ";
    Printer<float>::stream(s, indent + "  ", v.Q_1_1);
  }
};

} // namespace message_operations
} // namespace ros

#endif // WA_TWO_MESSAGE_MODEL_H
