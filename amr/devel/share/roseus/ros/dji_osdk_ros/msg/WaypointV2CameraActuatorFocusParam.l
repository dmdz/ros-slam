;; Auto-generated. Do not edit!


(when (boundp 'dji_osdk_ros::WaypointV2CameraActuatorFocusParam)
  (if (not (find-package "DJI_OSDK_ROS"))
    (make-package "DJI_OSDK_ROS"))
  (shadow 'WaypointV2CameraActuatorFocusParam (find-package "DJI_OSDK_ROS")))
(unless (find-package "DJI_OSDK_ROS::WAYPOINTV2CAMERAACTUATORFOCUSPARAM")
  (make-package "DJI_OSDK_ROS::WAYPOINTV2CAMERAACTUATORFOCUSPARAM"))

(in-package "ROS")
;;//! \htmlinclude WaypointV2CameraActuatorFocusParam.msg.html


(intern "*RETRYTIMES*" (find-package "DJI_OSDK_ROS::WAYPOINTV2CAMERAACTUATORFOCUSPARAM"))
(shadow '*RETRYTIMES* (find-package "DJI_OSDK_ROS::WAYPOINTV2CAMERAACTUATORFOCUSPARAM"))
(defconstant dji_osdk_ros::WaypointV2CameraActuatorFocusParam::*RETRYTIMES* 1)
(intern "*FOCUSDELAYTIME*" (find-package "DJI_OSDK_ROS::WAYPOINTV2CAMERAACTUATORFOCUSPARAM"))
(shadow '*FOCUSDELAYTIME* (find-package "DJI_OSDK_ROS::WAYPOINTV2CAMERAACTUATORFOCUSPARAM"))
(defconstant dji_osdk_ros::WaypointV2CameraActuatorFocusParam::*FOCUSDELAYTIME* 0)
(defclass dji_osdk_ros::WaypointV2CameraActuatorFocusParam
  :super ros::object
  :slots (_x _y ))

(defmethod dji_osdk_ros::WaypointV2CameraActuatorFocusParam
  (:init
   (&key
    ((:x __x) 0)
    ((:y __y) 0)
    )
   (send-super :init)
   (setq _x (round __x))
   (setq _y (round __y))
   self)
  (:x
   (&optional __x)
   (if __x (setq _x __x)) _x)
  (:y
   (&optional __y)
   (if __y (setq _y __y)) _y)
  (:serialization-length
   ()
   (+
    ;; uint16 _x
    2
    ;; uint16 _y
    2
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; uint16 _x
       (write-word _x s)
     ;; uint16 _y
       (write-word _y s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; uint16 _x
     (setq _x (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint16 _y
     (setq _y (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;;
   self)
  )

(setf (get dji_osdk_ros::WaypointV2CameraActuatorFocusParam :md5sum-) "3ae752ecdbd7e3838b2e6e28ece598e3")
(setf (get dji_osdk_ros::WaypointV2CameraActuatorFocusParam :datatype-) "dji_osdk_ros/WaypointV2CameraActuatorFocusParam")
(setf (get dji_osdk_ros::WaypointV2CameraActuatorFocusParam :definition-)
      "# This class defines a camera focus operation for ``DJIWaypointV2Action_DJIWaypointV2CameraActuatorParam``.
# The lens focus target point. When the focus mode is auto, the target point
# is the focal point. When the focus mode is manual, the target point is the zoom
# out area if the focus assistant is enabled for the manual mode.
#  The range for x and y is from 0.0 to 1.0. The point [0.0, 0.0] represents the top-left angle of the screen.
  
  uint16 x # x axis focus point value.range: [0,10000]
  uint16 y # y axis focus point value.range: [0,10000]
  uint8 retryTimes = 1
  uint8 focusDelayTime = 0
")



(provide :dji_osdk_ros/WaypointV2CameraActuatorFocusParam "3ae752ecdbd7e3838b2e6e28ece598e3")


