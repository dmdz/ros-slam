;; Auto-generated. Do not edit!


(when (boundp 'dji_osdk_ros::SetNewHomePoint)
  (if (not (find-package "DJI_OSDK_ROS"))
    (make-package "DJI_OSDK_ROS"))
  (shadow 'SetNewHomePoint (find-package "DJI_OSDK_ROS")))
(unless (find-package "DJI_OSDK_ROS::SETNEWHOMEPOINT")
  (make-package "DJI_OSDK_ROS::SETNEWHOMEPOINT"))
(unless (find-package "DJI_OSDK_ROS::SETNEWHOMEPOINTREQUEST")
  (make-package "DJI_OSDK_ROS::SETNEWHOMEPOINTREQUEST"))
(unless (find-package "DJI_OSDK_ROS::SETNEWHOMEPOINTRESPONSE")
  (make-package "DJI_OSDK_ROS::SETNEWHOMEPOINTRESPONSE"))

(in-package "ROS")





(defclass dji_osdk_ros::SetNewHomePointRequest
  :super ros::object
  :slots ())

(defmethod dji_osdk_ros::SetNewHomePointRequest
  (:init
   (&key
    )
   (send-super :init)
   self)
  (:serialization-length
   ()
   (+
    0
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;;
   self)
  )

(defclass dji_osdk_ros::SetNewHomePointResponse
  :super ros::object
  :slots (_result ))

(defmethod dji_osdk_ros::SetNewHomePointResponse
  (:init
   (&key
    ((:result __result) nil)
    )
   (send-super :init)
   (setq _result __result)
   self)
  (:result
   (&optional __result)
   (if __result (setq _result __result)) _result)
  (:serialization-length
   ()
   (+
    ;; bool _result
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _result
       (if _result (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _result
     (setq _result (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(defclass dji_osdk_ros::SetNewHomePoint
  :super ros::object
  :slots ())

(setf (get dji_osdk_ros::SetNewHomePoint :md5sum-) "eb13ac1f1354ccecb7941ee8fa2192e8")
(setf (get dji_osdk_ros::SetNewHomePoint :datatype-) "dji_osdk_ros/SetNewHomePoint")
(setf (get dji_osdk_ros::SetNewHomePoint :request) dji_osdk_ros::SetNewHomePointRequest)
(setf (get dji_osdk_ros::SetNewHomePoint :response) dji_osdk_ros::SetNewHomePointResponse)

(defmethod dji_osdk_ros::SetNewHomePointRequest
  (:response () (instance dji_osdk_ros::SetNewHomePointResponse :init)))

(setf (get dji_osdk_ros::SetNewHomePointRequest :md5sum-) "eb13ac1f1354ccecb7941ee8fa2192e8")
(setf (get dji_osdk_ros::SetNewHomePointRequest :datatype-) "dji_osdk_ros/SetNewHomePointRequest")
(setf (get dji_osdk_ros::SetNewHomePointRequest :definition-)
      "---
bool result
")

(setf (get dji_osdk_ros::SetNewHomePointResponse :md5sum-) "eb13ac1f1354ccecb7941ee8fa2192e8")
(setf (get dji_osdk_ros::SetNewHomePointResponse :datatype-) "dji_osdk_ros/SetNewHomePointResponse")
(setf (get dji_osdk_ros::SetNewHomePointResponse :definition-)
      "---
bool result
")



(provide :dji_osdk_ros/SetNewHomePoint "eb13ac1f1354ccecb7941ee8fa2192e8")


