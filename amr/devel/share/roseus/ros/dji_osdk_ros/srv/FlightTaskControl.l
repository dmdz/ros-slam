;; Auto-generated. Do not edit!


(when (boundp 'dji_osdk_ros::FlightTaskControl)
  (if (not (find-package "DJI_OSDK_ROS"))
    (make-package "DJI_OSDK_ROS"))
  (shadow 'FlightTaskControl (find-package "DJI_OSDK_ROS")))
(unless (find-package "DJI_OSDK_ROS::FLIGHTTASKCONTROL")
  (make-package "DJI_OSDK_ROS::FLIGHTTASKCONTROL"))
(unless (find-package "DJI_OSDK_ROS::FLIGHTTASKCONTROLREQUEST")
  (make-package "DJI_OSDK_ROS::FLIGHTTASKCONTROLREQUEST"))
(unless (find-package "DJI_OSDK_ROS::FLIGHTTASKCONTROLRESPONSE")
  (make-package "DJI_OSDK_ROS::FLIGHTTASKCONTROLRESPONSE"))

(in-package "ROS")





(intern "*TASK_GOHOME*" (find-package "DJI_OSDK_ROS::FLIGHTTASKCONTROLREQUEST"))
(shadow '*TASK_GOHOME* (find-package "DJI_OSDK_ROS::FLIGHTTASKCONTROLREQUEST"))
(defconstant dji_osdk_ros::FlightTaskControlRequest::*TASK_GOHOME* 1)
(intern "*TASK_GO_LOCAL_POS*" (find-package "DJI_OSDK_ROS::FLIGHTTASKCONTROLREQUEST"))
(shadow '*TASK_GO_LOCAL_POS* (find-package "DJI_OSDK_ROS::FLIGHTTASKCONTROLREQUEST"))
(defconstant dji_osdk_ros::FlightTaskControlRequest::*TASK_GO_LOCAL_POS* 2)
(intern "*TASK_GOHOME_AND_CONFIRM_LANDING*" (find-package "DJI_OSDK_ROS::FLIGHTTASKCONTROLREQUEST"))
(shadow '*TASK_GOHOME_AND_CONFIRM_LANDING* (find-package "DJI_OSDK_ROS::FLIGHTTASKCONTROLREQUEST"))
(defconstant dji_osdk_ros::FlightTaskControlRequest::*TASK_GOHOME_AND_CONFIRM_LANDING* 3)
(intern "*TASK_TAKEOFF*" (find-package "DJI_OSDK_ROS::FLIGHTTASKCONTROLREQUEST"))
(shadow '*TASK_TAKEOFF* (find-package "DJI_OSDK_ROS::FLIGHTTASKCONTROLREQUEST"))
(defconstant dji_osdk_ros::FlightTaskControlRequest::*TASK_TAKEOFF* 4)
(intern "*TASK_LAND*" (find-package "DJI_OSDK_ROS::FLIGHTTASKCONTROLREQUEST"))
(shadow '*TASK_LAND* (find-package "DJI_OSDK_ROS::FLIGHTTASKCONTROLREQUEST"))
(defconstant dji_osdk_ros::FlightTaskControlRequest::*TASK_LAND* 6)
(defclass dji_osdk_ros::FlightTaskControlRequest
  :super ros::object
  :slots (_task _pos_offset _yaw_params ))

(defmethod dji_osdk_ros::FlightTaskControlRequest
  (:init
   (&key
    ((:task __task) 0)
    ((:pos_offset __pos_offset) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:yaw_params __yaw_params) (make-array 0 :initial-element 0.0 :element-type :float))
    )
   (send-super :init)
   (setq _task (round __task))
   (setq _pos_offset __pos_offset)
   (setq _yaw_params __yaw_params)
   self)
  (:task
   (&optional __task)
   (if __task (setq _task __task)) _task)
  (:pos_offset
   (&optional __pos_offset)
   (if __pos_offset (setq _pos_offset __pos_offset)) _pos_offset)
  (:yaw_params
   (&optional __yaw_params)
   (if __yaw_params (setq _yaw_params __yaw_params)) _yaw_params)
  (:serialization-length
   ()
   (+
    ;; uint8 _task
    1
    ;; float64[] _pos_offset
    (* 8    (length _pos_offset)) 4
    ;; float64[] _yaw_params
    (* 8    (length _yaw_params)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; uint8 _task
       (write-byte _task s)
     ;; float64[] _pos_offset
     (write-long (length _pos_offset) s)
     (dotimes (i (length _pos_offset))
       (sys::poke (elt _pos_offset i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[] _yaw_params
     (write-long (length _yaw_params) s)
     (dotimes (i (length _yaw_params))
       (sys::poke (elt _yaw_params i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; uint8 _task
     (setq _task (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; float64[] _pos_offset
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _pos_offset (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _pos_offset i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; float64[] _yaw_params
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _yaw_params (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _yaw_params i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;;
   self)
  )

(defclass dji_osdk_ros::FlightTaskControlResponse
  :super ros::object
  :slots (_result _cmd_set _cmd_id _ack_data ))

(defmethod dji_osdk_ros::FlightTaskControlResponse
  (:init
   (&key
    ((:result __result) nil)
    ((:cmd_set __cmd_set) 0)
    ((:cmd_id __cmd_id) 0)
    ((:ack_data __ack_data) 0)
    )
   (send-super :init)
   (setq _result __result)
   (setq _cmd_set (round __cmd_set))
   (setq _cmd_id (round __cmd_id))
   (setq _ack_data (round __ack_data))
   self)
  (:result
   (&optional __result)
   (if __result (setq _result __result)) _result)
  (:cmd_set
   (&optional __cmd_set)
   (if __cmd_set (setq _cmd_set __cmd_set)) _cmd_set)
  (:cmd_id
   (&optional __cmd_id)
   (if __cmd_id (setq _cmd_id __cmd_id)) _cmd_id)
  (:ack_data
   (&optional __ack_data)
   (if __ack_data (setq _ack_data __ack_data)) _ack_data)
  (:serialization-length
   ()
   (+
    ;; bool _result
    1
    ;; uint8 _cmd_set
    1
    ;; uint8 _cmd_id
    1
    ;; uint32 _ack_data
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _result
       (if _result (write-byte -1 s) (write-byte 0 s))
     ;; uint8 _cmd_set
       (write-byte _cmd_set s)
     ;; uint8 _cmd_id
       (write-byte _cmd_id s)
     ;; uint32 _ack_data
       (write-long _ack_data s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _result
     (setq _result (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; uint8 _cmd_set
     (setq _cmd_set (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _cmd_id
     (setq _cmd_id (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint32 _ack_data
     (setq _ack_data (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(defclass dji_osdk_ros::FlightTaskControl
  :super ros::object
  :slots ())

(setf (get dji_osdk_ros::FlightTaskControl :md5sum-) "2f0fa5b45abe3a96e38cf4ab21baa780")
(setf (get dji_osdk_ros::FlightTaskControl :datatype-) "dji_osdk_ros/FlightTaskControl")
(setf (get dji_osdk_ros::FlightTaskControl :request) dji_osdk_ros::FlightTaskControlRequest)
(setf (get dji_osdk_ros::FlightTaskControl :response) dji_osdk_ros::FlightTaskControlResponse)

(defmethod dji_osdk_ros::FlightTaskControlRequest
  (:response () (instance dji_osdk_ros::FlightTaskControlResponse :init)))

(setf (get dji_osdk_ros::FlightTaskControlRequest :md5sum-) "2f0fa5b45abe3a96e38cf4ab21baa780")
(setf (get dji_osdk_ros::FlightTaskControlRequest :datatype-) "dji_osdk_ros/FlightTaskControlRequest")
(setf (get dji_osdk_ros::FlightTaskControlRequest :definition-)
      "#constant for tasks
uint8 TASK_GOHOME = 1
uint8 TASK_GO_LOCAL_POS = 2
uint8 TASK_GOHOME_AND_CONFIRM_LANDING = 3
uint8 TASK_TAKEOFF = 4
uint8 TASK_LAND = 6

#request
uint8 task    # see constants above for possible tasks
float64[] pos_offset #A vector contains that position_x_offset, position_y_offset, position_z_offset in order
float64[] yaw_params #A vector contains that yaw_desired, position_threshold(Meter), yaw_threshold(Degree)
---
bool result
# for debugging usage
uint8 cmd_set
uint8 cmd_id
uint32 ack_data


")

(setf (get dji_osdk_ros::FlightTaskControlResponse :md5sum-) "2f0fa5b45abe3a96e38cf4ab21baa780")
(setf (get dji_osdk_ros::FlightTaskControlResponse :datatype-) "dji_osdk_ros/FlightTaskControlResponse")
(setf (get dji_osdk_ros::FlightTaskControlResponse :definition-)
      "#constant for tasks
uint8 TASK_GOHOME = 1
uint8 TASK_GO_LOCAL_POS = 2
uint8 TASK_GOHOME_AND_CONFIRM_LANDING = 3
uint8 TASK_TAKEOFF = 4
uint8 TASK_LAND = 6

#request
uint8 task    # see constants above for possible tasks
float64[] pos_offset #A vector contains that position_x_offset, position_y_offset, position_z_offset in order
float64[] yaw_params #A vector contains that yaw_desired, position_threshold(Meter), yaw_threshold(Degree)
---
bool result
# for debugging usage
uint8 cmd_set
uint8 cmd_id
uint32 ack_data


")



(provide :dji_osdk_ros/FlightTaskControl "2f0fa5b45abe3a96e38cf4ab21baa780")


