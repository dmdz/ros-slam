;; Auto-generated. Do not edit!


(when (boundp 'dji_osdk_ros::AvoidEnable)
  (if (not (find-package "DJI_OSDK_ROS"))
    (make-package "DJI_OSDK_ROS"))
  (shadow 'AvoidEnable (find-package "DJI_OSDK_ROS")))
(unless (find-package "DJI_OSDK_ROS::AVOIDENABLE")
  (make-package "DJI_OSDK_ROS::AVOIDENABLE"))
(unless (find-package "DJI_OSDK_ROS::AVOIDENABLEREQUEST")
  (make-package "DJI_OSDK_ROS::AVOIDENABLEREQUEST"))
(unless (find-package "DJI_OSDK_ROS::AVOIDENABLERESPONSE")
  (make-package "DJI_OSDK_ROS::AVOIDENABLERESPONSE"))

(in-package "ROS")





(defclass dji_osdk_ros::AvoidEnableRequest
  :super ros::object
  :slots (_enable ))

(defmethod dji_osdk_ros::AvoidEnableRequest
  (:init
   (&key
    ((:enable __enable) nil)
    )
   (send-super :init)
   (setq _enable __enable)
   self)
  (:enable
   (&optional __enable)
   (if __enable (setq _enable __enable)) _enable)
  (:serialization-length
   ()
   (+
    ;; bool _enable
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _enable
       (if _enable (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _enable
     (setq _enable (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(defclass dji_osdk_ros::AvoidEnableResponse
  :super ros::object
  :slots (_result ))

(defmethod dji_osdk_ros::AvoidEnableResponse
  (:init
   (&key
    ((:result __result) nil)
    )
   (send-super :init)
   (setq _result __result)
   self)
  (:result
   (&optional __result)
   (if __result (setq _result __result)) _result)
  (:serialization-length
   ()
   (+
    ;; bool _result
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _result
       (if _result (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _result
     (setq _result (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(defclass dji_osdk_ros::AvoidEnable
  :super ros::object
  :slots ())

(setf (get dji_osdk_ros::AvoidEnable :md5sum-) "29d58f387352c15c4e4f5763022ae875")
(setf (get dji_osdk_ros::AvoidEnable :datatype-) "dji_osdk_ros/AvoidEnable")
(setf (get dji_osdk_ros::AvoidEnable :request) dji_osdk_ros::AvoidEnableRequest)
(setf (get dji_osdk_ros::AvoidEnable :response) dji_osdk_ros::AvoidEnableResponse)

(defmethod dji_osdk_ros::AvoidEnableRequest
  (:response () (instance dji_osdk_ros::AvoidEnableResponse :init)))

(setf (get dji_osdk_ros::AvoidEnableRequest :md5sum-) "29d58f387352c15c4e4f5763022ae875")
(setf (get dji_osdk_ros::AvoidEnableRequest :datatype-) "dji_osdk_ros/AvoidEnableRequest")
(setf (get dji_osdk_ros::AvoidEnableRequest :definition-)
      "bool enable
---
bool result
")

(setf (get dji_osdk_ros::AvoidEnableResponse :md5sum-) "29d58f387352c15c4e4f5763022ae875")
(setf (get dji_osdk_ros::AvoidEnableResponse :datatype-) "dji_osdk_ros/AvoidEnableResponse")
(setf (get dji_osdk_ros::AvoidEnableResponse :definition-)
      "bool enable
---
bool result
")



(provide :dji_osdk_ros/AvoidEnable "29d58f387352c15c4e4f5763022ae875")


