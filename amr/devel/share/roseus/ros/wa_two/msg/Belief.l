;; Auto-generated. Do not edit!


(when (boundp 'wa_two::Belief)
  (if (not (find-package "WA_TWO"))
    (make-package "WA_TWO"))
  (shadow 'Belief (find-package "WA_TWO")))
(unless (find-package "WA_TWO::BELIEF")
  (make-package "WA_TWO::BELIEF"))

(in-package "ROS")
;;//! \htmlinclude Belief.msg.html


(defclass wa_two::Belief
  :super ros::object
  :slots (_Xi1_0_0 _Xi1_1_0 _Ohm1_0_0 _Ohm1_0_1 _Ohm1_1_0 _Ohm1_1_1 ))

(defmethod wa_two::Belief
  (:init
   (&key
    ((:Xi1_0_0 __Xi1_0_0) 0.0)
    ((:Xi1_1_0 __Xi1_1_0) 0.0)
    ((:Ohm1_0_0 __Ohm1_0_0) 0.0)
    ((:Ohm1_0_1 __Ohm1_0_1) 0.0)
    ((:Ohm1_1_0 __Ohm1_1_0) 0.0)
    ((:Ohm1_1_1 __Ohm1_1_1) 0.0)
    )
   (send-super :init)
   (setq _Xi1_0_0 (float __Xi1_0_0))
   (setq _Xi1_1_0 (float __Xi1_1_0))
   (setq _Ohm1_0_0 (float __Ohm1_0_0))
   (setq _Ohm1_0_1 (float __Ohm1_0_1))
   (setq _Ohm1_1_0 (float __Ohm1_1_0))
   (setq _Ohm1_1_1 (float __Ohm1_1_1))
   self)
  (:Xi1_0_0
   (&optional __Xi1_0_0)
   (if __Xi1_0_0 (setq _Xi1_0_0 __Xi1_0_0)) _Xi1_0_0)
  (:Xi1_1_0
   (&optional __Xi1_1_0)
   (if __Xi1_1_0 (setq _Xi1_1_0 __Xi1_1_0)) _Xi1_1_0)
  (:Ohm1_0_0
   (&optional __Ohm1_0_0)
   (if __Ohm1_0_0 (setq _Ohm1_0_0 __Ohm1_0_0)) _Ohm1_0_0)
  (:Ohm1_0_1
   (&optional __Ohm1_0_1)
   (if __Ohm1_0_1 (setq _Ohm1_0_1 __Ohm1_0_1)) _Ohm1_0_1)
  (:Ohm1_1_0
   (&optional __Ohm1_1_0)
   (if __Ohm1_1_0 (setq _Ohm1_1_0 __Ohm1_1_0)) _Ohm1_1_0)
  (:Ohm1_1_1
   (&optional __Ohm1_1_1)
   (if __Ohm1_1_1 (setq _Ohm1_1_1 __Ohm1_1_1)) _Ohm1_1_1)
  (:serialization-length
   ()
   (+
    ;; float32 _Xi1_0_0
    4
    ;; float32 _Xi1_1_0
    4
    ;; float32 _Ohm1_0_0
    4
    ;; float32 _Ohm1_0_1
    4
    ;; float32 _Ohm1_1_0
    4
    ;; float32 _Ohm1_1_1
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _Xi1_0_0
       (sys::poke _Xi1_0_0 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _Xi1_1_0
       (sys::poke _Xi1_1_0 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _Ohm1_0_0
       (sys::poke _Ohm1_0_0 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _Ohm1_0_1
       (sys::poke _Ohm1_0_1 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _Ohm1_1_0
       (sys::poke _Ohm1_1_0 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _Ohm1_1_1
       (sys::poke _Ohm1_1_1 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _Xi1_0_0
     (setq _Xi1_0_0 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _Xi1_1_0
     (setq _Xi1_1_0 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _Ohm1_0_0
     (setq _Ohm1_0_0 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _Ohm1_0_1
     (setq _Ohm1_0_1 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _Ohm1_1_0
     (setq _Ohm1_1_0 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _Ohm1_1_1
     (setq _Ohm1_1_1 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get wa_two::Belief :md5sum-) "a684de5435d4050cd6350a964c19142c")
(setf (get wa_two::Belief :datatype-) "wa_two/Belief")
(setf (get wa_two::Belief :definition-)
      "float32 Xi1_0_0
float32 Xi1_1_0

float32 Ohm1_0_0
float32 Ohm1_0_1
float32 Ohm1_1_0
float32 Ohm1_1_1
")



(provide :wa_two/Belief "a684de5435d4050cd6350a964c19142c")


