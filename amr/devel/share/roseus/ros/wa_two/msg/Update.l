;; Auto-generated. Do not edit!


(when (boundp 'wa_two::Update)
  (if (not (find-package "WA_TWO"))
    (make-package "WA_TWO"))
  (shadow 'Update (find-package "WA_TWO")))
(unless (find-package "WA_TWO::UPDATE")
  (make-package "WA_TWO::UPDATE"))

(in-package "ROS")
;;//! \htmlinclude Update.msg.html


(defclass wa_two::Update
  :super ros::object
  :slots (_u1_0_0 _u1_1_0 _z1_0_0 _z1_1_0 ))

(defmethod wa_two::Update
  (:init
   (&key
    ((:u1_0_0 __u1_0_0) 0.0)
    ((:u1_1_0 __u1_1_0) 0.0)
    ((:z1_0_0 __z1_0_0) 0.0)
    ((:z1_1_0 __z1_1_0) 0.0)
    )
   (send-super :init)
   (setq _u1_0_0 (float __u1_0_0))
   (setq _u1_1_0 (float __u1_1_0))
   (setq _z1_0_0 (float __z1_0_0))
   (setq _z1_1_0 (float __z1_1_0))
   self)
  (:u1_0_0
   (&optional __u1_0_0)
   (if __u1_0_0 (setq _u1_0_0 __u1_0_0)) _u1_0_0)
  (:u1_1_0
   (&optional __u1_1_0)
   (if __u1_1_0 (setq _u1_1_0 __u1_1_0)) _u1_1_0)
  (:z1_0_0
   (&optional __z1_0_0)
   (if __z1_0_0 (setq _z1_0_0 __z1_0_0)) _z1_0_0)
  (:z1_1_0
   (&optional __z1_1_0)
   (if __z1_1_0 (setq _z1_1_0 __z1_1_0)) _z1_1_0)
  (:serialization-length
   ()
   (+
    ;; float32 _u1_0_0
    4
    ;; float32 _u1_1_0
    4
    ;; float32 _z1_0_0
    4
    ;; float32 _z1_1_0
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _u1_0_0
       (sys::poke _u1_0_0 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _u1_1_0
       (sys::poke _u1_1_0 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _z1_0_0
       (sys::poke _z1_0_0 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _z1_1_0
       (sys::poke _z1_1_0 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _u1_0_0
     (setq _u1_0_0 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _u1_1_0
     (setq _u1_1_0 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _z1_0_0
     (setq _z1_0_0 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _z1_1_0
     (setq _z1_1_0 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get wa_two::Update :md5sum-) "fff9533cd774067ff316660288526941")
(setf (get wa_two::Update :datatype-) "wa_two/Update")
(setf (get wa_two::Update :definition-)
      "float32 u1_0_0
float32 u1_1_0

float32 z1_0_0
float32 z1_1_0
")



(provide :wa_two/Update "fff9533cd774067ff316660288526941")


