;; Auto-generated. Do not edit!


(when (boundp 'wa_two::Model)
  (if (not (find-package "WA_TWO"))
    (make-package "WA_TWO"))
  (shadow 'Model (find-package "WA_TWO")))
(unless (find-package "WA_TWO::MODEL")
  (make-package "WA_TWO::MODEL"))

(in-package "ROS")
;;//! \htmlinclude Model.msg.html


(defclass wa_two::Model
  :super ros::object
  :slots (_Xi0_0_0 _Xi0_1_0 _Ohm0_0_0 _Ohm0_0_1 _Ohm0_1_0 _Ohm0_1_1 _A_0_0 _A_0_1 _A_1_0 _A_1_1 _B_0_0 _B_0_1 _B_1_0 _B_1_1 _C_0_0 _C_0_1 _C_1_0 _C_1_1 _R_0_0 _R_0_1 _R_1_0 _R_1_1 _Q_0_0 _Q_0_1 _Q_1_0 _Q_1_1 ))

(defmethod wa_two::Model
  (:init
   (&key
    ((:Xi0_0_0 __Xi0_0_0) 0.0)
    ((:Xi0_1_0 __Xi0_1_0) 0.0)
    ((:Ohm0_0_0 __Ohm0_0_0) 0.0)
    ((:Ohm0_0_1 __Ohm0_0_1) 0.0)
    ((:Ohm0_1_0 __Ohm0_1_0) 0.0)
    ((:Ohm0_1_1 __Ohm0_1_1) 0.0)
    ((:A_0_0 __A_0_0) 0.0)
    ((:A_0_1 __A_0_1) 0.0)
    ((:A_1_0 __A_1_0) 0.0)
    ((:A_1_1 __A_1_1) 0.0)
    ((:B_0_0 __B_0_0) 0.0)
    ((:B_0_1 __B_0_1) 0.0)
    ((:B_1_0 __B_1_0) 0.0)
    ((:B_1_1 __B_1_1) 0.0)
    ((:C_0_0 __C_0_0) 0.0)
    ((:C_0_1 __C_0_1) 0.0)
    ((:C_1_0 __C_1_0) 0.0)
    ((:C_1_1 __C_1_1) 0.0)
    ((:R_0_0 __R_0_0) 0.0)
    ((:R_0_1 __R_0_1) 0.0)
    ((:R_1_0 __R_1_0) 0.0)
    ((:R_1_1 __R_1_1) 0.0)
    ((:Q_0_0 __Q_0_0) 0.0)
    ((:Q_0_1 __Q_0_1) 0.0)
    ((:Q_1_0 __Q_1_0) 0.0)
    ((:Q_1_1 __Q_1_1) 0.0)
    )
   (send-super :init)
   (setq _Xi0_0_0 (float __Xi0_0_0))
   (setq _Xi0_1_0 (float __Xi0_1_0))
   (setq _Ohm0_0_0 (float __Ohm0_0_0))
   (setq _Ohm0_0_1 (float __Ohm0_0_1))
   (setq _Ohm0_1_0 (float __Ohm0_1_0))
   (setq _Ohm0_1_1 (float __Ohm0_1_1))
   (setq _A_0_0 (float __A_0_0))
   (setq _A_0_1 (float __A_0_1))
   (setq _A_1_0 (float __A_1_0))
   (setq _A_1_1 (float __A_1_1))
   (setq _B_0_0 (float __B_0_0))
   (setq _B_0_1 (float __B_0_1))
   (setq _B_1_0 (float __B_1_0))
   (setq _B_1_1 (float __B_1_1))
   (setq _C_0_0 (float __C_0_0))
   (setq _C_0_1 (float __C_0_1))
   (setq _C_1_0 (float __C_1_0))
   (setq _C_1_1 (float __C_1_1))
   (setq _R_0_0 (float __R_0_0))
   (setq _R_0_1 (float __R_0_1))
   (setq _R_1_0 (float __R_1_0))
   (setq _R_1_1 (float __R_1_1))
   (setq _Q_0_0 (float __Q_0_0))
   (setq _Q_0_1 (float __Q_0_1))
   (setq _Q_1_0 (float __Q_1_0))
   (setq _Q_1_1 (float __Q_1_1))
   self)
  (:Xi0_0_0
   (&optional __Xi0_0_0)
   (if __Xi0_0_0 (setq _Xi0_0_0 __Xi0_0_0)) _Xi0_0_0)
  (:Xi0_1_0
   (&optional __Xi0_1_0)
   (if __Xi0_1_0 (setq _Xi0_1_0 __Xi0_1_0)) _Xi0_1_0)
  (:Ohm0_0_0
   (&optional __Ohm0_0_0)
   (if __Ohm0_0_0 (setq _Ohm0_0_0 __Ohm0_0_0)) _Ohm0_0_0)
  (:Ohm0_0_1
   (&optional __Ohm0_0_1)
   (if __Ohm0_0_1 (setq _Ohm0_0_1 __Ohm0_0_1)) _Ohm0_0_1)
  (:Ohm0_1_0
   (&optional __Ohm0_1_0)
   (if __Ohm0_1_0 (setq _Ohm0_1_0 __Ohm0_1_0)) _Ohm0_1_0)
  (:Ohm0_1_1
   (&optional __Ohm0_1_1)
   (if __Ohm0_1_1 (setq _Ohm0_1_1 __Ohm0_1_1)) _Ohm0_1_1)
  (:A_0_0
   (&optional __A_0_0)
   (if __A_0_0 (setq _A_0_0 __A_0_0)) _A_0_0)
  (:A_0_1
   (&optional __A_0_1)
   (if __A_0_1 (setq _A_0_1 __A_0_1)) _A_0_1)
  (:A_1_0
   (&optional __A_1_0)
   (if __A_1_0 (setq _A_1_0 __A_1_0)) _A_1_0)
  (:A_1_1
   (&optional __A_1_1)
   (if __A_1_1 (setq _A_1_1 __A_1_1)) _A_1_1)
  (:B_0_0
   (&optional __B_0_0)
   (if __B_0_0 (setq _B_0_0 __B_0_0)) _B_0_0)
  (:B_0_1
   (&optional __B_0_1)
   (if __B_0_1 (setq _B_0_1 __B_0_1)) _B_0_1)
  (:B_1_0
   (&optional __B_1_0)
   (if __B_1_0 (setq _B_1_0 __B_1_0)) _B_1_0)
  (:B_1_1
   (&optional __B_1_1)
   (if __B_1_1 (setq _B_1_1 __B_1_1)) _B_1_1)
  (:C_0_0
   (&optional __C_0_0)
   (if __C_0_0 (setq _C_0_0 __C_0_0)) _C_0_0)
  (:C_0_1
   (&optional __C_0_1)
   (if __C_0_1 (setq _C_0_1 __C_0_1)) _C_0_1)
  (:C_1_0
   (&optional __C_1_0)
   (if __C_1_0 (setq _C_1_0 __C_1_0)) _C_1_0)
  (:C_1_1
   (&optional __C_1_1)
   (if __C_1_1 (setq _C_1_1 __C_1_1)) _C_1_1)
  (:R_0_0
   (&optional __R_0_0)
   (if __R_0_0 (setq _R_0_0 __R_0_0)) _R_0_0)
  (:R_0_1
   (&optional __R_0_1)
   (if __R_0_1 (setq _R_0_1 __R_0_1)) _R_0_1)
  (:R_1_0
   (&optional __R_1_0)
   (if __R_1_0 (setq _R_1_0 __R_1_0)) _R_1_0)
  (:R_1_1
   (&optional __R_1_1)
   (if __R_1_1 (setq _R_1_1 __R_1_1)) _R_1_1)
  (:Q_0_0
   (&optional __Q_0_0)
   (if __Q_0_0 (setq _Q_0_0 __Q_0_0)) _Q_0_0)
  (:Q_0_1
   (&optional __Q_0_1)
   (if __Q_0_1 (setq _Q_0_1 __Q_0_1)) _Q_0_1)
  (:Q_1_0
   (&optional __Q_1_0)
   (if __Q_1_0 (setq _Q_1_0 __Q_1_0)) _Q_1_0)
  (:Q_1_1
   (&optional __Q_1_1)
   (if __Q_1_1 (setq _Q_1_1 __Q_1_1)) _Q_1_1)
  (:serialization-length
   ()
   (+
    ;; float32 _Xi0_0_0
    4
    ;; float32 _Xi0_1_0
    4
    ;; float32 _Ohm0_0_0
    4
    ;; float32 _Ohm0_0_1
    4
    ;; float32 _Ohm0_1_0
    4
    ;; float32 _Ohm0_1_1
    4
    ;; float32 _A_0_0
    4
    ;; float32 _A_0_1
    4
    ;; float32 _A_1_0
    4
    ;; float32 _A_1_1
    4
    ;; float32 _B_0_0
    4
    ;; float32 _B_0_1
    4
    ;; float32 _B_1_0
    4
    ;; float32 _B_1_1
    4
    ;; float32 _C_0_0
    4
    ;; float32 _C_0_1
    4
    ;; float32 _C_1_0
    4
    ;; float32 _C_1_1
    4
    ;; float32 _R_0_0
    4
    ;; float32 _R_0_1
    4
    ;; float32 _R_1_0
    4
    ;; float32 _R_1_1
    4
    ;; float32 _Q_0_0
    4
    ;; float32 _Q_0_1
    4
    ;; float32 _Q_1_0
    4
    ;; float32 _Q_1_1
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _Xi0_0_0
       (sys::poke _Xi0_0_0 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _Xi0_1_0
       (sys::poke _Xi0_1_0 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _Ohm0_0_0
       (sys::poke _Ohm0_0_0 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _Ohm0_0_1
       (sys::poke _Ohm0_0_1 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _Ohm0_1_0
       (sys::poke _Ohm0_1_0 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _Ohm0_1_1
       (sys::poke _Ohm0_1_1 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _A_0_0
       (sys::poke _A_0_0 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _A_0_1
       (sys::poke _A_0_1 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _A_1_0
       (sys::poke _A_1_0 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _A_1_1
       (sys::poke _A_1_1 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _B_0_0
       (sys::poke _B_0_0 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _B_0_1
       (sys::poke _B_0_1 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _B_1_0
       (sys::poke _B_1_0 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _B_1_1
       (sys::poke _B_1_1 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _C_0_0
       (sys::poke _C_0_0 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _C_0_1
       (sys::poke _C_0_1 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _C_1_0
       (sys::poke _C_1_0 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _C_1_1
       (sys::poke _C_1_1 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _R_0_0
       (sys::poke _R_0_0 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _R_0_1
       (sys::poke _R_0_1 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _R_1_0
       (sys::poke _R_1_0 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _R_1_1
       (sys::poke _R_1_1 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _Q_0_0
       (sys::poke _Q_0_0 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _Q_0_1
       (sys::poke _Q_0_1 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _Q_1_0
       (sys::poke _Q_1_0 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _Q_1_1
       (sys::poke _Q_1_1 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _Xi0_0_0
     (setq _Xi0_0_0 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _Xi0_1_0
     (setq _Xi0_1_0 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _Ohm0_0_0
     (setq _Ohm0_0_0 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _Ohm0_0_1
     (setq _Ohm0_0_1 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _Ohm0_1_0
     (setq _Ohm0_1_0 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _Ohm0_1_1
     (setq _Ohm0_1_1 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _A_0_0
     (setq _A_0_0 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _A_0_1
     (setq _A_0_1 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _A_1_0
     (setq _A_1_0 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _A_1_1
     (setq _A_1_1 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _B_0_0
     (setq _B_0_0 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _B_0_1
     (setq _B_0_1 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _B_1_0
     (setq _B_1_0 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _B_1_1
     (setq _B_1_1 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _C_0_0
     (setq _C_0_0 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _C_0_1
     (setq _C_0_1 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _C_1_0
     (setq _C_1_0 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _C_1_1
     (setq _C_1_1 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _R_0_0
     (setq _R_0_0 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _R_0_1
     (setq _R_0_1 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _R_1_0
     (setq _R_1_0 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _R_1_1
     (setq _R_1_1 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _Q_0_0
     (setq _Q_0_0 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _Q_0_1
     (setq _Q_0_1 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _Q_1_0
     (setq _Q_1_0 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _Q_1_1
     (setq _Q_1_1 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get wa_two::Model :md5sum-) "86a1455dd7ccb59c04c3e718684e2f4f")
(setf (get wa_two::Model :datatype-) "wa_two/Model")
(setf (get wa_two::Model :definition-)
      "float32 Xi0_0_0
float32 Xi0_1_0

float32 Ohm0_0_0
float32 Ohm0_0_1
float32 Ohm0_1_0
float32 Ohm0_1_1

float32 A_0_0
float32 A_0_1
float32 A_1_0
float32 A_1_1

float32 B_0_0
float32 B_0_1
float32 B_1_0
float32 B_1_1

float32 C_0_0
float32 C_0_1
float32 C_1_0
float32 C_1_1

float32 R_0_0
float32 R_0_1
float32 R_1_0
float32 R_1_1

float32 Q_0_0
float32 Q_0_1
float32 Q_1_0
float32 Q_1_1
")



(provide :wa_two/Model "86a1455dd7ccb59c04c3e718684e2f4f")


