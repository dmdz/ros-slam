; Auto-generated. Do not edit!


(cl:in-package wa_two-msg)


;//! \htmlinclude Model.msg.html

(cl:defclass <Model> (roslisp-msg-protocol:ros-message)
  ((Xi0_0_0
    :reader Xi0_0_0
    :initarg :Xi0_0_0
    :type cl:float
    :initform 0.0)
   (Xi0_1_0
    :reader Xi0_1_0
    :initarg :Xi0_1_0
    :type cl:float
    :initform 0.0)
   (Ohm0_0_0
    :reader Ohm0_0_0
    :initarg :Ohm0_0_0
    :type cl:float
    :initform 0.0)
   (Ohm0_0_1
    :reader Ohm0_0_1
    :initarg :Ohm0_0_1
    :type cl:float
    :initform 0.0)
   (Ohm0_1_0
    :reader Ohm0_1_0
    :initarg :Ohm0_1_0
    :type cl:float
    :initform 0.0)
   (Ohm0_1_1
    :reader Ohm0_1_1
    :initarg :Ohm0_1_1
    :type cl:float
    :initform 0.0)
   (A_0_0
    :reader A_0_0
    :initarg :A_0_0
    :type cl:float
    :initform 0.0)
   (A_0_1
    :reader A_0_1
    :initarg :A_0_1
    :type cl:float
    :initform 0.0)
   (A_1_0
    :reader A_1_0
    :initarg :A_1_0
    :type cl:float
    :initform 0.0)
   (A_1_1
    :reader A_1_1
    :initarg :A_1_1
    :type cl:float
    :initform 0.0)
   (B_0_0
    :reader B_0_0
    :initarg :B_0_0
    :type cl:float
    :initform 0.0)
   (B_0_1
    :reader B_0_1
    :initarg :B_0_1
    :type cl:float
    :initform 0.0)
   (B_1_0
    :reader B_1_0
    :initarg :B_1_0
    :type cl:float
    :initform 0.0)
   (B_1_1
    :reader B_1_1
    :initarg :B_1_1
    :type cl:float
    :initform 0.0)
   (C_0_0
    :reader C_0_0
    :initarg :C_0_0
    :type cl:float
    :initform 0.0)
   (C_0_1
    :reader C_0_1
    :initarg :C_0_1
    :type cl:float
    :initform 0.0)
   (C_1_0
    :reader C_1_0
    :initarg :C_1_0
    :type cl:float
    :initform 0.0)
   (C_1_1
    :reader C_1_1
    :initarg :C_1_1
    :type cl:float
    :initform 0.0)
   (R_0_0
    :reader R_0_0
    :initarg :R_0_0
    :type cl:float
    :initform 0.0)
   (R_0_1
    :reader R_0_1
    :initarg :R_0_1
    :type cl:float
    :initform 0.0)
   (R_1_0
    :reader R_1_0
    :initarg :R_1_0
    :type cl:float
    :initform 0.0)
   (R_1_1
    :reader R_1_1
    :initarg :R_1_1
    :type cl:float
    :initform 0.0)
   (Q_0_0
    :reader Q_0_0
    :initarg :Q_0_0
    :type cl:float
    :initform 0.0)
   (Q_0_1
    :reader Q_0_1
    :initarg :Q_0_1
    :type cl:float
    :initform 0.0)
   (Q_1_0
    :reader Q_1_0
    :initarg :Q_1_0
    :type cl:float
    :initform 0.0)
   (Q_1_1
    :reader Q_1_1
    :initarg :Q_1_1
    :type cl:float
    :initform 0.0))
)

(cl:defclass Model (<Model>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Model>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Model)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name wa_two-msg:<Model> is deprecated: use wa_two-msg:Model instead.")))

(cl:ensure-generic-function 'Xi0_0_0-val :lambda-list '(m))
(cl:defmethod Xi0_0_0-val ((m <Model>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:Xi0_0_0-val is deprecated.  Use wa_two-msg:Xi0_0_0 instead.")
  (Xi0_0_0 m))

(cl:ensure-generic-function 'Xi0_1_0-val :lambda-list '(m))
(cl:defmethod Xi0_1_0-val ((m <Model>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:Xi0_1_0-val is deprecated.  Use wa_two-msg:Xi0_1_0 instead.")
  (Xi0_1_0 m))

(cl:ensure-generic-function 'Ohm0_0_0-val :lambda-list '(m))
(cl:defmethod Ohm0_0_0-val ((m <Model>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:Ohm0_0_0-val is deprecated.  Use wa_two-msg:Ohm0_0_0 instead.")
  (Ohm0_0_0 m))

(cl:ensure-generic-function 'Ohm0_0_1-val :lambda-list '(m))
(cl:defmethod Ohm0_0_1-val ((m <Model>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:Ohm0_0_1-val is deprecated.  Use wa_two-msg:Ohm0_0_1 instead.")
  (Ohm0_0_1 m))

(cl:ensure-generic-function 'Ohm0_1_0-val :lambda-list '(m))
(cl:defmethod Ohm0_1_0-val ((m <Model>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:Ohm0_1_0-val is deprecated.  Use wa_two-msg:Ohm0_1_0 instead.")
  (Ohm0_1_0 m))

(cl:ensure-generic-function 'Ohm0_1_1-val :lambda-list '(m))
(cl:defmethod Ohm0_1_1-val ((m <Model>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:Ohm0_1_1-val is deprecated.  Use wa_two-msg:Ohm0_1_1 instead.")
  (Ohm0_1_1 m))

(cl:ensure-generic-function 'A_0_0-val :lambda-list '(m))
(cl:defmethod A_0_0-val ((m <Model>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:A_0_0-val is deprecated.  Use wa_two-msg:A_0_0 instead.")
  (A_0_0 m))

(cl:ensure-generic-function 'A_0_1-val :lambda-list '(m))
(cl:defmethod A_0_1-val ((m <Model>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:A_0_1-val is deprecated.  Use wa_two-msg:A_0_1 instead.")
  (A_0_1 m))

(cl:ensure-generic-function 'A_1_0-val :lambda-list '(m))
(cl:defmethod A_1_0-val ((m <Model>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:A_1_0-val is deprecated.  Use wa_two-msg:A_1_0 instead.")
  (A_1_0 m))

(cl:ensure-generic-function 'A_1_1-val :lambda-list '(m))
(cl:defmethod A_1_1-val ((m <Model>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:A_1_1-val is deprecated.  Use wa_two-msg:A_1_1 instead.")
  (A_1_1 m))

(cl:ensure-generic-function 'B_0_0-val :lambda-list '(m))
(cl:defmethod B_0_0-val ((m <Model>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:B_0_0-val is deprecated.  Use wa_two-msg:B_0_0 instead.")
  (B_0_0 m))

(cl:ensure-generic-function 'B_0_1-val :lambda-list '(m))
(cl:defmethod B_0_1-val ((m <Model>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:B_0_1-val is deprecated.  Use wa_two-msg:B_0_1 instead.")
  (B_0_1 m))

(cl:ensure-generic-function 'B_1_0-val :lambda-list '(m))
(cl:defmethod B_1_0-val ((m <Model>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:B_1_0-val is deprecated.  Use wa_two-msg:B_1_0 instead.")
  (B_1_0 m))

(cl:ensure-generic-function 'B_1_1-val :lambda-list '(m))
(cl:defmethod B_1_1-val ((m <Model>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:B_1_1-val is deprecated.  Use wa_two-msg:B_1_1 instead.")
  (B_1_1 m))

(cl:ensure-generic-function 'C_0_0-val :lambda-list '(m))
(cl:defmethod C_0_0-val ((m <Model>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:C_0_0-val is deprecated.  Use wa_two-msg:C_0_0 instead.")
  (C_0_0 m))

(cl:ensure-generic-function 'C_0_1-val :lambda-list '(m))
(cl:defmethod C_0_1-val ((m <Model>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:C_0_1-val is deprecated.  Use wa_two-msg:C_0_1 instead.")
  (C_0_1 m))

(cl:ensure-generic-function 'C_1_0-val :lambda-list '(m))
(cl:defmethod C_1_0-val ((m <Model>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:C_1_0-val is deprecated.  Use wa_two-msg:C_1_0 instead.")
  (C_1_0 m))

(cl:ensure-generic-function 'C_1_1-val :lambda-list '(m))
(cl:defmethod C_1_1-val ((m <Model>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:C_1_1-val is deprecated.  Use wa_two-msg:C_1_1 instead.")
  (C_1_1 m))

(cl:ensure-generic-function 'R_0_0-val :lambda-list '(m))
(cl:defmethod R_0_0-val ((m <Model>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:R_0_0-val is deprecated.  Use wa_two-msg:R_0_0 instead.")
  (R_0_0 m))

(cl:ensure-generic-function 'R_0_1-val :lambda-list '(m))
(cl:defmethod R_0_1-val ((m <Model>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:R_0_1-val is deprecated.  Use wa_two-msg:R_0_1 instead.")
  (R_0_1 m))

(cl:ensure-generic-function 'R_1_0-val :lambda-list '(m))
(cl:defmethod R_1_0-val ((m <Model>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:R_1_0-val is deprecated.  Use wa_two-msg:R_1_0 instead.")
  (R_1_0 m))

(cl:ensure-generic-function 'R_1_1-val :lambda-list '(m))
(cl:defmethod R_1_1-val ((m <Model>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:R_1_1-val is deprecated.  Use wa_two-msg:R_1_1 instead.")
  (R_1_1 m))

(cl:ensure-generic-function 'Q_0_0-val :lambda-list '(m))
(cl:defmethod Q_0_0-val ((m <Model>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:Q_0_0-val is deprecated.  Use wa_two-msg:Q_0_0 instead.")
  (Q_0_0 m))

(cl:ensure-generic-function 'Q_0_1-val :lambda-list '(m))
(cl:defmethod Q_0_1-val ((m <Model>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:Q_0_1-val is deprecated.  Use wa_two-msg:Q_0_1 instead.")
  (Q_0_1 m))

(cl:ensure-generic-function 'Q_1_0-val :lambda-list '(m))
(cl:defmethod Q_1_0-val ((m <Model>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:Q_1_0-val is deprecated.  Use wa_two-msg:Q_1_0 instead.")
  (Q_1_0 m))

(cl:ensure-generic-function 'Q_1_1-val :lambda-list '(m))
(cl:defmethod Q_1_1-val ((m <Model>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:Q_1_1-val is deprecated.  Use wa_two-msg:Q_1_1 instead.")
  (Q_1_1 m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Model>) ostream)
  "Serializes a message object of type '<Model>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Xi0_0_0))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Xi0_1_0))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Ohm0_0_0))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Ohm0_0_1))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Ohm0_1_0))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Ohm0_1_1))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'A_0_0))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'A_0_1))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'A_1_0))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'A_1_1))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'B_0_0))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'B_0_1))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'B_1_0))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'B_1_1))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'C_0_0))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'C_0_1))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'C_1_0))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'C_1_1))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'R_0_0))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'R_0_1))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'R_1_0))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'R_1_1))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Q_0_0))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Q_0_1))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Q_1_0))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Q_1_1))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Model>) istream)
  "Deserializes a message object of type '<Model>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Xi0_0_0) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Xi0_1_0) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Ohm0_0_0) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Ohm0_0_1) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Ohm0_1_0) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Ohm0_1_1) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'A_0_0) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'A_0_1) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'A_1_0) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'A_1_1) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'B_0_0) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'B_0_1) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'B_1_0) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'B_1_1) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'C_0_0) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'C_0_1) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'C_1_0) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'C_1_1) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'R_0_0) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'R_0_1) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'R_1_0) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'R_1_1) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Q_0_0) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Q_0_1) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Q_1_0) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Q_1_1) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Model>)))
  "Returns string type for a message object of type '<Model>"
  "wa_two/Model")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Model)))
  "Returns string type for a message object of type 'Model"
  "wa_two/Model")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Model>)))
  "Returns md5sum for a message object of type '<Model>"
  "86a1455dd7ccb59c04c3e718684e2f4f")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Model)))
  "Returns md5sum for a message object of type 'Model"
  "86a1455dd7ccb59c04c3e718684e2f4f")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Model>)))
  "Returns full string definition for message of type '<Model>"
  (cl:format cl:nil "float32 Xi0_0_0~%float32 Xi0_1_0~%~%float32 Ohm0_0_0~%float32 Ohm0_0_1~%float32 Ohm0_1_0~%float32 Ohm0_1_1~%~%float32 A_0_0~%float32 A_0_1~%float32 A_1_0~%float32 A_1_1~%~%float32 B_0_0~%float32 B_0_1~%float32 B_1_0~%float32 B_1_1~%~%float32 C_0_0~%float32 C_0_1~%float32 C_1_0~%float32 C_1_1~%~%float32 R_0_0~%float32 R_0_1~%float32 R_1_0~%float32 R_1_1~%~%float32 Q_0_0~%float32 Q_0_1~%float32 Q_1_0~%float32 Q_1_1~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Model)))
  "Returns full string definition for message of type 'Model"
  (cl:format cl:nil "float32 Xi0_0_0~%float32 Xi0_1_0~%~%float32 Ohm0_0_0~%float32 Ohm0_0_1~%float32 Ohm0_1_0~%float32 Ohm0_1_1~%~%float32 A_0_0~%float32 A_0_1~%float32 A_1_0~%float32 A_1_1~%~%float32 B_0_0~%float32 B_0_1~%float32 B_1_0~%float32 B_1_1~%~%float32 C_0_0~%float32 C_0_1~%float32 C_1_0~%float32 C_1_1~%~%float32 R_0_0~%float32 R_0_1~%float32 R_1_0~%float32 R_1_1~%~%float32 Q_0_0~%float32 Q_0_1~%float32 Q_1_0~%float32 Q_1_1~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Model>))
  (cl:+ 0
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Model>))
  "Converts a ROS message object to a list"
  (cl:list 'Model
    (cl:cons ':Xi0_0_0 (Xi0_0_0 msg))
    (cl:cons ':Xi0_1_0 (Xi0_1_0 msg))
    (cl:cons ':Ohm0_0_0 (Ohm0_0_0 msg))
    (cl:cons ':Ohm0_0_1 (Ohm0_0_1 msg))
    (cl:cons ':Ohm0_1_0 (Ohm0_1_0 msg))
    (cl:cons ':Ohm0_1_1 (Ohm0_1_1 msg))
    (cl:cons ':A_0_0 (A_0_0 msg))
    (cl:cons ':A_0_1 (A_0_1 msg))
    (cl:cons ':A_1_0 (A_1_0 msg))
    (cl:cons ':A_1_1 (A_1_1 msg))
    (cl:cons ':B_0_0 (B_0_0 msg))
    (cl:cons ':B_0_1 (B_0_1 msg))
    (cl:cons ':B_1_0 (B_1_0 msg))
    (cl:cons ':B_1_1 (B_1_1 msg))
    (cl:cons ':C_0_0 (C_0_0 msg))
    (cl:cons ':C_0_1 (C_0_1 msg))
    (cl:cons ':C_1_0 (C_1_0 msg))
    (cl:cons ':C_1_1 (C_1_1 msg))
    (cl:cons ':R_0_0 (R_0_0 msg))
    (cl:cons ':R_0_1 (R_0_1 msg))
    (cl:cons ':R_1_0 (R_1_0 msg))
    (cl:cons ':R_1_1 (R_1_1 msg))
    (cl:cons ':Q_0_0 (Q_0_0 msg))
    (cl:cons ':Q_0_1 (Q_0_1 msg))
    (cl:cons ':Q_1_0 (Q_1_0 msg))
    (cl:cons ':Q_1_1 (Q_1_1 msg))
))
