; Auto-generated. Do not edit!


(cl:in-package wa_two-msg)


;//! \htmlinclude Belief.msg.html

(cl:defclass <Belief> (roslisp-msg-protocol:ros-message)
  ((Xi1_0_0
    :reader Xi1_0_0
    :initarg :Xi1_0_0
    :type cl:float
    :initform 0.0)
   (Xi1_1_0
    :reader Xi1_1_0
    :initarg :Xi1_1_0
    :type cl:float
    :initform 0.0)
   (Ohm1_0_0
    :reader Ohm1_0_0
    :initarg :Ohm1_0_0
    :type cl:float
    :initform 0.0)
   (Ohm1_0_1
    :reader Ohm1_0_1
    :initarg :Ohm1_0_1
    :type cl:float
    :initform 0.0)
   (Ohm1_1_0
    :reader Ohm1_1_0
    :initarg :Ohm1_1_0
    :type cl:float
    :initform 0.0)
   (Ohm1_1_1
    :reader Ohm1_1_1
    :initarg :Ohm1_1_1
    :type cl:float
    :initform 0.0))
)

(cl:defclass Belief (<Belief>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Belief>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Belief)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name wa_two-msg:<Belief> is deprecated: use wa_two-msg:Belief instead.")))

(cl:ensure-generic-function 'Xi1_0_0-val :lambda-list '(m))
(cl:defmethod Xi1_0_0-val ((m <Belief>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:Xi1_0_0-val is deprecated.  Use wa_two-msg:Xi1_0_0 instead.")
  (Xi1_0_0 m))

(cl:ensure-generic-function 'Xi1_1_0-val :lambda-list '(m))
(cl:defmethod Xi1_1_0-val ((m <Belief>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:Xi1_1_0-val is deprecated.  Use wa_two-msg:Xi1_1_0 instead.")
  (Xi1_1_0 m))

(cl:ensure-generic-function 'Ohm1_0_0-val :lambda-list '(m))
(cl:defmethod Ohm1_0_0-val ((m <Belief>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:Ohm1_0_0-val is deprecated.  Use wa_two-msg:Ohm1_0_0 instead.")
  (Ohm1_0_0 m))

(cl:ensure-generic-function 'Ohm1_0_1-val :lambda-list '(m))
(cl:defmethod Ohm1_0_1-val ((m <Belief>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:Ohm1_0_1-val is deprecated.  Use wa_two-msg:Ohm1_0_1 instead.")
  (Ohm1_0_1 m))

(cl:ensure-generic-function 'Ohm1_1_0-val :lambda-list '(m))
(cl:defmethod Ohm1_1_0-val ((m <Belief>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:Ohm1_1_0-val is deprecated.  Use wa_two-msg:Ohm1_1_0 instead.")
  (Ohm1_1_0 m))

(cl:ensure-generic-function 'Ohm1_1_1-val :lambda-list '(m))
(cl:defmethod Ohm1_1_1-val ((m <Belief>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:Ohm1_1_1-val is deprecated.  Use wa_two-msg:Ohm1_1_1 instead.")
  (Ohm1_1_1 m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Belief>) ostream)
  "Serializes a message object of type '<Belief>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Xi1_0_0))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Xi1_1_0))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Ohm1_0_0))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Ohm1_0_1))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Ohm1_1_0))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Ohm1_1_1))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Belief>) istream)
  "Deserializes a message object of type '<Belief>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Xi1_0_0) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Xi1_1_0) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Ohm1_0_0) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Ohm1_0_1) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Ohm1_1_0) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Ohm1_1_1) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Belief>)))
  "Returns string type for a message object of type '<Belief>"
  "wa_two/Belief")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Belief)))
  "Returns string type for a message object of type 'Belief"
  "wa_two/Belief")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Belief>)))
  "Returns md5sum for a message object of type '<Belief>"
  "a684de5435d4050cd6350a964c19142c")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Belief)))
  "Returns md5sum for a message object of type 'Belief"
  "a684de5435d4050cd6350a964c19142c")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Belief>)))
  "Returns full string definition for message of type '<Belief>"
  (cl:format cl:nil "float32 Xi1_0_0~%float32 Xi1_1_0~%~%float32 Ohm1_0_0~%float32 Ohm1_0_1~%float32 Ohm1_1_0~%float32 Ohm1_1_1~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Belief)))
  "Returns full string definition for message of type 'Belief"
  (cl:format cl:nil "float32 Xi1_0_0~%float32 Xi1_1_0~%~%float32 Ohm1_0_0~%float32 Ohm1_0_1~%float32 Ohm1_1_0~%float32 Ohm1_1_1~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Belief>))
  (cl:+ 0
     4
     4
     4
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Belief>))
  "Converts a ROS message object to a list"
  (cl:list 'Belief
    (cl:cons ':Xi1_0_0 (Xi1_0_0 msg))
    (cl:cons ':Xi1_1_0 (Xi1_1_0 msg))
    (cl:cons ':Ohm1_0_0 (Ohm1_0_0 msg))
    (cl:cons ':Ohm1_0_1 (Ohm1_0_1 msg))
    (cl:cons ':Ohm1_1_0 (Ohm1_1_0 msg))
    (cl:cons ':Ohm1_1_1 (Ohm1_1_1 msg))
))
