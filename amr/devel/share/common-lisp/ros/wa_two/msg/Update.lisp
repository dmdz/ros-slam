; Auto-generated. Do not edit!


(cl:in-package wa_two-msg)


;//! \htmlinclude Update.msg.html

(cl:defclass <Update> (roslisp-msg-protocol:ros-message)
  ((u1_0_0
    :reader u1_0_0
    :initarg :u1_0_0
    :type cl:float
    :initform 0.0)
   (u1_1_0
    :reader u1_1_0
    :initarg :u1_1_0
    :type cl:float
    :initform 0.0)
   (z1_0_0
    :reader z1_0_0
    :initarg :z1_0_0
    :type cl:float
    :initform 0.0)
   (z1_1_0
    :reader z1_1_0
    :initarg :z1_1_0
    :type cl:float
    :initform 0.0))
)

(cl:defclass Update (<Update>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Update>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Update)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name wa_two-msg:<Update> is deprecated: use wa_two-msg:Update instead.")))

(cl:ensure-generic-function 'u1_0_0-val :lambda-list '(m))
(cl:defmethod u1_0_0-val ((m <Update>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:u1_0_0-val is deprecated.  Use wa_two-msg:u1_0_0 instead.")
  (u1_0_0 m))

(cl:ensure-generic-function 'u1_1_0-val :lambda-list '(m))
(cl:defmethod u1_1_0-val ((m <Update>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:u1_1_0-val is deprecated.  Use wa_two-msg:u1_1_0 instead.")
  (u1_1_0 m))

(cl:ensure-generic-function 'z1_0_0-val :lambda-list '(m))
(cl:defmethod z1_0_0-val ((m <Update>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:z1_0_0-val is deprecated.  Use wa_two-msg:z1_0_0 instead.")
  (z1_0_0 m))

(cl:ensure-generic-function 'z1_1_0-val :lambda-list '(m))
(cl:defmethod z1_1_0-val ((m <Update>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader wa_two-msg:z1_1_0-val is deprecated.  Use wa_two-msg:z1_1_0 instead.")
  (z1_1_0 m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Update>) ostream)
  "Serializes a message object of type '<Update>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'u1_0_0))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'u1_1_0))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'z1_0_0))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'z1_1_0))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Update>) istream)
  "Deserializes a message object of type '<Update>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'u1_0_0) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'u1_1_0) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'z1_0_0) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'z1_1_0) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Update>)))
  "Returns string type for a message object of type '<Update>"
  "wa_two/Update")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Update)))
  "Returns string type for a message object of type 'Update"
  "wa_two/Update")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Update>)))
  "Returns md5sum for a message object of type '<Update>"
  "fff9533cd774067ff316660288526941")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Update)))
  "Returns md5sum for a message object of type 'Update"
  "fff9533cd774067ff316660288526941")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Update>)))
  "Returns full string definition for message of type '<Update>"
  (cl:format cl:nil "float32 u1_0_0~%float32 u1_1_0~%~%float32 z1_0_0~%float32 z1_1_0~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Update)))
  "Returns full string definition for message of type 'Update"
  (cl:format cl:nil "float32 u1_0_0~%float32 u1_1_0~%~%float32 z1_0_0~%float32 z1_1_0~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Update>))
  (cl:+ 0
     4
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Update>))
  "Converts a ROS message object to a list"
  (cl:list 'Update
    (cl:cons ':u1_0_0 (u1_0_0 msg))
    (cl:cons ':u1_1_0 (u1_1_0 msg))
    (cl:cons ':z1_0_0 (z1_0_0 msg))
    (cl:cons ':z1_1_0 (z1_1_0 msg))
))
