; Auto-generated. Do not edit!


(cl:in-package dji_osdk_ros-msg)


;//! \htmlinclude WaypointV2CameraActuatorFocusParam.msg.html

(cl:defclass <WaypointV2CameraActuatorFocusParam> (roslisp-msg-protocol:ros-message)
  ((x
    :reader x
    :initarg :x
    :type cl:fixnum
    :initform 0)
   (y
    :reader y
    :initarg :y
    :type cl:fixnum
    :initform 0))
)

(cl:defclass WaypointV2CameraActuatorFocusParam (<WaypointV2CameraActuatorFocusParam>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <WaypointV2CameraActuatorFocusParam>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'WaypointV2CameraActuatorFocusParam)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name dji_osdk_ros-msg:<WaypointV2CameraActuatorFocusParam> is deprecated: use dji_osdk_ros-msg:WaypointV2CameraActuatorFocusParam instead.")))

(cl:ensure-generic-function 'x-val :lambda-list '(m))
(cl:defmethod x-val ((m <WaypointV2CameraActuatorFocusParam>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader dji_osdk_ros-msg:x-val is deprecated.  Use dji_osdk_ros-msg:x instead.")
  (x m))

(cl:ensure-generic-function 'y-val :lambda-list '(m))
(cl:defmethod y-val ((m <WaypointV2CameraActuatorFocusParam>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader dji_osdk_ros-msg:y-val is deprecated.  Use dji_osdk_ros-msg:y instead.")
  (y m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<WaypointV2CameraActuatorFocusParam>)))
    "Constants for message type '<WaypointV2CameraActuatorFocusParam>"
  '((:RETRYTIMES . 1)
    (:FOCUSDELAYTIME . 0))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'WaypointV2CameraActuatorFocusParam)))
    "Constants for message type 'WaypointV2CameraActuatorFocusParam"
  '((:RETRYTIMES . 1)
    (:FOCUSDELAYTIME . 0))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <WaypointV2CameraActuatorFocusParam>) ostream)
  "Serializes a message object of type '<WaypointV2CameraActuatorFocusParam>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'x)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'x)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'y)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'y)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <WaypointV2CameraActuatorFocusParam>) istream)
  "Deserializes a message object of type '<WaypointV2CameraActuatorFocusParam>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'x)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'x)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'y)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'y)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<WaypointV2CameraActuatorFocusParam>)))
  "Returns string type for a message object of type '<WaypointV2CameraActuatorFocusParam>"
  "dji_osdk_ros/WaypointV2CameraActuatorFocusParam")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'WaypointV2CameraActuatorFocusParam)))
  "Returns string type for a message object of type 'WaypointV2CameraActuatorFocusParam"
  "dji_osdk_ros/WaypointV2CameraActuatorFocusParam")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<WaypointV2CameraActuatorFocusParam>)))
  "Returns md5sum for a message object of type '<WaypointV2CameraActuatorFocusParam>"
  "3ae752ecdbd7e3838b2e6e28ece598e3")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'WaypointV2CameraActuatorFocusParam)))
  "Returns md5sum for a message object of type 'WaypointV2CameraActuatorFocusParam"
  "3ae752ecdbd7e3838b2e6e28ece598e3")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<WaypointV2CameraActuatorFocusParam>)))
  "Returns full string definition for message of type '<WaypointV2CameraActuatorFocusParam>"
  (cl:format cl:nil "# This class defines a camera focus operation for ``DJIWaypointV2Action_DJIWaypointV2CameraActuatorParam``.~%# The lens focus target point. When the focus mode is auto, the target point~%# is the focal point. When the focus mode is manual, the target point is the zoom~%# out area if the focus assistant is enabled for the manual mode.~%#  The range for x and y is from 0.0 to 1.0. The point [0.0, 0.0] represents the top-left angle of the screen.~%  ~%  uint16 x # x axis focus point value.range: [0,10000]~%  uint16 y # y axis focus point value.range: [0,10000]~%  uint8 retryTimes = 1~%  uint8 focusDelayTime = 0~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'WaypointV2CameraActuatorFocusParam)))
  "Returns full string definition for message of type 'WaypointV2CameraActuatorFocusParam"
  (cl:format cl:nil "# This class defines a camera focus operation for ``DJIWaypointV2Action_DJIWaypointV2CameraActuatorParam``.~%# The lens focus target point. When the focus mode is auto, the target point~%# is the focal point. When the focus mode is manual, the target point is the zoom~%# out area if the focus assistant is enabled for the manual mode.~%#  The range for x and y is from 0.0 to 1.0. The point [0.0, 0.0] represents the top-left angle of the screen.~%  ~%  uint16 x # x axis focus point value.range: [0,10000]~%  uint16 y # y axis focus point value.range: [0,10000]~%  uint8 retryTimes = 1~%  uint8 focusDelayTime = 0~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <WaypointV2CameraActuatorFocusParam>))
  (cl:+ 0
     2
     2
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <WaypointV2CameraActuatorFocusParam>))
  "Converts a ROS message object to a list"
  (cl:list 'WaypointV2CameraActuatorFocusParam
    (cl:cons ':x (x msg))
    (cl:cons ':y (y msg))
))
