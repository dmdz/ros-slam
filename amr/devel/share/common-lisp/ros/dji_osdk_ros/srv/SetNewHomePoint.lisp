; Auto-generated. Do not edit!


(cl:in-package dji_osdk_ros-srv)


;//! \htmlinclude SetNewHomePoint-request.msg.html

(cl:defclass <SetNewHomePoint-request> (roslisp-msg-protocol:ros-message)
  ()
)

(cl:defclass SetNewHomePoint-request (<SetNewHomePoint-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <SetNewHomePoint-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'SetNewHomePoint-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name dji_osdk_ros-srv:<SetNewHomePoint-request> is deprecated: use dji_osdk_ros-srv:SetNewHomePoint-request instead.")))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <SetNewHomePoint-request>) ostream)
  "Serializes a message object of type '<SetNewHomePoint-request>"
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <SetNewHomePoint-request>) istream)
  "Deserializes a message object of type '<SetNewHomePoint-request>"
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<SetNewHomePoint-request>)))
  "Returns string type for a service object of type '<SetNewHomePoint-request>"
  "dji_osdk_ros/SetNewHomePointRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SetNewHomePoint-request)))
  "Returns string type for a service object of type 'SetNewHomePoint-request"
  "dji_osdk_ros/SetNewHomePointRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<SetNewHomePoint-request>)))
  "Returns md5sum for a message object of type '<SetNewHomePoint-request>"
  "eb13ac1f1354ccecb7941ee8fa2192e8")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'SetNewHomePoint-request)))
  "Returns md5sum for a message object of type 'SetNewHomePoint-request"
  "eb13ac1f1354ccecb7941ee8fa2192e8")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<SetNewHomePoint-request>)))
  "Returns full string definition for message of type '<SetNewHomePoint-request>"
  (cl:format cl:nil "~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'SetNewHomePoint-request)))
  "Returns full string definition for message of type 'SetNewHomePoint-request"
  (cl:format cl:nil "~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <SetNewHomePoint-request>))
  (cl:+ 0
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <SetNewHomePoint-request>))
  "Converts a ROS message object to a list"
  (cl:list 'SetNewHomePoint-request
))
;//! \htmlinclude SetNewHomePoint-response.msg.html

(cl:defclass <SetNewHomePoint-response> (roslisp-msg-protocol:ros-message)
  ((result
    :reader result
    :initarg :result
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass SetNewHomePoint-response (<SetNewHomePoint-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <SetNewHomePoint-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'SetNewHomePoint-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name dji_osdk_ros-srv:<SetNewHomePoint-response> is deprecated: use dji_osdk_ros-srv:SetNewHomePoint-response instead.")))

(cl:ensure-generic-function 'result-val :lambda-list '(m))
(cl:defmethod result-val ((m <SetNewHomePoint-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader dji_osdk_ros-srv:result-val is deprecated.  Use dji_osdk_ros-srv:result instead.")
  (result m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <SetNewHomePoint-response>) ostream)
  "Serializes a message object of type '<SetNewHomePoint-response>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'result) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <SetNewHomePoint-response>) istream)
  "Deserializes a message object of type '<SetNewHomePoint-response>"
    (cl:setf (cl:slot-value msg 'result) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<SetNewHomePoint-response>)))
  "Returns string type for a service object of type '<SetNewHomePoint-response>"
  "dji_osdk_ros/SetNewHomePointResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SetNewHomePoint-response)))
  "Returns string type for a service object of type 'SetNewHomePoint-response"
  "dji_osdk_ros/SetNewHomePointResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<SetNewHomePoint-response>)))
  "Returns md5sum for a message object of type '<SetNewHomePoint-response>"
  "eb13ac1f1354ccecb7941ee8fa2192e8")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'SetNewHomePoint-response)))
  "Returns md5sum for a message object of type 'SetNewHomePoint-response"
  "eb13ac1f1354ccecb7941ee8fa2192e8")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<SetNewHomePoint-response>)))
  "Returns full string definition for message of type '<SetNewHomePoint-response>"
  (cl:format cl:nil "bool result~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'SetNewHomePoint-response)))
  "Returns full string definition for message of type 'SetNewHomePoint-response"
  (cl:format cl:nil "bool result~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <SetNewHomePoint-response>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <SetNewHomePoint-response>))
  "Converts a ROS message object to a list"
  (cl:list 'SetNewHomePoint-response
    (cl:cons ':result (result msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'SetNewHomePoint)))
  'SetNewHomePoint-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'SetNewHomePoint)))
  'SetNewHomePoint-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SetNewHomePoint)))
  "Returns string type for a service object of type '<SetNewHomePoint>"
  "dji_osdk_ros/SetNewHomePoint")