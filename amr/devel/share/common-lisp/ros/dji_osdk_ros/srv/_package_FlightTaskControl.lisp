(cl:in-package dji_osdk_ros-srv)
(cl:export '(TASK-VAL
          TASK
          POS_OFFSET-VAL
          POS_OFFSET
          YAW_PARAMS-VAL
          YAW_PARAMS
          RESULT-VAL
          RESULT
          CMD_SET-VAL
          CMD_SET
          CMD_ID-VAL
          CMD_ID
          ACK_DATA-VAL
          ACK_DATA
))