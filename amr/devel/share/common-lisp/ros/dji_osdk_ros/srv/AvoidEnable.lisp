; Auto-generated. Do not edit!


(cl:in-package dji_osdk_ros-srv)


;//! \htmlinclude AvoidEnable-request.msg.html

(cl:defclass <AvoidEnable-request> (roslisp-msg-protocol:ros-message)
  ((enable
    :reader enable
    :initarg :enable
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass AvoidEnable-request (<AvoidEnable-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <AvoidEnable-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'AvoidEnable-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name dji_osdk_ros-srv:<AvoidEnable-request> is deprecated: use dji_osdk_ros-srv:AvoidEnable-request instead.")))

(cl:ensure-generic-function 'enable-val :lambda-list '(m))
(cl:defmethod enable-val ((m <AvoidEnable-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader dji_osdk_ros-srv:enable-val is deprecated.  Use dji_osdk_ros-srv:enable instead.")
  (enable m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <AvoidEnable-request>) ostream)
  "Serializes a message object of type '<AvoidEnable-request>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'enable) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <AvoidEnable-request>) istream)
  "Deserializes a message object of type '<AvoidEnable-request>"
    (cl:setf (cl:slot-value msg 'enable) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<AvoidEnable-request>)))
  "Returns string type for a service object of type '<AvoidEnable-request>"
  "dji_osdk_ros/AvoidEnableRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'AvoidEnable-request)))
  "Returns string type for a service object of type 'AvoidEnable-request"
  "dji_osdk_ros/AvoidEnableRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<AvoidEnable-request>)))
  "Returns md5sum for a message object of type '<AvoidEnable-request>"
  "29d58f387352c15c4e4f5763022ae875")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'AvoidEnable-request)))
  "Returns md5sum for a message object of type 'AvoidEnable-request"
  "29d58f387352c15c4e4f5763022ae875")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<AvoidEnable-request>)))
  "Returns full string definition for message of type '<AvoidEnable-request>"
  (cl:format cl:nil "bool enable~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'AvoidEnable-request)))
  "Returns full string definition for message of type 'AvoidEnable-request"
  (cl:format cl:nil "bool enable~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <AvoidEnable-request>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <AvoidEnable-request>))
  "Converts a ROS message object to a list"
  (cl:list 'AvoidEnable-request
    (cl:cons ':enable (enable msg))
))
;//! \htmlinclude AvoidEnable-response.msg.html

(cl:defclass <AvoidEnable-response> (roslisp-msg-protocol:ros-message)
  ((result
    :reader result
    :initarg :result
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass AvoidEnable-response (<AvoidEnable-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <AvoidEnable-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'AvoidEnable-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name dji_osdk_ros-srv:<AvoidEnable-response> is deprecated: use dji_osdk_ros-srv:AvoidEnable-response instead.")))

(cl:ensure-generic-function 'result-val :lambda-list '(m))
(cl:defmethod result-val ((m <AvoidEnable-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader dji_osdk_ros-srv:result-val is deprecated.  Use dji_osdk_ros-srv:result instead.")
  (result m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <AvoidEnable-response>) ostream)
  "Serializes a message object of type '<AvoidEnable-response>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'result) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <AvoidEnable-response>) istream)
  "Deserializes a message object of type '<AvoidEnable-response>"
    (cl:setf (cl:slot-value msg 'result) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<AvoidEnable-response>)))
  "Returns string type for a service object of type '<AvoidEnable-response>"
  "dji_osdk_ros/AvoidEnableResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'AvoidEnable-response)))
  "Returns string type for a service object of type 'AvoidEnable-response"
  "dji_osdk_ros/AvoidEnableResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<AvoidEnable-response>)))
  "Returns md5sum for a message object of type '<AvoidEnable-response>"
  "29d58f387352c15c4e4f5763022ae875")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'AvoidEnable-response)))
  "Returns md5sum for a message object of type 'AvoidEnable-response"
  "29d58f387352c15c4e4f5763022ae875")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<AvoidEnable-response>)))
  "Returns full string definition for message of type '<AvoidEnable-response>"
  (cl:format cl:nil "bool result~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'AvoidEnable-response)))
  "Returns full string definition for message of type 'AvoidEnable-response"
  (cl:format cl:nil "bool result~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <AvoidEnable-response>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <AvoidEnable-response>))
  "Converts a ROS message object to a list"
  (cl:list 'AvoidEnable-response
    (cl:cons ':result (result msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'AvoidEnable)))
  'AvoidEnable-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'AvoidEnable)))
  'AvoidEnable-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'AvoidEnable)))
  "Returns string type for a service object of type '<AvoidEnable>"
  "dji_osdk_ros/AvoidEnable")