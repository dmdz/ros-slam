; Auto-generated. Do not edit!


(cl:in-package dji_osdk_ros-srv)


;//! \htmlinclude FlightTaskControl-request.msg.html

(cl:defclass <FlightTaskControl-request> (roslisp-msg-protocol:ros-message)
  ((task
    :reader task
    :initarg :task
    :type cl:fixnum
    :initform 0)
   (pos_offset
    :reader pos_offset
    :initarg :pos_offset
    :type (cl:vector cl:float)
   :initform (cl:make-array 0 :element-type 'cl:float :initial-element 0.0))
   (yaw_params
    :reader yaw_params
    :initarg :yaw_params
    :type (cl:vector cl:float)
   :initform (cl:make-array 0 :element-type 'cl:float :initial-element 0.0)))
)

(cl:defclass FlightTaskControl-request (<FlightTaskControl-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <FlightTaskControl-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'FlightTaskControl-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name dji_osdk_ros-srv:<FlightTaskControl-request> is deprecated: use dji_osdk_ros-srv:FlightTaskControl-request instead.")))

(cl:ensure-generic-function 'task-val :lambda-list '(m))
(cl:defmethod task-val ((m <FlightTaskControl-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader dji_osdk_ros-srv:task-val is deprecated.  Use dji_osdk_ros-srv:task instead.")
  (task m))

(cl:ensure-generic-function 'pos_offset-val :lambda-list '(m))
(cl:defmethod pos_offset-val ((m <FlightTaskControl-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader dji_osdk_ros-srv:pos_offset-val is deprecated.  Use dji_osdk_ros-srv:pos_offset instead.")
  (pos_offset m))

(cl:ensure-generic-function 'yaw_params-val :lambda-list '(m))
(cl:defmethod yaw_params-val ((m <FlightTaskControl-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader dji_osdk_ros-srv:yaw_params-val is deprecated.  Use dji_osdk_ros-srv:yaw_params instead.")
  (yaw_params m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<FlightTaskControl-request>)))
    "Constants for message type '<FlightTaskControl-request>"
  '((:TASK_GOHOME . 1)
    (:TASK_GO_LOCAL_POS . 2)
    (:TASK_GOHOME_AND_CONFIRM_LANDING . 3)
    (:TASK_TAKEOFF . 4)
    (:TASK_LAND . 6))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'FlightTaskControl-request)))
    "Constants for message type 'FlightTaskControl-request"
  '((:TASK_GOHOME . 1)
    (:TASK_GO_LOCAL_POS . 2)
    (:TASK_GOHOME_AND_CONFIRM_LANDING . 3)
    (:TASK_TAKEOFF . 4)
    (:TASK_LAND . 6))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <FlightTaskControl-request>) ostream)
  "Serializes a message object of type '<FlightTaskControl-request>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'task)) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'pos_offset))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-double-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream)))
   (cl:slot-value msg 'pos_offset))
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'yaw_params))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-double-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream)))
   (cl:slot-value msg 'yaw_params))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <FlightTaskControl-request>) istream)
  "Deserializes a message object of type '<FlightTaskControl-request>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'task)) (cl:read-byte istream))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'pos_offset) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'pos_offset)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-double-float-bits bits))))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'yaw_params) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'yaw_params)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-double-float-bits bits))))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<FlightTaskControl-request>)))
  "Returns string type for a service object of type '<FlightTaskControl-request>"
  "dji_osdk_ros/FlightTaskControlRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'FlightTaskControl-request)))
  "Returns string type for a service object of type 'FlightTaskControl-request"
  "dji_osdk_ros/FlightTaskControlRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<FlightTaskControl-request>)))
  "Returns md5sum for a message object of type '<FlightTaskControl-request>"
  "2f0fa5b45abe3a96e38cf4ab21baa780")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'FlightTaskControl-request)))
  "Returns md5sum for a message object of type 'FlightTaskControl-request"
  "2f0fa5b45abe3a96e38cf4ab21baa780")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<FlightTaskControl-request>)))
  "Returns full string definition for message of type '<FlightTaskControl-request>"
  (cl:format cl:nil "#constant for tasks~%uint8 TASK_GOHOME = 1~%uint8 TASK_GO_LOCAL_POS = 2~%uint8 TASK_GOHOME_AND_CONFIRM_LANDING = 3~%uint8 TASK_TAKEOFF = 4~%uint8 TASK_LAND = 6~%~%#request~%uint8 task    # see constants above for possible tasks~%float64[] pos_offset #A vector contains that position_x_offset, position_y_offset, position_z_offset in order~%float64[] yaw_params #A vector contains that yaw_desired, position_threshold(Meter), yaw_threshold(Degree)~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'FlightTaskControl-request)))
  "Returns full string definition for message of type 'FlightTaskControl-request"
  (cl:format cl:nil "#constant for tasks~%uint8 TASK_GOHOME = 1~%uint8 TASK_GO_LOCAL_POS = 2~%uint8 TASK_GOHOME_AND_CONFIRM_LANDING = 3~%uint8 TASK_TAKEOFF = 4~%uint8 TASK_LAND = 6~%~%#request~%uint8 task    # see constants above for possible tasks~%float64[] pos_offset #A vector contains that position_x_offset, position_y_offset, position_z_offset in order~%float64[] yaw_params #A vector contains that yaw_desired, position_threshold(Meter), yaw_threshold(Degree)~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <FlightTaskControl-request>))
  (cl:+ 0
     1
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'pos_offset) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 8)))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'yaw_params) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 8)))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <FlightTaskControl-request>))
  "Converts a ROS message object to a list"
  (cl:list 'FlightTaskControl-request
    (cl:cons ':task (task msg))
    (cl:cons ':pos_offset (pos_offset msg))
    (cl:cons ':yaw_params (yaw_params msg))
))
;//! \htmlinclude FlightTaskControl-response.msg.html

(cl:defclass <FlightTaskControl-response> (roslisp-msg-protocol:ros-message)
  ((result
    :reader result
    :initarg :result
    :type cl:boolean
    :initform cl:nil)
   (cmd_set
    :reader cmd_set
    :initarg :cmd_set
    :type cl:fixnum
    :initform 0)
   (cmd_id
    :reader cmd_id
    :initarg :cmd_id
    :type cl:fixnum
    :initform 0)
   (ack_data
    :reader ack_data
    :initarg :ack_data
    :type cl:integer
    :initform 0))
)

(cl:defclass FlightTaskControl-response (<FlightTaskControl-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <FlightTaskControl-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'FlightTaskControl-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name dji_osdk_ros-srv:<FlightTaskControl-response> is deprecated: use dji_osdk_ros-srv:FlightTaskControl-response instead.")))

(cl:ensure-generic-function 'result-val :lambda-list '(m))
(cl:defmethod result-val ((m <FlightTaskControl-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader dji_osdk_ros-srv:result-val is deprecated.  Use dji_osdk_ros-srv:result instead.")
  (result m))

(cl:ensure-generic-function 'cmd_set-val :lambda-list '(m))
(cl:defmethod cmd_set-val ((m <FlightTaskControl-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader dji_osdk_ros-srv:cmd_set-val is deprecated.  Use dji_osdk_ros-srv:cmd_set instead.")
  (cmd_set m))

(cl:ensure-generic-function 'cmd_id-val :lambda-list '(m))
(cl:defmethod cmd_id-val ((m <FlightTaskControl-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader dji_osdk_ros-srv:cmd_id-val is deprecated.  Use dji_osdk_ros-srv:cmd_id instead.")
  (cmd_id m))

(cl:ensure-generic-function 'ack_data-val :lambda-list '(m))
(cl:defmethod ack_data-val ((m <FlightTaskControl-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader dji_osdk_ros-srv:ack_data-val is deprecated.  Use dji_osdk_ros-srv:ack_data instead.")
  (ack_data m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <FlightTaskControl-response>) ostream)
  "Serializes a message object of type '<FlightTaskControl-response>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'result) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'cmd_set)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'cmd_id)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'ack_data)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'ack_data)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'ack_data)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'ack_data)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <FlightTaskControl-response>) istream)
  "Deserializes a message object of type '<FlightTaskControl-response>"
    (cl:setf (cl:slot-value msg 'result) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'cmd_set)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'cmd_id)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'ack_data)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'ack_data)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'ack_data)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'ack_data)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<FlightTaskControl-response>)))
  "Returns string type for a service object of type '<FlightTaskControl-response>"
  "dji_osdk_ros/FlightTaskControlResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'FlightTaskControl-response)))
  "Returns string type for a service object of type 'FlightTaskControl-response"
  "dji_osdk_ros/FlightTaskControlResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<FlightTaskControl-response>)))
  "Returns md5sum for a message object of type '<FlightTaskControl-response>"
  "2f0fa5b45abe3a96e38cf4ab21baa780")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'FlightTaskControl-response)))
  "Returns md5sum for a message object of type 'FlightTaskControl-response"
  "2f0fa5b45abe3a96e38cf4ab21baa780")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<FlightTaskControl-response>)))
  "Returns full string definition for message of type '<FlightTaskControl-response>"
  (cl:format cl:nil "bool result~%# for debugging usage~%uint8 cmd_set~%uint8 cmd_id~%uint32 ack_data~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'FlightTaskControl-response)))
  "Returns full string definition for message of type 'FlightTaskControl-response"
  (cl:format cl:nil "bool result~%# for debugging usage~%uint8 cmd_set~%uint8 cmd_id~%uint32 ack_data~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <FlightTaskControl-response>))
  (cl:+ 0
     1
     1
     1
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <FlightTaskControl-response>))
  "Converts a ROS message object to a list"
  (cl:list 'FlightTaskControl-response
    (cl:cons ':result (result msg))
    (cl:cons ':cmd_set (cmd_set msg))
    (cl:cons ':cmd_id (cmd_id msg))
    (cl:cons ':ack_data (ack_data msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'FlightTaskControl)))
  'FlightTaskControl-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'FlightTaskControl)))
  'FlightTaskControl-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'FlightTaskControl)))
  "Returns string type for a service object of type '<FlightTaskControl>"
  "dji_osdk_ros/FlightTaskControl")