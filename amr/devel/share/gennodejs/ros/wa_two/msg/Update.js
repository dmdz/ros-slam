// Auto-generated. Do not edit!

// (in-package wa_two.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class Update {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.u1_0_0 = null;
      this.u1_1_0 = null;
      this.z1_0_0 = null;
      this.z1_1_0 = null;
    }
    else {
      if (initObj.hasOwnProperty('u1_0_0')) {
        this.u1_0_0 = initObj.u1_0_0
      }
      else {
        this.u1_0_0 = 0.0;
      }
      if (initObj.hasOwnProperty('u1_1_0')) {
        this.u1_1_0 = initObj.u1_1_0
      }
      else {
        this.u1_1_0 = 0.0;
      }
      if (initObj.hasOwnProperty('z1_0_0')) {
        this.z1_0_0 = initObj.z1_0_0
      }
      else {
        this.z1_0_0 = 0.0;
      }
      if (initObj.hasOwnProperty('z1_1_0')) {
        this.z1_1_0 = initObj.z1_1_0
      }
      else {
        this.z1_1_0 = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type Update
    // Serialize message field [u1_0_0]
    bufferOffset = _serializer.float32(obj.u1_0_0, buffer, bufferOffset);
    // Serialize message field [u1_1_0]
    bufferOffset = _serializer.float32(obj.u1_1_0, buffer, bufferOffset);
    // Serialize message field [z1_0_0]
    bufferOffset = _serializer.float32(obj.z1_0_0, buffer, bufferOffset);
    // Serialize message field [z1_1_0]
    bufferOffset = _serializer.float32(obj.z1_1_0, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type Update
    let len;
    let data = new Update(null);
    // Deserialize message field [u1_0_0]
    data.u1_0_0 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [u1_1_0]
    data.u1_1_0 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [z1_0_0]
    data.z1_0_0 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [z1_1_0]
    data.z1_1_0 = _deserializer.float32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 16;
  }

  static datatype() {
    // Returns string type for a message object
    return 'wa_two/Update';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'fff9533cd774067ff316660288526941';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float32 u1_0_0
    float32 u1_1_0
    
    float32 z1_0_0
    float32 z1_1_0
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new Update(null);
    if (msg.u1_0_0 !== undefined) {
      resolved.u1_0_0 = msg.u1_0_0;
    }
    else {
      resolved.u1_0_0 = 0.0
    }

    if (msg.u1_1_0 !== undefined) {
      resolved.u1_1_0 = msg.u1_1_0;
    }
    else {
      resolved.u1_1_0 = 0.0
    }

    if (msg.z1_0_0 !== undefined) {
      resolved.z1_0_0 = msg.z1_0_0;
    }
    else {
      resolved.z1_0_0 = 0.0
    }

    if (msg.z1_1_0 !== undefined) {
      resolved.z1_1_0 = msg.z1_1_0;
    }
    else {
      resolved.z1_1_0 = 0.0
    }

    return resolved;
    }
};

module.exports = Update;
