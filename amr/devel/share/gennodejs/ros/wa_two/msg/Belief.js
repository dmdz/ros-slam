// Auto-generated. Do not edit!

// (in-package wa_two.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class Belief {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.Xi1_0_0 = null;
      this.Xi1_1_0 = null;
      this.Ohm1_0_0 = null;
      this.Ohm1_0_1 = null;
      this.Ohm1_1_0 = null;
      this.Ohm1_1_1 = null;
    }
    else {
      if (initObj.hasOwnProperty('Xi1_0_0')) {
        this.Xi1_0_0 = initObj.Xi1_0_0
      }
      else {
        this.Xi1_0_0 = 0.0;
      }
      if (initObj.hasOwnProperty('Xi1_1_0')) {
        this.Xi1_1_0 = initObj.Xi1_1_0
      }
      else {
        this.Xi1_1_0 = 0.0;
      }
      if (initObj.hasOwnProperty('Ohm1_0_0')) {
        this.Ohm1_0_0 = initObj.Ohm1_0_0
      }
      else {
        this.Ohm1_0_0 = 0.0;
      }
      if (initObj.hasOwnProperty('Ohm1_0_1')) {
        this.Ohm1_0_1 = initObj.Ohm1_0_1
      }
      else {
        this.Ohm1_0_1 = 0.0;
      }
      if (initObj.hasOwnProperty('Ohm1_1_0')) {
        this.Ohm1_1_0 = initObj.Ohm1_1_0
      }
      else {
        this.Ohm1_1_0 = 0.0;
      }
      if (initObj.hasOwnProperty('Ohm1_1_1')) {
        this.Ohm1_1_1 = initObj.Ohm1_1_1
      }
      else {
        this.Ohm1_1_1 = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type Belief
    // Serialize message field [Xi1_0_0]
    bufferOffset = _serializer.float32(obj.Xi1_0_0, buffer, bufferOffset);
    // Serialize message field [Xi1_1_0]
    bufferOffset = _serializer.float32(obj.Xi1_1_0, buffer, bufferOffset);
    // Serialize message field [Ohm1_0_0]
    bufferOffset = _serializer.float32(obj.Ohm1_0_0, buffer, bufferOffset);
    // Serialize message field [Ohm1_0_1]
    bufferOffset = _serializer.float32(obj.Ohm1_0_1, buffer, bufferOffset);
    // Serialize message field [Ohm1_1_0]
    bufferOffset = _serializer.float32(obj.Ohm1_1_0, buffer, bufferOffset);
    // Serialize message field [Ohm1_1_1]
    bufferOffset = _serializer.float32(obj.Ohm1_1_1, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type Belief
    let len;
    let data = new Belief(null);
    // Deserialize message field [Xi1_0_0]
    data.Xi1_0_0 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [Xi1_1_0]
    data.Xi1_1_0 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [Ohm1_0_0]
    data.Ohm1_0_0 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [Ohm1_0_1]
    data.Ohm1_0_1 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [Ohm1_1_0]
    data.Ohm1_1_0 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [Ohm1_1_1]
    data.Ohm1_1_1 = _deserializer.float32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 24;
  }

  static datatype() {
    // Returns string type for a message object
    return 'wa_two/Belief';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'a684de5435d4050cd6350a964c19142c';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float32 Xi1_0_0
    float32 Xi1_1_0
    
    float32 Ohm1_0_0
    float32 Ohm1_0_1
    float32 Ohm1_1_0
    float32 Ohm1_1_1
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new Belief(null);
    if (msg.Xi1_0_0 !== undefined) {
      resolved.Xi1_0_0 = msg.Xi1_0_0;
    }
    else {
      resolved.Xi1_0_0 = 0.0
    }

    if (msg.Xi1_1_0 !== undefined) {
      resolved.Xi1_1_0 = msg.Xi1_1_0;
    }
    else {
      resolved.Xi1_1_0 = 0.0
    }

    if (msg.Ohm1_0_0 !== undefined) {
      resolved.Ohm1_0_0 = msg.Ohm1_0_0;
    }
    else {
      resolved.Ohm1_0_0 = 0.0
    }

    if (msg.Ohm1_0_1 !== undefined) {
      resolved.Ohm1_0_1 = msg.Ohm1_0_1;
    }
    else {
      resolved.Ohm1_0_1 = 0.0
    }

    if (msg.Ohm1_1_0 !== undefined) {
      resolved.Ohm1_1_0 = msg.Ohm1_1_0;
    }
    else {
      resolved.Ohm1_1_0 = 0.0
    }

    if (msg.Ohm1_1_1 !== undefined) {
      resolved.Ohm1_1_1 = msg.Ohm1_1_1;
    }
    else {
      resolved.Ohm1_1_1 = 0.0
    }

    return resolved;
    }
};

module.exports = Belief;
