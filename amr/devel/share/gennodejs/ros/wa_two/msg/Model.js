// Auto-generated. Do not edit!

// (in-package wa_two.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class Model {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.Xi0_0_0 = null;
      this.Xi0_1_0 = null;
      this.Ohm0_0_0 = null;
      this.Ohm0_0_1 = null;
      this.Ohm0_1_0 = null;
      this.Ohm0_1_1 = null;
      this.A_0_0 = null;
      this.A_0_1 = null;
      this.A_1_0 = null;
      this.A_1_1 = null;
      this.B_0_0 = null;
      this.B_0_1 = null;
      this.B_1_0 = null;
      this.B_1_1 = null;
      this.C_0_0 = null;
      this.C_0_1 = null;
      this.C_1_0 = null;
      this.C_1_1 = null;
      this.R_0_0 = null;
      this.R_0_1 = null;
      this.R_1_0 = null;
      this.R_1_1 = null;
      this.Q_0_0 = null;
      this.Q_0_1 = null;
      this.Q_1_0 = null;
      this.Q_1_1 = null;
    }
    else {
      if (initObj.hasOwnProperty('Xi0_0_0')) {
        this.Xi0_0_0 = initObj.Xi0_0_0
      }
      else {
        this.Xi0_0_0 = 0.0;
      }
      if (initObj.hasOwnProperty('Xi0_1_0')) {
        this.Xi0_1_0 = initObj.Xi0_1_0
      }
      else {
        this.Xi0_1_0 = 0.0;
      }
      if (initObj.hasOwnProperty('Ohm0_0_0')) {
        this.Ohm0_0_0 = initObj.Ohm0_0_0
      }
      else {
        this.Ohm0_0_0 = 0.0;
      }
      if (initObj.hasOwnProperty('Ohm0_0_1')) {
        this.Ohm0_0_1 = initObj.Ohm0_0_1
      }
      else {
        this.Ohm0_0_1 = 0.0;
      }
      if (initObj.hasOwnProperty('Ohm0_1_0')) {
        this.Ohm0_1_0 = initObj.Ohm0_1_0
      }
      else {
        this.Ohm0_1_0 = 0.0;
      }
      if (initObj.hasOwnProperty('Ohm0_1_1')) {
        this.Ohm0_1_1 = initObj.Ohm0_1_1
      }
      else {
        this.Ohm0_1_1 = 0.0;
      }
      if (initObj.hasOwnProperty('A_0_0')) {
        this.A_0_0 = initObj.A_0_0
      }
      else {
        this.A_0_0 = 0.0;
      }
      if (initObj.hasOwnProperty('A_0_1')) {
        this.A_0_1 = initObj.A_0_1
      }
      else {
        this.A_0_1 = 0.0;
      }
      if (initObj.hasOwnProperty('A_1_0')) {
        this.A_1_0 = initObj.A_1_0
      }
      else {
        this.A_1_0 = 0.0;
      }
      if (initObj.hasOwnProperty('A_1_1')) {
        this.A_1_1 = initObj.A_1_1
      }
      else {
        this.A_1_1 = 0.0;
      }
      if (initObj.hasOwnProperty('B_0_0')) {
        this.B_0_0 = initObj.B_0_0
      }
      else {
        this.B_0_0 = 0.0;
      }
      if (initObj.hasOwnProperty('B_0_1')) {
        this.B_0_1 = initObj.B_0_1
      }
      else {
        this.B_0_1 = 0.0;
      }
      if (initObj.hasOwnProperty('B_1_0')) {
        this.B_1_0 = initObj.B_1_0
      }
      else {
        this.B_1_0 = 0.0;
      }
      if (initObj.hasOwnProperty('B_1_1')) {
        this.B_1_1 = initObj.B_1_1
      }
      else {
        this.B_1_1 = 0.0;
      }
      if (initObj.hasOwnProperty('C_0_0')) {
        this.C_0_0 = initObj.C_0_0
      }
      else {
        this.C_0_0 = 0.0;
      }
      if (initObj.hasOwnProperty('C_0_1')) {
        this.C_0_1 = initObj.C_0_1
      }
      else {
        this.C_0_1 = 0.0;
      }
      if (initObj.hasOwnProperty('C_1_0')) {
        this.C_1_0 = initObj.C_1_0
      }
      else {
        this.C_1_0 = 0.0;
      }
      if (initObj.hasOwnProperty('C_1_1')) {
        this.C_1_1 = initObj.C_1_1
      }
      else {
        this.C_1_1 = 0.0;
      }
      if (initObj.hasOwnProperty('R_0_0')) {
        this.R_0_0 = initObj.R_0_0
      }
      else {
        this.R_0_0 = 0.0;
      }
      if (initObj.hasOwnProperty('R_0_1')) {
        this.R_0_1 = initObj.R_0_1
      }
      else {
        this.R_0_1 = 0.0;
      }
      if (initObj.hasOwnProperty('R_1_0')) {
        this.R_1_0 = initObj.R_1_0
      }
      else {
        this.R_1_0 = 0.0;
      }
      if (initObj.hasOwnProperty('R_1_1')) {
        this.R_1_1 = initObj.R_1_1
      }
      else {
        this.R_1_1 = 0.0;
      }
      if (initObj.hasOwnProperty('Q_0_0')) {
        this.Q_0_0 = initObj.Q_0_0
      }
      else {
        this.Q_0_0 = 0.0;
      }
      if (initObj.hasOwnProperty('Q_0_1')) {
        this.Q_0_1 = initObj.Q_0_1
      }
      else {
        this.Q_0_1 = 0.0;
      }
      if (initObj.hasOwnProperty('Q_1_0')) {
        this.Q_1_0 = initObj.Q_1_0
      }
      else {
        this.Q_1_0 = 0.0;
      }
      if (initObj.hasOwnProperty('Q_1_1')) {
        this.Q_1_1 = initObj.Q_1_1
      }
      else {
        this.Q_1_1 = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type Model
    // Serialize message field [Xi0_0_0]
    bufferOffset = _serializer.float32(obj.Xi0_0_0, buffer, bufferOffset);
    // Serialize message field [Xi0_1_0]
    bufferOffset = _serializer.float32(obj.Xi0_1_0, buffer, bufferOffset);
    // Serialize message field [Ohm0_0_0]
    bufferOffset = _serializer.float32(obj.Ohm0_0_0, buffer, bufferOffset);
    // Serialize message field [Ohm0_0_1]
    bufferOffset = _serializer.float32(obj.Ohm0_0_1, buffer, bufferOffset);
    // Serialize message field [Ohm0_1_0]
    bufferOffset = _serializer.float32(obj.Ohm0_1_0, buffer, bufferOffset);
    // Serialize message field [Ohm0_1_1]
    bufferOffset = _serializer.float32(obj.Ohm0_1_1, buffer, bufferOffset);
    // Serialize message field [A_0_0]
    bufferOffset = _serializer.float32(obj.A_0_0, buffer, bufferOffset);
    // Serialize message field [A_0_1]
    bufferOffset = _serializer.float32(obj.A_0_1, buffer, bufferOffset);
    // Serialize message field [A_1_0]
    bufferOffset = _serializer.float32(obj.A_1_0, buffer, bufferOffset);
    // Serialize message field [A_1_1]
    bufferOffset = _serializer.float32(obj.A_1_1, buffer, bufferOffset);
    // Serialize message field [B_0_0]
    bufferOffset = _serializer.float32(obj.B_0_0, buffer, bufferOffset);
    // Serialize message field [B_0_1]
    bufferOffset = _serializer.float32(obj.B_0_1, buffer, bufferOffset);
    // Serialize message field [B_1_0]
    bufferOffset = _serializer.float32(obj.B_1_0, buffer, bufferOffset);
    // Serialize message field [B_1_1]
    bufferOffset = _serializer.float32(obj.B_1_1, buffer, bufferOffset);
    // Serialize message field [C_0_0]
    bufferOffset = _serializer.float32(obj.C_0_0, buffer, bufferOffset);
    // Serialize message field [C_0_1]
    bufferOffset = _serializer.float32(obj.C_0_1, buffer, bufferOffset);
    // Serialize message field [C_1_0]
    bufferOffset = _serializer.float32(obj.C_1_0, buffer, bufferOffset);
    // Serialize message field [C_1_1]
    bufferOffset = _serializer.float32(obj.C_1_1, buffer, bufferOffset);
    // Serialize message field [R_0_0]
    bufferOffset = _serializer.float32(obj.R_0_0, buffer, bufferOffset);
    // Serialize message field [R_0_1]
    bufferOffset = _serializer.float32(obj.R_0_1, buffer, bufferOffset);
    // Serialize message field [R_1_0]
    bufferOffset = _serializer.float32(obj.R_1_0, buffer, bufferOffset);
    // Serialize message field [R_1_1]
    bufferOffset = _serializer.float32(obj.R_1_1, buffer, bufferOffset);
    // Serialize message field [Q_0_0]
    bufferOffset = _serializer.float32(obj.Q_0_0, buffer, bufferOffset);
    // Serialize message field [Q_0_1]
    bufferOffset = _serializer.float32(obj.Q_0_1, buffer, bufferOffset);
    // Serialize message field [Q_1_0]
    bufferOffset = _serializer.float32(obj.Q_1_0, buffer, bufferOffset);
    // Serialize message field [Q_1_1]
    bufferOffset = _serializer.float32(obj.Q_1_1, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type Model
    let len;
    let data = new Model(null);
    // Deserialize message field [Xi0_0_0]
    data.Xi0_0_0 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [Xi0_1_0]
    data.Xi0_1_0 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [Ohm0_0_0]
    data.Ohm0_0_0 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [Ohm0_0_1]
    data.Ohm0_0_1 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [Ohm0_1_0]
    data.Ohm0_1_0 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [Ohm0_1_1]
    data.Ohm0_1_1 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [A_0_0]
    data.A_0_0 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [A_0_1]
    data.A_0_1 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [A_1_0]
    data.A_1_0 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [A_1_1]
    data.A_1_1 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [B_0_0]
    data.B_0_0 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [B_0_1]
    data.B_0_1 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [B_1_0]
    data.B_1_0 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [B_1_1]
    data.B_1_1 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [C_0_0]
    data.C_0_0 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [C_0_1]
    data.C_0_1 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [C_1_0]
    data.C_1_0 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [C_1_1]
    data.C_1_1 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [R_0_0]
    data.R_0_0 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [R_0_1]
    data.R_0_1 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [R_1_0]
    data.R_1_0 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [R_1_1]
    data.R_1_1 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [Q_0_0]
    data.Q_0_0 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [Q_0_1]
    data.Q_0_1 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [Q_1_0]
    data.Q_1_0 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [Q_1_1]
    data.Q_1_1 = _deserializer.float32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 104;
  }

  static datatype() {
    // Returns string type for a message object
    return 'wa_two/Model';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '86a1455dd7ccb59c04c3e718684e2f4f';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float32 Xi0_0_0
    float32 Xi0_1_0
    
    float32 Ohm0_0_0
    float32 Ohm0_0_1
    float32 Ohm0_1_0
    float32 Ohm0_1_1
    
    float32 A_0_0
    float32 A_0_1
    float32 A_1_0
    float32 A_1_1
    
    float32 B_0_0
    float32 B_0_1
    float32 B_1_0
    float32 B_1_1
    
    float32 C_0_0
    float32 C_0_1
    float32 C_1_0
    float32 C_1_1
    
    float32 R_0_0
    float32 R_0_1
    float32 R_1_0
    float32 R_1_1
    
    float32 Q_0_0
    float32 Q_0_1
    float32 Q_1_0
    float32 Q_1_1
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new Model(null);
    if (msg.Xi0_0_0 !== undefined) {
      resolved.Xi0_0_0 = msg.Xi0_0_0;
    }
    else {
      resolved.Xi0_0_0 = 0.0
    }

    if (msg.Xi0_1_0 !== undefined) {
      resolved.Xi0_1_0 = msg.Xi0_1_0;
    }
    else {
      resolved.Xi0_1_0 = 0.0
    }

    if (msg.Ohm0_0_0 !== undefined) {
      resolved.Ohm0_0_0 = msg.Ohm0_0_0;
    }
    else {
      resolved.Ohm0_0_0 = 0.0
    }

    if (msg.Ohm0_0_1 !== undefined) {
      resolved.Ohm0_0_1 = msg.Ohm0_0_1;
    }
    else {
      resolved.Ohm0_0_1 = 0.0
    }

    if (msg.Ohm0_1_0 !== undefined) {
      resolved.Ohm0_1_0 = msg.Ohm0_1_0;
    }
    else {
      resolved.Ohm0_1_0 = 0.0
    }

    if (msg.Ohm0_1_1 !== undefined) {
      resolved.Ohm0_1_1 = msg.Ohm0_1_1;
    }
    else {
      resolved.Ohm0_1_1 = 0.0
    }

    if (msg.A_0_0 !== undefined) {
      resolved.A_0_0 = msg.A_0_0;
    }
    else {
      resolved.A_0_0 = 0.0
    }

    if (msg.A_0_1 !== undefined) {
      resolved.A_0_1 = msg.A_0_1;
    }
    else {
      resolved.A_0_1 = 0.0
    }

    if (msg.A_1_0 !== undefined) {
      resolved.A_1_0 = msg.A_1_0;
    }
    else {
      resolved.A_1_0 = 0.0
    }

    if (msg.A_1_1 !== undefined) {
      resolved.A_1_1 = msg.A_1_1;
    }
    else {
      resolved.A_1_1 = 0.0
    }

    if (msg.B_0_0 !== undefined) {
      resolved.B_0_0 = msg.B_0_0;
    }
    else {
      resolved.B_0_0 = 0.0
    }

    if (msg.B_0_1 !== undefined) {
      resolved.B_0_1 = msg.B_0_1;
    }
    else {
      resolved.B_0_1 = 0.0
    }

    if (msg.B_1_0 !== undefined) {
      resolved.B_1_0 = msg.B_1_0;
    }
    else {
      resolved.B_1_0 = 0.0
    }

    if (msg.B_1_1 !== undefined) {
      resolved.B_1_1 = msg.B_1_1;
    }
    else {
      resolved.B_1_1 = 0.0
    }

    if (msg.C_0_0 !== undefined) {
      resolved.C_0_0 = msg.C_0_0;
    }
    else {
      resolved.C_0_0 = 0.0
    }

    if (msg.C_0_1 !== undefined) {
      resolved.C_0_1 = msg.C_0_1;
    }
    else {
      resolved.C_0_1 = 0.0
    }

    if (msg.C_1_0 !== undefined) {
      resolved.C_1_0 = msg.C_1_0;
    }
    else {
      resolved.C_1_0 = 0.0
    }

    if (msg.C_1_1 !== undefined) {
      resolved.C_1_1 = msg.C_1_1;
    }
    else {
      resolved.C_1_1 = 0.0
    }

    if (msg.R_0_0 !== undefined) {
      resolved.R_0_0 = msg.R_0_0;
    }
    else {
      resolved.R_0_0 = 0.0
    }

    if (msg.R_0_1 !== undefined) {
      resolved.R_0_1 = msg.R_0_1;
    }
    else {
      resolved.R_0_1 = 0.0
    }

    if (msg.R_1_0 !== undefined) {
      resolved.R_1_0 = msg.R_1_0;
    }
    else {
      resolved.R_1_0 = 0.0
    }

    if (msg.R_1_1 !== undefined) {
      resolved.R_1_1 = msg.R_1_1;
    }
    else {
      resolved.R_1_1 = 0.0
    }

    if (msg.Q_0_0 !== undefined) {
      resolved.Q_0_0 = msg.Q_0_0;
    }
    else {
      resolved.Q_0_0 = 0.0
    }

    if (msg.Q_0_1 !== undefined) {
      resolved.Q_0_1 = msg.Q_0_1;
    }
    else {
      resolved.Q_0_1 = 0.0
    }

    if (msg.Q_1_0 !== undefined) {
      resolved.Q_1_0 = msg.Q_1_0;
    }
    else {
      resolved.Q_1_0 = 0.0
    }

    if (msg.Q_1_1 !== undefined) {
      resolved.Q_1_1 = msg.Q_1_1;
    }
    else {
      resolved.Q_1_1 = 0.0
    }

    return resolved;
    }
};

module.exports = Model;
