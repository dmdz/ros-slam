
"use strict";

let Landmarks = require('./Landmarks.js');
let Landmark = require('./Landmark.js');
let Observation = require('./Observation.js');
let Observations = require('./Observations.js');

module.exports = {
  Landmarks: Landmarks,
  Landmark: Landmark,
  Observation: Observation,
  Observations: Observations,
};
