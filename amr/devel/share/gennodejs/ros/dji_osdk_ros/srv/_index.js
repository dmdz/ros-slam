
"use strict";

let GimbalAction = require('./GimbalAction.js')
let DroneTaskControl = require('./DroneTaskControl.js')
let FlightTaskControl = require('./FlightTaskControl.js')
let SDKControlAuthority = require('./SDKControlAuthority.js')
let SetGoHomeAltitude = require('./SetGoHomeAltitude.js')
let CameraZoomCtrl = require('./CameraZoomCtrl.js')
let StopWaypointV2Mission = require('./StopWaypointV2Mission.js')
let SetupCameraStream = require('./SetupCameraStream.js')
let DownloadWaypointV2Mission = require('./DownloadWaypointV2Mission.js')
let MissionHpResetYaw = require('./MissionHpResetYaw.js')
let DroneArmControl = require('./DroneArmControl.js')
let GetGlobalCruisespeed = require('./GetGlobalCruisespeed.js')
let UploadWaypointV2Mission = require('./UploadWaypointV2Mission.js')
let GetDroneType = require('./GetDroneType.js')
let StereoVGASubscription = require('./StereoVGASubscription.js')
let StartWaypointV2Mission = require('./StartWaypointV2Mission.js')
let MissionWpSetSpeed = require('./MissionWpSetSpeed.js')
let MissionWpGetSpeed = require('./MissionWpGetSpeed.js')
let CameraStartShootSinglePhoto = require('./CameraStartShootSinglePhoto.js')
let MFIOConfig = require('./MFIOConfig.js')
let AvoidEnable = require('./AvoidEnable.js')
let CameraShutterSpeed = require('./CameraShutterSpeed.js')
let SetHardSync = require('./SetHardSync.js')
let CameraStopShootPhoto = require('./CameraStopShootPhoto.js')
let UploadWaypointV2Action = require('./UploadWaypointV2Action.js')
let QueryDroneVersion = require('./QueryDroneVersion.js')
let SetGlobalCruisespeed = require('./SetGlobalCruisespeed.js')
let SendPayloadData = require('./SendPayloadData.js')
let MissionHpUpdateYawRate = require('./MissionHpUpdateYawRate.js')
let InitWaypointV2Setting = require('./InitWaypointV2Setting.js')
let MissionHpGetInfo = require('./MissionHpGetInfo.js')
let MissionHpAction = require('./MissionHpAction.js')
let MFIOSetValue = require('./MFIOSetValue.js')
let CameraStartShootBurstPhoto = require('./CameraStartShootBurstPhoto.js')
let MissionWpUpload = require('./MissionWpUpload.js')
let MissionHpUpdateRadius = require('./MissionHpUpdateRadius.js')
let MFIO = require('./MFIO.js')
let CameraTapZoomPoint = require('./CameraTapZoomPoint.js')
let StereoDepthSubscription = require('./StereoDepthSubscription.js')
let CameraEV = require('./CameraEV.js')
let SetNewHomePoint = require('./SetNewHomePoint.js')
let CameraISO = require('./CameraISO.js')
let CameraStartShootAEBPhoto = require('./CameraStartShootAEBPhoto.js')
let CameraStartShootIntervalPhoto = require('./CameraStartShootIntervalPhoto.js')
let SendMobileData = require('./SendMobileData.js')
let GenerateWaypointV2Action = require('./GenerateWaypointV2Action.js')
let SetLocalPosRef = require('./SetLocalPosRef.js')
let MissionStatus = require('./MissionStatus.js')
let PauseWaypointV2Mission = require('./PauseWaypointV2Mission.js')
let CameraRecordVideoAction = require('./CameraRecordVideoAction.js')
let CameraFocusPoint = require('./CameraFocusPoint.js')
let MissionWpGetInfo = require('./MissionWpGetInfo.js')
let MissionHpUpload = require('./MissionHpUpload.js')
let GetM300StereoParams = require('./GetM300StereoParams.js')
let SetupCameraH264 = require('./SetupCameraH264.js')
let Stereo240pSubscription = require('./Stereo240pSubscription.js')
let CameraAction = require('./CameraAction.js')
let Activation = require('./Activation.js')
let ResumeWaypointV2Mission = require('./ResumeWaypointV2Mission.js')
let CameraAperture = require('./CameraAperture.js')
let MissionWpAction = require('./MissionWpAction.js')

module.exports = {
  GimbalAction: GimbalAction,
  DroneTaskControl: DroneTaskControl,
  FlightTaskControl: FlightTaskControl,
  SDKControlAuthority: SDKControlAuthority,
  SetGoHomeAltitude: SetGoHomeAltitude,
  CameraZoomCtrl: CameraZoomCtrl,
  StopWaypointV2Mission: StopWaypointV2Mission,
  SetupCameraStream: SetupCameraStream,
  DownloadWaypointV2Mission: DownloadWaypointV2Mission,
  MissionHpResetYaw: MissionHpResetYaw,
  DroneArmControl: DroneArmControl,
  GetGlobalCruisespeed: GetGlobalCruisespeed,
  UploadWaypointV2Mission: UploadWaypointV2Mission,
  GetDroneType: GetDroneType,
  StereoVGASubscription: StereoVGASubscription,
  StartWaypointV2Mission: StartWaypointV2Mission,
  MissionWpSetSpeed: MissionWpSetSpeed,
  MissionWpGetSpeed: MissionWpGetSpeed,
  CameraStartShootSinglePhoto: CameraStartShootSinglePhoto,
  MFIOConfig: MFIOConfig,
  AvoidEnable: AvoidEnable,
  CameraShutterSpeed: CameraShutterSpeed,
  SetHardSync: SetHardSync,
  CameraStopShootPhoto: CameraStopShootPhoto,
  UploadWaypointV2Action: UploadWaypointV2Action,
  QueryDroneVersion: QueryDroneVersion,
  SetGlobalCruisespeed: SetGlobalCruisespeed,
  SendPayloadData: SendPayloadData,
  MissionHpUpdateYawRate: MissionHpUpdateYawRate,
  InitWaypointV2Setting: InitWaypointV2Setting,
  MissionHpGetInfo: MissionHpGetInfo,
  MissionHpAction: MissionHpAction,
  MFIOSetValue: MFIOSetValue,
  CameraStartShootBurstPhoto: CameraStartShootBurstPhoto,
  MissionWpUpload: MissionWpUpload,
  MissionHpUpdateRadius: MissionHpUpdateRadius,
  MFIO: MFIO,
  CameraTapZoomPoint: CameraTapZoomPoint,
  StereoDepthSubscription: StereoDepthSubscription,
  CameraEV: CameraEV,
  SetNewHomePoint: SetNewHomePoint,
  CameraISO: CameraISO,
  CameraStartShootAEBPhoto: CameraStartShootAEBPhoto,
  CameraStartShootIntervalPhoto: CameraStartShootIntervalPhoto,
  SendMobileData: SendMobileData,
  GenerateWaypointV2Action: GenerateWaypointV2Action,
  SetLocalPosRef: SetLocalPosRef,
  MissionStatus: MissionStatus,
  PauseWaypointV2Mission: PauseWaypointV2Mission,
  CameraRecordVideoAction: CameraRecordVideoAction,
  CameraFocusPoint: CameraFocusPoint,
  MissionWpGetInfo: MissionWpGetInfo,
  MissionHpUpload: MissionHpUpload,
  GetM300StereoParams: GetM300StereoParams,
  SetupCameraH264: SetupCameraH264,
  Stereo240pSubscription: Stereo240pSubscription,
  CameraAction: CameraAction,
  Activation: Activation,
  ResumeWaypointV2Mission: ResumeWaypointV2Mission,
  CameraAperture: CameraAperture,
  MissionWpAction: MissionWpAction,
};
