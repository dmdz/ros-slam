// Auto-generated. Do not edit!

// (in-package dji_osdk_ros.srv)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------


//-----------------------------------------------------------

class FlightTaskControlRequest {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.task = null;
      this.pos_offset = null;
      this.yaw_params = null;
    }
    else {
      if (initObj.hasOwnProperty('task')) {
        this.task = initObj.task
      }
      else {
        this.task = 0;
      }
      if (initObj.hasOwnProperty('pos_offset')) {
        this.pos_offset = initObj.pos_offset
      }
      else {
        this.pos_offset = [];
      }
      if (initObj.hasOwnProperty('yaw_params')) {
        this.yaw_params = initObj.yaw_params
      }
      else {
        this.yaw_params = [];
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type FlightTaskControlRequest
    // Serialize message field [task]
    bufferOffset = _serializer.uint8(obj.task, buffer, bufferOffset);
    // Serialize message field [pos_offset]
    bufferOffset = _arraySerializer.float64(obj.pos_offset, buffer, bufferOffset, null);
    // Serialize message field [yaw_params]
    bufferOffset = _arraySerializer.float64(obj.yaw_params, buffer, bufferOffset, null);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type FlightTaskControlRequest
    let len;
    let data = new FlightTaskControlRequest(null);
    // Deserialize message field [task]
    data.task = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [pos_offset]
    data.pos_offset = _arrayDeserializer.float64(buffer, bufferOffset, null)
    // Deserialize message field [yaw_params]
    data.yaw_params = _arrayDeserializer.float64(buffer, bufferOffset, null)
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += 8 * object.pos_offset.length;
    length += 8 * object.yaw_params.length;
    return length + 9;
  }

  static datatype() {
    // Returns string type for a service object
    return 'dji_osdk_ros/FlightTaskControlRequest';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'acb64b4ce3e62a7a574f169761458f57';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    #constant for tasks
    uint8 TASK_GOHOME = 1
    uint8 TASK_GO_LOCAL_POS = 2
    uint8 TASK_GOHOME_AND_CONFIRM_LANDING = 3
    uint8 TASK_TAKEOFF = 4
    uint8 TASK_LAND = 6
    
    #request
    uint8 task    # see constants above for possible tasks
    float64[] pos_offset #A vector contains that position_x_offset, position_y_offset, position_z_offset in order
    float64[] yaw_params #A vector contains that yaw_desired, position_threshold(Meter), yaw_threshold(Degree)
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new FlightTaskControlRequest(null);
    if (msg.task !== undefined) {
      resolved.task = msg.task;
    }
    else {
      resolved.task = 0
    }

    if (msg.pos_offset !== undefined) {
      resolved.pos_offset = msg.pos_offset;
    }
    else {
      resolved.pos_offset = []
    }

    if (msg.yaw_params !== undefined) {
      resolved.yaw_params = msg.yaw_params;
    }
    else {
      resolved.yaw_params = []
    }

    return resolved;
    }
};

// Constants for message
FlightTaskControlRequest.Constants = {
  TASK_GOHOME: 1,
  TASK_GO_LOCAL_POS: 2,
  TASK_GOHOME_AND_CONFIRM_LANDING: 3,
  TASK_TAKEOFF: 4,
  TASK_LAND: 6,
}

class FlightTaskControlResponse {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.result = null;
      this.cmd_set = null;
      this.cmd_id = null;
      this.ack_data = null;
    }
    else {
      if (initObj.hasOwnProperty('result')) {
        this.result = initObj.result
      }
      else {
        this.result = false;
      }
      if (initObj.hasOwnProperty('cmd_set')) {
        this.cmd_set = initObj.cmd_set
      }
      else {
        this.cmd_set = 0;
      }
      if (initObj.hasOwnProperty('cmd_id')) {
        this.cmd_id = initObj.cmd_id
      }
      else {
        this.cmd_id = 0;
      }
      if (initObj.hasOwnProperty('ack_data')) {
        this.ack_data = initObj.ack_data
      }
      else {
        this.ack_data = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type FlightTaskControlResponse
    // Serialize message field [result]
    bufferOffset = _serializer.bool(obj.result, buffer, bufferOffset);
    // Serialize message field [cmd_set]
    bufferOffset = _serializer.uint8(obj.cmd_set, buffer, bufferOffset);
    // Serialize message field [cmd_id]
    bufferOffset = _serializer.uint8(obj.cmd_id, buffer, bufferOffset);
    // Serialize message field [ack_data]
    bufferOffset = _serializer.uint32(obj.ack_data, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type FlightTaskControlResponse
    let len;
    let data = new FlightTaskControlResponse(null);
    // Deserialize message field [result]
    data.result = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [cmd_set]
    data.cmd_set = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [cmd_id]
    data.cmd_id = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [ack_data]
    data.ack_data = _deserializer.uint32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 7;
  }

  static datatype() {
    // Returns string type for a service object
    return 'dji_osdk_ros/FlightTaskControlResponse';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'f03b0bc279b265c40946c1011df83013';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool result
    # for debugging usage
    uint8 cmd_set
    uint8 cmd_id
    uint32 ack_data
    
    
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new FlightTaskControlResponse(null);
    if (msg.result !== undefined) {
      resolved.result = msg.result;
    }
    else {
      resolved.result = false
    }

    if (msg.cmd_set !== undefined) {
      resolved.cmd_set = msg.cmd_set;
    }
    else {
      resolved.cmd_set = 0
    }

    if (msg.cmd_id !== undefined) {
      resolved.cmd_id = msg.cmd_id;
    }
    else {
      resolved.cmd_id = 0
    }

    if (msg.ack_data !== undefined) {
      resolved.ack_data = msg.ack_data;
    }
    else {
      resolved.ack_data = 0
    }

    return resolved;
    }
};

module.exports = {
  Request: FlightTaskControlRequest,
  Response: FlightTaskControlResponse,
  md5sum() { return '2f0fa5b45abe3a96e38cf4ab21baa780'; },
  datatype() { return 'dji_osdk_ros/FlightTaskControl'; }
};
