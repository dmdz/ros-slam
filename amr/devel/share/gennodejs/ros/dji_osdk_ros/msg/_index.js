
"use strict";

let WaypointV2CameraActuator = require('./WaypointV2CameraActuator.js');
let MissionHotpointTask = require('./MissionHotpointTask.js');
let WaypointList = require('./WaypointList.js');
let WaypointV2AircraftControlActuatorRotateHeading = require('./WaypointV2AircraftControlActuatorRotateHeading.js');
let WaypointV2GimbalActuator = require('./WaypointV2GimbalActuator.js');
let WaypointV2CameraActuatorFocusParam = require('./WaypointV2CameraActuatorFocusParam.js');
let WaypointV2TrajectoryTrigger = require('./WaypointV2TrajectoryTrigger.js');
let MissionWaypoint = require('./MissionWaypoint.js');
let Gimbal = require('./Gimbal.js');
let MissionWaypointAction = require('./MissionWaypointAction.js');
let PayloadData = require('./PayloadData.js');
let Waypoint = require('./Waypoint.js');
let WaypointV2AircraftControlActuatorFlying = require('./WaypointV2AircraftControlActuatorFlying.js');
let WaypointV2ReachpointTrigger = require('./WaypointV2ReachpointTrigger.js');
let WaypointV2Config = require('./WaypointV2Config.js');
let WaypointV2AircraftControlActuator = require('./WaypointV2AircraftControlActuator.js');
let WaypointV2 = require('./WaypointV2.js');
let MobileData = require('./MobileData.js');
let WaypointV2CameraActuatorFocalLengthParam = require('./WaypointV2CameraActuatorFocalLengthParam.js');
let WaypointV2GimbalActuatorRotationParam = require('./WaypointV2GimbalActuatorRotationParam.js');
let MissionWaypointTask = require('./MissionWaypointTask.js');
let WaypointV2AssociateTrigger = require('./WaypointV2AssociateTrigger.js');
let VOPosition = require('./VOPosition.js');
let WaypointV2Action = require('./WaypointV2Action.js');
let WaypointV2IntervalTrigger = require('./WaypointV2IntervalTrigger.js');
let WaypointV2InitSetting = require('./WaypointV2InitSetting.js');
let FlightAnomaly = require('./FlightAnomaly.js');
let WaypointV2SampleReachPointTrigger = require('./WaypointV2SampleReachPointTrigger.js');
let FCTimeInUTC = require('./FCTimeInUTC.js');
let GPSUTC = require('./GPSUTC.js');

module.exports = {
  WaypointV2CameraActuator: WaypointV2CameraActuator,
  MissionHotpointTask: MissionHotpointTask,
  WaypointList: WaypointList,
  WaypointV2AircraftControlActuatorRotateHeading: WaypointV2AircraftControlActuatorRotateHeading,
  WaypointV2GimbalActuator: WaypointV2GimbalActuator,
  WaypointV2CameraActuatorFocusParam: WaypointV2CameraActuatorFocusParam,
  WaypointV2TrajectoryTrigger: WaypointV2TrajectoryTrigger,
  MissionWaypoint: MissionWaypoint,
  Gimbal: Gimbal,
  MissionWaypointAction: MissionWaypointAction,
  PayloadData: PayloadData,
  Waypoint: Waypoint,
  WaypointV2AircraftControlActuatorFlying: WaypointV2AircraftControlActuatorFlying,
  WaypointV2ReachpointTrigger: WaypointV2ReachpointTrigger,
  WaypointV2Config: WaypointV2Config,
  WaypointV2AircraftControlActuator: WaypointV2AircraftControlActuator,
  WaypointV2: WaypointV2,
  MobileData: MobileData,
  WaypointV2CameraActuatorFocalLengthParam: WaypointV2CameraActuatorFocalLengthParam,
  WaypointV2GimbalActuatorRotationParam: WaypointV2GimbalActuatorRotationParam,
  MissionWaypointTask: MissionWaypointTask,
  WaypointV2AssociateTrigger: WaypointV2AssociateTrigger,
  VOPosition: VOPosition,
  WaypointV2Action: WaypointV2Action,
  WaypointV2IntervalTrigger: WaypointV2IntervalTrigger,
  WaypointV2InitSetting: WaypointV2InitSetting,
  FlightAnomaly: FlightAnomaly,
  WaypointV2SampleReachPointTrigger: WaypointV2SampleReachPointTrigger,
  FCTimeInUTC: FCTimeInUTC,
  GPSUTC: GPSUTC,
};
