# Simultaneous Localization & Mapping (SLAM) robot on Robot Operating System (ROS)

Implemented autonomous mobile robot on ROS with EKF-SLAM, occupancy grid, planner (A* search/ Dijkstra's algorithm), path following control, GUI, simulator, executive & camera perception nodes.
Navigated a TurtleBot to 10 waypoints in 75s, explored the area & observed 10 landmarks in 4 mins, without hitting any wall/ obstacles. 

## To launch simulation for waypoints navigation:

```bash
source amr/devel/setup.bash
roslaunch amr/src/executive/launch/amr_sim.launch
```

![GUI Simulation](/images/amr.gif)
![GUI Simulation2](/images/GUI_Simulation2.PNG)

## To run on a TurtleBot:

```bash
source amr/devel/setup.bash
roslaunch amr/src/executive/launch/amr.launch 
```

## ROS topics:
![ROS topics](/images/ROStopics.PNG)

#### Tag
robot, ROS, EKF-SLAM, A* search, Dijkstra's algorithm, camera perception, TurtleBot2, navigation 

