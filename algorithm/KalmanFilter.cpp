#include <iostream> 
#include <math.h> 
#include <stdlib.h>
#include <time.h>  

#define N 2 //max dimension of matrix
using namespace std; 
double mu0[2][1]= {{0}, {0}};
double Sigma0[2][2]= { {0.02, 0 }, { 0, 0.02} };

double A1[2][2] = {{0.2, 0.7 }, {0.4, 0.3 }};
double A1transpose[2][2];

double B1[2][2] = { {0.4, 0.3 }, {0.9, 0.6} };

double C1[2][2] = { {0.2 , 0.05}, {0.07 ,0.2 }};
double C1transpose[2][2];

double R1[2][2] = { {0.01, 0.02}, {0.02, 0.01}};
double Q1[2][2] = { {0.04, 0}, {0, 0.08} };
double u1 [2][1] = {1, 0.5};
double z1[2][1] = {2, 1};
double mu1[2][1], Sigma1[2][2];
double muBar1[2][1], SigmaBar1[2][2];
double K1[2][2];
double inversedMatrix[2][2];
double I[2][2] = {{1,0}, {1,0}};


// https://www.geeksforgeeks.org/c-program-multiply-two-matrices/
// Multiplies two matrices mat1[][] and mat2[][] and prints result. 
// (m1) x (m2) and (n1) x (n2) are dimensions of given matrices. 
void multiplyMatrix(int m1, int m2, double mat1[][N], int n1, int n2, double mat2[][N]) 
{ 
    int x, i, j; 
    int res[m1][n2]; 
    for (i = 0; i < m1; i++)  
    { 
        for (j = 0; j < n2; j++)  
        { 
            res[i][j] = 0; 
            for (x = 0; x < m2; x++)  
            { 
                *(*(res + i) + j) += *(*(mat1 + i) + x) * 
                                     *(*(mat2 + x) + j); 
            } 
        } 
    } 
    for (i = 0; i < m1; i++)  
    { 
        for (j = 0; j < n2; j++)  
        { 
            cout << *(*(res + i) + j) << " "; 
        } 
        cout << "\n"; 
    } 
} 

// This function adds A[][] and B[][], and stores  
// the result in C[][]  
void addMatrix(double A[][N], double B[][N], double C[][N])  
{  
    int i, j;  
    for (i = 0; i < N; i++)  
        for (j = 0; j < N; j++)  
            C[i][j] = A[i][j] + B[i][j];  
}

void printMatrix(int m, int n, double mat[][N]) {
	for (int i = 0; i < m; i++) 
	    for (int j = 0; j < n; j++) 
	    	// Prints ' ' if j != n-1 else prints '\n'           
	        cout << mat[i][j] << " \n"[j == n-1]; 
}

void transposeMatrix(double A[][N], double B[][N]) 
{ 
    int i, j; 
    for (i = 0; i < N; i++) 
        for (j = 0; j < N; j++) 
            B[i][j] = A[j][i]; 
} 

void scalarProductMat(double mat[][N], int k) 
{ 
    // scalar element is multiplied by the matrix 
    for (int i = 0; i < N; i++)  
        for (int j = 0; j < N; j++)  
            mat[i][j] = mat[i][j] * k;         
} 

void getCofactor(double A[N][N], double temp[N][N], int p, int q, int n) 
{ 
    int i = 0, j = 0; 
  
    // Looping for each element of the matrix 
    for (int row = 0; row < n; row++) 
    { 
        for (int col = 0; col < n; col++) 
        { 
            //  Copying into temporary matrix only those element 
            //  which are not in given row and column 
            if (row != p && col != q) 
            { 
                temp[i][j++] = A[row][col]; 
  
                // Row is filled, so increase row index and 
                // reset col index 
                if (j == n - 1) 
                { 
                    j = 0; 
                    i++; 
                } 
            } 
        } 
    } 
} 
  
/* Recursive function for finding determinant of matrix. 
   n is current dimension of A[][]. */
double determinant(double A[N][N], int n) 
{ 
    int D = 0; // Initialize result 
  
    //  Base case : if matrix contains single element 
    if (n == 1) 
        return A[0][0]; 
  
    double temp[N][N]; // To store cofactors 
  
    int sign = 1;  // To store sign multiplier 
  
     // Iterate for each element of first row 
    for (int f = 0; f < n; f++) 
    { 
        // Getting Cofactor of A[0][f] 
        getCofactor(A, temp, 0, f, n); 
        D += sign * A[0][f] * determinant(temp, n - 1); 
  
        // terms are to be added with alternate sign 
        sign = -sign; 
    } 
  
    return D; 
} 
  
// Function to get adjoint of A[N][N] in adj[N][N]. 
void adjoint(double A[N][N],double adj[N][N]) 
{ 
    if (N == 1) 
    { 
        adj[0][0] = 1; 
        return; 
    } 
  
    // temp is used to store cofactors of A[][] 
    double sign = 1, temp[N][N]; 
  
    for (int i=0; i<N; i++) 
    { 
        for (int j=0; j<N; j++) 
        { 
            // Get cofactor of A[i][j] 
            getCofactor(A, temp, i, j, N); 
  
            // sign of adj[j][i] positive if sum of row 
            // and column indexes is even. 
            sign = ((i+j)%2==0)? 1: -1; 
  
            // Interchanging rows and columns to get the 
            // transpose of the cofactor matrix 
            adj[j][i] = (sign)*(determinant(temp, N-1)); 
        } 
    } 
} 
  
// Function to calculate and store inverse, returns false if 
// matrix is singular 
void inverseMatrix(double A[N][N], double inverse[N][N]) 
{ 
    // Find determinant of A[][] 
    int det = determinant(A, N); 
    if (det == 0) 
    { 
        cout << "Singular matrix, can't find its inverse"; 
        return; 
    } 
  
    // Find adjoint 
    double adj[N][N]; 
    adjoint(A, adj); 
  
    // Find Inverse using formula "inverse(A) = adj(A)/det(A)" 
    for (int i=0; i<N; i++) 
        for (int j=0; j<N; j++) 
  	        inverse[i][j] = adj[i][j]/double(det); 
      
} 


void KalmanFilter() {
	addMatrix( multiplyMatrix(2,2 ,A1,2 ,1,mu0 ), multiplyMatrix(2,2, B1, 2, 1, u1), muBar1); //muBar= 2x1
	transposeMatrix(A1, A1transpose);
	transposeMatrix(C1, C1transpose);
	addMatrix( multiplyMatrix  ((multiplyMatrix(2,2, A1, 2,2, Sigma0)) , A1transpose), R1, SigmaBar1); // SigmaBar1= 2x2

	double C1XSigmaBar1[2][2];
	double C1XSigmaBar1[2][2]= multiplyMatrix(2,2, C1, 2,2, SigmaBar1);
	double C1XSigmaBar1XC1transpose[2][2];
	double C1XSigmaBar1XC1transpose[2][2] = multiplyMatrix(2,2, C1XSigmaBar1, 2,2, C1transpose);
	double C1XSigmaBar1XC1transpose__Q1[2][2];
	addMatrix(C1XSigmaBar1XC1transpose, Q1,C1XSigmaBar1XC1transpose__Q1) ;
	inverseMatrix(C1XSigmaBar1XC1transpose__Q1, inversedMatrix);

	double SigmaBar1XC1transpose[2][2]= multiplyMatrix(2,2, SigmaBar1, 2,2, C1transpose);
	K1= multiplyMatrix(2,2, SigmaBar1XC1transpose, 2, 2, inversedMatrix);

	double C1XmuBar1[2][1] = multiplyMatrix(2,2, C1, 2, 1, muBar1);
	double C1XmuBar1X_1[2][1] = scalarProductMat(C1XmuBar1, -1);
	double z1_C1XmuBar1[2][1];
	addMatrix(z1, C1XmuBar1X_1, z1_C1XmuBar1);
	double K1X__z1_C1XmuBar1__ [2][1] = multiplyMatrix(2,2, K1, 2,1, z1_C1XmuBar1);
	addMatrix(muBar1, K1X__z1_C1XmuBar1__, mu1); // mu1= 2x1

	double K1XC1[2][2]= multiplyMatrix(2,2, K1, 2,2, C1);
	double _K1XC1[2][2]= scalarProductMat(K1XC1, -1);
	double I_K1XC1[2][2];
	addMatrix(I, _K1XC1, I_K1XC1);
	Sigma1= multiplyMatrix(2, 2, I_K1XC1, 2, 2, SigmaBar1); 
	cout <<"mu1 matrix" << endl;
	printMatrix(2, 1, mu1);
	cout <<"Sigma1 matrix" << endl;
	printMatrix(2,2,Sigma1);
}

int main() 
{ 
	//cout << probCalc(1,4);
	srand (time(NULL));
	//cout << sampleCalc(10000);
	KalmanFilter();
	 
    return 0; 
}



