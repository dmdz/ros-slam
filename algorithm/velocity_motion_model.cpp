#include <iostream> 
#include <math.h> 
#include <stdlib.h>
#include <time.h>  


using namespace std; 
  
double a1= 0.1, a2= 0.2, a3= 0.4, a4= 0.3, a5= 0.3, a6=0.2;
double v= 1;
double w= 0.1;
double x=2, y=1, theta=0.3;
double deltaT= 0.5;
double vHat, wHat, gammaHat;
double xPrime, yPrime, thetaPrime;



double probCalc (double a, double b2) {
	return ( (1/sqrt(2* M_PI *b2) * exp(-(a*a) / (2*b2) ) ) ) ;
}

double rand_FloatRange(double a, double b) //random btw a and b
{
    return ((b - a) * ((double)rand() / RAND_MAX)) + a;
}

double sampleCalc(double b2) {
	double result=0, sum_temp=0;
	int b= int(sqrt(b2));
	for (int i=0; i<12; i++) {
		sum_temp += rand_FloatRange(b*-1, b );
	}
	result= sum_temp/2;
	return result;
}

void velocityMotionModel () {
	vHat= v + sampleCalc(a1*v*v + a2*w*w);
	wHat= w + sampleCalc(a3*v*v + a4*w*w);
	gammaHat= sampleCalc(a5*v*v + a6*w*w);
	xPrime= x - (vHat/wHat)*sin(theta) + (vHat/wHat)*sin(theta + wHat * deltaT);
	yPrime= y + (vHat/wHat)*cos(theta) - (vHat/wHat)*cos(theta + wHat * deltaT);
	thetaPrime = theta + wHat*deltaT + gammaHat * deltaT;
	cout << "vHat: " << vHat << endl <<"wHat: "<< wHat << endl <<"gammaHat: "<< gammaHat << endl;
	cout << "xPrime: " <<xPrime <<endl << "yPrime: "<<yPrime << endl << "thetaPrime: "<<thetaPrime<<endl;
}

int main() 
{ 
	//cout << probCalc(1,4);
	srand (time(NULL));
	//cout << sampleCalc(10000);
	velocityMotionModel();  
    return 0; 
}



