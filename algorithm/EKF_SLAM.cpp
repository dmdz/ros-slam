#include <iostream> 
#include <math.h> 
//#include <Eigen/Dense>
#include <eigen3/Eigen/Dense>
#include <iomanip>

using namespace Eigen; 
using namespace std;




void EKF_Simult_Local() {
	Vector3d mu_0 (0.5, 0, 0.1);
	double mu_0_x= mu_0(0);
	double mu_0_y= mu_0(1);
	double mu_0_theta= mu_0(2);
	Matrix3d Sigma_0;
	Vector2d u_1(0.25, -0.05);
	double v_1= u_1(0);
	double w_1= u_1(1);
	double a1, a2, a3, a4;
	a1= a2= a3= a4= 0.002;
	double sigma_2_r= 0.003;
	double sigma_2_phi= 0.003;
	double sigma_2_s= 0.001;
	Vector3d z_1_1(4.3, 1.1, 1);
	Vector3d z_2_1(4.4, -1.4, 2);
	double r_1_1= z_1_1(0);
	double r_2_1= z_2_1(0);
	double phi_1_1= z_1_1(1);
	double phi_2_1= z_2_1(1);
	double s_1_1= z_1_1(2);
	double s_2_1= z_2_1(2);
	double delta_t= 0.1;

//Need to find
	// double mu_bar_1;
	// double Sigma_bar_1;
	// double mu_1;
	// double Sigma_1;

	Sigma_0 << 	0.5, 0.05, 0.05,
	0.05, 0.5, 0.05,
	0.05, 0.05, 0.5;

	int N=0; //number of landmarks
	MatrixXd F_x (3, 3+3*N); // 3 x (3+3N) // mu_bar_1 = (3+3N)x3 * (3x1) = 3+3N x 1;
	F_x << MatrixXd::Identity(3,3);

	Vector3d temp_calc_mu_bar_1(-(v_1/w_1)*sin(mu_0_theta) + (v_1/w_1)*sin(mu_0_theta + w_1*delta_t), 
		(v_1/w_1)*cos(mu_0_theta) - (v_1/w_1)*cos(mu_0_theta + w_1*delta_t), 
		w_1*delta_t);
	VectorXd mu_0_calc(3+3*N);
	mu_0_calc << 0.5, 0, 0.1;
	VectorXd mu_bar_1(3+3*N);
	VectorXd mu_bar_1_original(3+3*N);
	mu_bar_1= mu_0_calc + F_x.transpose()*temp_calc_mu_bar_1;
	mu_bar_1_original= mu_bar_1;

	MatrixXd G_1(3+3*N, 3+3*N);
	Matrix3d temp_calc_G_1;
	temp_calc_G_1 << 	0, 0, -(v_1/w_1)*cos(mu_0_theta) + (v_1/w_1)*cos(mu_0_theta + w_1*delta_t),
	0, 0, -(v_1/w_1)*sin(mu_0_theta) + (v_1/w_1)*sin(mu_0_theta + w_1*delta_t),
	0, 0, 0;
	G_1 = MatrixXd::Identity(3*N+3, 3*N+3) + F_x.transpose()*temp_calc_G_1*F_x;
	Matrix2d M_1;
	MatrixXd V_1(3,2);
	V_1 << 	(-sin(mu_0_theta) + sin(mu_0_theta+w_1*delta_t))/w_1,
	v_1*(sin(mu_0_theta) - sin(mu_0_theta+w_1*delta_t))/(w_1*w_1) +v_1*delta_t*(cos(mu_0_theta+w_1*delta_t))/(w_1),
	(cos(mu_0_theta) - cos(mu_0_theta+w_1*delta_t))/w_1,
	-v_1*(cos(mu_0_theta) - cos(mu_0_theta+w_1*delta_t))/(w_1*w_1) +v_1*delta_t*(sin(mu_0_theta+w_1*delta_t))/(w_1),
	0, delta_t;	
	M_1 << a1*v_1*v_1 + a2*w_1*w_1, 0, 0, a3*v_1*v_1+ a4*w_1*w_1;
	Matrix3d R_1;
	R_1 = V_1*M_1*(V_1.transpose());
	MatrixXd Sigma_bar_1(3+3*N, 3+3*N);
	MatrixXd Sigma_bar_1_original(3+3*N, 3+3*N);	
	
	Sigma_bar_1 = G_1*Sigma_0*G_1.transpose() + F_x.transpose()*R_1*F_x;
	Sigma_bar_1_original= Sigma_bar_1;
	/////

	Matrix3d Q_1;
	Q_1 << 	sigma_2_r, 0, 0,
			0, sigma_2_phi, 0,
			0, 0, sigma_2_s;
	//for all observed features zi
	//for (int j=1, j<=2, j++) {
	int j=1;
	N=1;
	Vector3d mu_bar_j_1;
	//mu_landmark_1
	mu_bar_j_1 << 	mu_bar_1(0) + r_1_1*cos(phi_1_1+mu_bar_1(2)), 
	mu_bar_1(1) + r_1_1*sin(phi_1_1+mu_bar_1(2)),
	1;
	// cout << "mu_bar_j_1" << mu_bar_j_1 << endl;
	Vector2d sigma;
	sigma << 	mu_bar_j_1(0) - mu_bar_1(0),
	mu_bar_j_1(1) - mu_bar_1(1);
	double q=1;
	q= sigma.transpose() * sigma;
	Vector3d z_hat_1_1 (sqrt(q), atan2(sigma(1), sigma(0)) - mu_bar_1_original(2), mu_bar_j_1(2) );
	MatrixXd F_x_j_1 (6, 3+3*N);
	
	F_x_j_1 << Matrix3d::Identity() , MatrixXd::Zero(3,3*j-3+3+3*N-3*j), MatrixXd::Zero(3, 3+3*j-3), Matrix3d::Identity(), MatrixXd::Zero(3, 3*N-3*j);
	
	MatrixXd H_1_1;
	MatrixXd temp_calc_H_1_1 (3,6);
	temp_calc_H_1_1 <<	-sqrt(q)*sigma(0), -sqrt(q)*sigma(1), 0, sqrt(q)*sigma(0), sqrt(q)*sigma(1), 0,
						sigma(1), -sigma(0), -q, -sigma(1), sigma(0), 0,
						0,0,0,0,0,q;
	H_1_1= (1/q)*temp_calc_H_1_1*F_x_j_1;					

	MatrixXd K_1_1 (6,3);
	MatrixXd Sigma_bar_1_loop1 (6,6);

	//reinitialize
	Sigma_bar_1_loop1  << Sigma_bar_1, MatrixXd::Zero(3,3), MatrixXd::Zero(3,3), 100*MatrixXd::Identity(3,3);
	// cout << Sigma_bar_1_loop1;
	VectorXd mu_bar_1_loop1 (6);
	mu_bar_1_loop1 << mu_bar_1_original,  mu_bar_j_1;
	VectorXd mu_bar_1_loop1_original = mu_bar_1_loop1;
	
	// cout <<  setprecision(4)<<endl <<"Sigma_bar_1_loop1: "<<endl <<Sigma_bar_1_loop1 <<endl ;

	K_1_1 = (Sigma_bar_1_loop1 * H_1_1.transpose()) * (( H_1_1 * Sigma_bar_1_loop1 * H_1_1.transpose() + Q_1).inverse());
	
	mu_bar_1_loop1 = mu_bar_1_loop1 + K_1_1*(z_1_1 - z_hat_1_1);

	Sigma_bar_1_loop1= (MatrixXd::Identity(3+3*N, 3+3*N)-K_1_1*H_1_1)*Sigma_bar_1_loop1;

/////////

	j=2;
	N=2;
	Vector3d mu_bar_j_2;
	//cout << endl << mu_bar_1(0) << endl << r_2_1 << endl << phi_1_1 << endl << mu_bar_1(2);
	mu_bar_j_2 << 	mu_bar_1(0) + r_2_1*cos(phi_2_1+mu_bar_1(2)),
	mu_bar_1(1) + r_2_1*sin(phi_2_1+mu_bar_1(2)),
	2;

	//Vector2d sigma;
	sigma << 	mu_bar_j_2(0) - mu_bar_1_loop1(0),
				mu_bar_j_2(1) - mu_bar_1_loop1(1);
	//double q;
	q= sigma.transpose() * sigma;
	Vector3d z_hat_2_1 (sqrt(q), atan2(sigma(1), sigma(0)), mu_bar_j_2(2) );
	MatrixXd F_x_j_2 (6, 3+3*j-3+3+3*N-3*j);
	
	F_x_j_2 << Matrix3d::Identity() , MatrixXd::Zero(3,3*j-3+3+3*N-3*j), MatrixXd::Zero(3, 3+3*j-3), Matrix3d::Identity(), MatrixXd::Zero(3, 3*N-3*j);
	
	MatrixXd H_2_1 (3, 3+3*j-3+3*N-3*j);
	MatrixXd temp_calc_H_2_1 (3,6);
	temp_calc_H_2_1 <<	-sqrt(q)*sigma(0), -sqrt(q)*sigma(1), 0, sqrt(q)*sigma(0), sqrt(q)*sigma(1), 0,
	sigma(1), -sigma(0), -q, -sigma(1), sigma(0), 0,
	0,0,0,0,0,q;
	H_2_1= (1/q)*temp_calc_H_2_1*F_x_j_2;		
	MatrixXd K_2_1;
	MatrixXd Sigma_bar_1_loop2 (9,9);
	Sigma_bar_1_loop2 << Sigma_bar_1_loop1, MatrixXd::Zero(6,3), MatrixXd::Zero(3,6), 100*MatrixXd::Identity(3,3);

	// cout << Sigma_bar_1_loop1;
	VectorXd mu_bar_1_loop2 (9);
	//cout << mu_bar_1_loop1_original;
	mu_bar_1_loop2 << mu_bar_1_loop1_original, mu_bar_j_2;
	VectorXd mu_bar_1_loop2_original= mu_bar_1_loop2;

	K_2_1 = Sigma_bar_1_loop2*H_2_1.transpose()*( H_2_1*Sigma_bar_1_loop2*H_2_1.transpose() + Q_1).inverse();
	mu_bar_1_loop2 = mu_bar_1_loop2 + K_2_1*(z_2_1 - z_hat_2_1);
	Sigma_bar_1_loop2= (MatrixXd::Identity(3+3*N, 3+3*N)-K_2_1*H_2_1)*Sigma_bar_1_loop2;

	
//////Output////////////
	// cout <<  setprecision(4) <<"H_1_1 "<<endl <<H_1_1 <<endl<<endl ;
	// cout <<  setprecision(4) <<"K_1_1 "<<endl <<K_1_1 <<endl<<endl ;
	// cout <<  setprecision(4) <<"z_1_1 " <<endl<<z_1_1 <<endl<<endl ;
	// cout <<  setprecision(4) <<"z_hat_1_1 " <<endl<<z_hat_1_1<<endl<<endl ;

	// cout <<  setprecision(4) <<"H_2_1 "<<endl <<H_2_1 <<endl<<endl ;
	// cout <<  setprecision(4) <<"K_2_1 "<<endl <<K_2_1 <<endl<<endl ;
	// cout <<  setprecision(4) <<"z_2_1 " <<endl<<z_2_1 <<endl<<endl ;
	// cout <<  setprecision(4) <<"z_hat_2_1 " <<endl<<z_hat_2_1<<endl<<endl ;

	cout <<  setprecision(4) <<"mu_bar_1: "<<endl <<mu_bar_1_original <<endl<<endl ;
	cout <<  setprecision(4) <<"Sigma_bar_1: " <<endl<<Sigma_bar_1_original <<endl<<endl ;

	cout <<  setprecision(4) << "mu_bar_1_loop1_original: "<<endl <<mu_bar_1_loop1_original<<endl<<endl ;
	cout <<  setprecision(4) <<"Sigma_bar_1_loop1: "<<endl <<Sigma_bar_1_loop1<<endl<<endl ;

	cout <<  setprecision(4) << "mu_1 = mu_bar_1_loop2_original: "<<endl <<mu_bar_1_loop2_original<<endl<<endl ;
	cout <<  setprecision(4) <<"Sigma_1 = Sigma_bar_1_loop2: "<<endl <<Sigma_bar_1_loop2<<endl<<endl ;
}

int main() 
{ 
	srand (time(NULL));
	EKF_Simult_Local();
	return 0; 
}



