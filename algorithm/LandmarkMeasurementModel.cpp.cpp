#include <iostream> 
#include <math.h> 
#include <stdlib.h>
#include <time.h>  


using namespace std; 
  
double c1t=3, sj=3 ;
double m1x= 1, m1y=4, m2x= 3, m2y= 2.5, m3x= 3, m3y= 2, m4x= 1.5, m4y= 2.3;
double x=0.5, y=0.75, theta=0;
double r1t=2.6, phi1t=0.4, s1t=3;
double sigma_r=0.5, sigma_phi=0.5, sigma_s=0.5;

double j, gammaHat; 
double rHat, phiHat;



double probCalc (double a, double b2) {
	return ( (1/sqrt(2* M_PI *b2) * exp(-(a*a) / (2*b2) ) ) ) ;
}

double rand_FloatRange(double a, double b) //random btw a and b
{
    return ((b - a) * ((double)rand() / RAND_MAX)) + a;
}

double sampleCalc(double b2) {
	double result=0, sum_temp=0;
	int b= int(sqrt(b2));
	for (int i=0; i<12; i++) {
		sum_temp += rand_FloatRange(b*-1, b );
	}
	result= sum_temp/2;
	return result;
}

void LandmarkMeasurementModel() {
	j= c1t;
	gammaHat= rand_FloatRange(0, 2*M_PI);
	rHat = r1t + sampleCalc(sigma_r);
	phiHat = phi1t + sampleCalc(sigma_phi);
	cout << "rHat: " << rHat << endl << "phiHat: " << phiHat << endl;
}

int main() 
{ 
	//cout << probCalc(1,4);
	srand (time(NULL));
	//cout << sampleCalc(10000);
	LandmarkMeasurementModel();
	 
    return 0; 
}



