#include <iostream> 
#include <math.h>  
//#include <Eigen/Dense>
#include <eigen3/Eigen/Dense>
#include <iomanip>

using namespace Eigen; 
using namespace std;




void EKF_cal() {
	Vector3d mu_t_1(0.2, 0, 0.1); 
	Matrix3d Sigma_t_1;
	Sigma_t_1 <<	0.5, 0.05, 0.05, 
					0.05, 0.5, 0.05, 
					0.05, 0.05, 0.5;	
	Vector2d u_t(0.25,-0.05);
	double a1=0.002, a2=0.002, a3=0.002, a4=0.002;
	//double s=1;
	double m1x=2;
	double m1y=4;
	double m1s=1; //should be 1
	double m2x=2;
	double m2y=-4;
	double m2s=2;
	double sigma2r=0.003;
	double sigma2phi=0.003;
	double sigma2s=0.001;
	Vector3d z1t(4.3,1.1,1)	;
	Vector3d z2t(4.4,-1.4,2)	;
	double deltaT=0.1;


	Matrix3d Gt;
	MatrixXd Vt(3,2);
	Matrix2d Mt;
	Vector3d mu_bar_t;
	Vector3d mu_bar_t_loop;
	Vector3d diff_mu_t_1_mu_bar_t;
	Matrix3d Sigma_bar_t;
	Matrix3d Sigma_bar_t_loop;
	Matrix3d Qt;

	Vector3d z_hat_1_t;
	Matrix3d H1t;
	Matrix3d S1t;
	Matrix3d S1t_2pi; //temp
	Matrix3d K1t;
	Vector3d z1t_zhat1t;

	Vector3d z_hat_2_t;
	Matrix3d H2t;
	Matrix3d S2t;
	Matrix3d S2t_2pi; //temp
	Matrix3d K2t;
	Vector3d z2t_zhat1t;

	Vector3d mu_t_final;
	Matrix3d Sigma_t;
	Matrix3d Identity3;


	// Eigen::Vector3d z_hat_2_t;
	// Eigen::Matrix3d H2t;
	// Eigen::Matrix3d S2t;

	double theta= mu_t_1(2);
	double v_t= u_t(0);
	double w_t= u_t(1);

	double q;
	double p_zt;
	

	Identity3 << 	1,0,0, 0,1,0, 0,0,1;

	Gt << 	1,0, -(v_t/w_t)*cos(theta) + (v_t/w_t)*cos(theta+w_t*deltaT),
			0,1, -(v_t/w_t)*sin(theta) + (v_t/w_t)*sin(theta+w_t*deltaT),
			0,0, 1;
	Vt << 	(-sin(theta) + sin(theta + w_t*deltaT))/w_t, (v_t*(sin(theta) - sin(theta + w_t*deltaT))/(w_t*w_t))  +  (v_t*(cos(theta + w_t*deltaT))*deltaT)/w_t,
			(cos(theta) - cos(theta + w_t*deltaT))/w_t, (-v_t*(cos(theta) - cos(theta + w_t*deltaT))/(w_t*w_t))  +  (v_t*(sin(theta + w_t*deltaT))*deltaT)/w_t,
			0, deltaT;
	Mt <<	a1*v_t*v_t + a2*w_t*w_t, 0,
			0, a3*v_t*v_t + a4*w_t*w_t;
	diff_mu_t_1_mu_bar_t << (-v_t/w_t)*sin(theta) + (v_t/w_t)*sin(theta+w_t*deltaT),
							(v_t/w_t)*cos(theta) - (v_t/w_t)*cos(theta+w_t*deltaT),
							w_t*deltaT;
	mu_bar_t = mu_t_1 + diff_mu_t_1_mu_bar_t;
	mu_bar_t_loop= mu_bar_t;
	Sigma_bar_t = Gt * Sigma_t_1 * Gt.transpose() + Vt* Mt * Vt.transpose();
	Sigma_bar_t_loop =Sigma_bar_t;
	Qt	<< 	sigma2r, 0, 0,
			0, sigma2phi, 0,
			0, 0, sigma2s;

	//for all observed features zit = (rit φit sit)T do

	q= (m1x - mu_bar_t(0))*(m1x - mu_bar_t(0)) + (m1y - mu_bar_t(1))*(m1y - mu_bar_t(1));
	z_hat_1_t << sqrt(q), atan2(m1y- mu_bar_t(1) , m1x - mu_bar_t(0)) - mu_bar_t(2), m1s;
	H1t << 	-(m1x- mu_bar_t(0))/sqrt(q), -(m1y- mu_bar_t(1))/sqrt(q), 0,
			(m1y- mu_bar_t(1))/q, -(m1x- mu_bar_t(0))/q, -1,
			0,0,0;
	S1t= H1t*Sigma_bar_t_loop* H1t.transpose() + Qt;
	K1t= Sigma_bar_t_loop * H1t.transpose()* S1t.inverse();
	mu_bar_t_loop = mu_bar_t + K1t*(z1t - z_hat_1_t);
	Sigma_bar_t_loop= (Identity3 - K1t * H1t) * Sigma_bar_t_loop;

///
	q= (m2x - mu_bar_t_loop(0))*(m2x - mu_bar_t_loop(0)) + (m2y - mu_bar_t_loop(1))*(m2y - mu_bar_t_loop(1));
	z_hat_2_t << sqrt(q), atan2(m2y- mu_bar_t_loop(1) , m2x - mu_bar_t_loop(0)) - mu_bar_t_loop(2), m2s;
	H2t << 	-(m2x- mu_bar_t_loop(0))/sqrt(q), -(m2y- mu_bar_t_loop(1))/sqrt(q), 0,
			(m2y- mu_bar_t_loop(1))/q, -(m2x- mu_bar_t_loop(0))/q, -1,
			0,0,0;
	S2t= H2t*Sigma_bar_t_loop* H2t.transpose() + Qt;
	K2t= Sigma_bar_t_loop * H2t.transpose()* S2t.inverse();
	mu_bar_t_loop = mu_bar_t_loop + K2t*(z2t - z_hat_2_t);
	Sigma_bar_t_loop= (Identity3 - K2t * H2t) * Sigma_bar_t_loop;

	//end for
	mu_t_final = mu_bar_t_loop;
	Sigma_t= Sigma_bar_t_loop;
	
	S1t_2pi= 2*M_PI*S1t;
	p_zt=1;
	double temp1= S1t_2pi.determinant();
	MatrixXd temp2= (-1/2)*(z1t - z_hat_1_t).transpose() * S1t.inverse()* (z1t- z_hat_1_t);
	
	// p_zt= (1/sqrt(temp1))* exp(temp2(0));
	
	S2t_2pi= 2*M_PI*S2t;
	temp1= S2t_2pi.determinant();
	MatrixXd temp3= (z2t - z_hat_2_t).transpose() * S2t.inverse()*(z2t- z_hat_2_t);
	cout << "temp3" << temp3 << endl;
	// p_zt= (1/sqrt(temp1))* exp((-1/2) *temp3(0));
	//p_zt= temp2;
	
	
	cout << fixed << setprecision(4) << "Gt: " << endl<< Gt << endl<< endl;
	cout << fixed << setprecision(4) << "Vt: " <<endl<< Vt<< endl<< endl;
	cout << fixed << setprecision(4) <<"Mt: " << endl<<Mt<< endl<< endl;
	cout << fixed << setprecision(4) <<"mu_bar_t: " <<endl<< mu_bar_t<< endl<< endl;
	cout << fixed << setprecision(4) <<"Sigma_bar_t: "<<endl<< Sigma_bar_t<< endl<< endl;
	cout << fixed << setprecision(4) <<"Qt: " << endl<<Qt<< endl<< endl;
	cout << fixed << setprecision(4) <<"z_hat_1_t: " <<endl<< z_hat_1_t<< endl<< endl;
	cout << fixed << setprecision(4) <<"H1t: " << endl<<H1t<< endl<< endl;
	cout << fixed << setprecision(4) <<"S1t: " << endl<<S1t<< endl<< endl;
	cout << fixed << setprecision(4) <<"K1t: " << endl<<K1t<< endl<< endl;
	cout << fixed << setprecision(4) <<"z1t - z_hat_1_t" << endl<<z1t - z_hat_1_t<< endl<< endl;

	cout << fixed << setprecision(4) <<"z_hat_2_t: " <<endl<< z_hat_2_t<< endl<< endl;
	cout << fixed << setprecision(4) <<"H2t: " << endl<<H2t<< endl<< endl;
	cout << fixed << setprecision(4) <<"S2t: " << endl<<S2t<< endl<< endl;
	cout << fixed << setprecision(4) <<"K2t: " << endl<<K2t<< endl<< endl;
	cout << fixed << setprecision(4) <<"z2t - z_hat_2_t" << endl<<z2t - z_hat_2_t<< endl<< endl;

	cout << fixed << setprecision(4) <<"mu_t : " << endl<<mu_t_final<< endl<< endl;
	cout << fixed << setprecision(4) <<"Sigma_t: " << endl<<Sigma_t<< endl<< endl;
	// cout << fixed << setprecision(4) <<"p_zt: " << endl<<p_zt<< endl<< endl;
}

int main() 
{ 
	EKF_cal();
	return 0; 
}



